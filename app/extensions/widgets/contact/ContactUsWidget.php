<?php

class ContactUsWidget extends CWidget
{

    public function run()
    {
        $model = new ApplicationForm;
        if (isset($_POST['ApplicationForm'])) {
            $model->setAttributes($_POST['ApplicationForm']);
            if ($model->perform()) {
                Yii::app()->getUser()->setFlash('success', Yii::t('common', 'Thank you for your submition.'));
                $this->controller->refresh();
            }
        }
        $this->render('contact', compact('model'));
    }
}