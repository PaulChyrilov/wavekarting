<div class="send-form-block">

    <h3><?= Yii::t('application_form', 'Application Form') ?></h3>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id'     => 'application-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data'
        )
    )); ?>

        <table>
            <tr>
                <td>
                    <?= $form->labelEx($model, 'username'); ?>
                    <?= $form->textField($model, 'username'); ?>
                    <?= $form->error($model, 'username'); ?>
                </td>
                <td>
                    <?= $form->labelEx($model, 'email'); ?>
                    <?= $form->textField($model, 'email'); ?>
                    <?= $form->error($model, 'email'); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= $form->labelEx($model, 'phone'); ?>
                    <?= $form->textField($model, 'phone'); ?>
                    <?= $form->error($model, 'phone'); ?>
                </td>
                <td>
                    <?= $form->labelEx($model, 'file'); ?>
                    <?= $form->fileField($model, 'file'); ?>
                    <?= $form->error($model, 'file'); ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">                    
                    <?= $form->labelEx($model, 'description'); ?>
                    <?= $form->textArea($model, 'description'); ?>
                    <?= $form->error($model, 'description'); ?>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <button class="submit-button" type="submit"><?= Yii::t('application_form', 'Submit')?></button>
                </td>
            </tr>
        </table>
    <?php $this->endWidget(); ?>

    <div class="description">
        <p>Here can be placed application form for Career details page or something else depending the page</p>
    </div>

</div>

<?php 
$baseUrl = Yii::app()->baseUrl;
Yii::app()->getClientScript()
    ->registerCssFile($baseUrl . '/css/jquery.formstyler.css')
    ->registerScriptFile($baseUrl . '/js/lib/jquery/jquery.formstyler.min.js')
    ->registerScript('fileinputstyle', "$('#ApplicationForm_file').styler({browseText: 'Browse'});
") ?>
