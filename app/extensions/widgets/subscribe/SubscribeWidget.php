<?php

class SubscribeWidget extends CWidget
{
    public function run()
    {
        $model = new SubscribeForm;
        if (isset($_POST['SubscribeForm'])) {
            $model->setAttributes($_POST['SubscribeForm']);
            if ($model->perform()) {
                $this->getController()->getUser()->setFlash('SubscribeWidget', Yii::t('mainpage', 'Congratulations on signing up!'));
                $this->getController()->refresh();
            }
        }
        $this->render('subscribe', compact('model'));
    }
}