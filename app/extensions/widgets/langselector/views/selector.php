<?php 
$currentLang = Yii::app()->getLanguage();
?>
<ul class="<?= $this->ulClass ?>">
<?php foreach ($languages as $key => $lang) { ?>
    <li<?php if ($key == $currentLang) {?> class="active"<?php } ?>><?= CHtml::link($lang['name'], array('/languages/languages/change', 'lang' => $lang['id']))?></li>
<?php } ?>
</ul>
