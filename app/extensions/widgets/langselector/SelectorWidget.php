<?php

Yii::import('application.modules.languages.widgets.LanguageSelectorWidget');
/**
 * Description of SelectorWidget
 *
 * @author mlapko
 */
class SelectorWidget extends LanguageSelectorWidget
{
    public $ulClass = 'top-language-menu';
}