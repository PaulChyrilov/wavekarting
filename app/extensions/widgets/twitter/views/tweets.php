<h3><?= Yii::t('mainpage', 'latest tweets') ?></h3>
<div id="<?= $this->getId() ?>">
    <ul>
        <?php foreach ($tweets as $tweet) { ?>
        <li>
            <div class="tweet-item">
                <a href="https://twitter.com/<?= $this->channel ?>" target="_blank"><?= $tweet['text'] ?></a>
                <time><?= Yii::app()->getLocale()->getDateFormatter()->formatDateTime(strtotime($tweet['created_at']), 'medium', 'short'); ?></time>
            </div>
        </li>
        <?php } ?>
    </ul>
    <div class="tweets-gradient"></div>
</div>
<a class="button" href="https://twitter.com/<?= $this->channel ?>"><?= Yii::t('mainpage', 'follow us on twitter') ?></a>
