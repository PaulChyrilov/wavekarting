<?php
require_once dirname(__FILE__) . '/TwitterAPIExchange.php';

class TweetWidget extends CWidget
{
    public $channel;
    
    public $limit = 10;
    
    public $settings = array(
        'oauth_access_token' => '',
        'oauth_access_token_secret' => '',
        'consumer_key' => '',
        'consumer_secret' => ''
    );
    public $options = array(
        'speed' => 800,
        'pause' => 13000,
        'animation' => 'fade',
        'mousePause' => true,
        'showItems' => 2,
        'height' => 150
    );
    
    
    public function run()
    {
        $key = 'tweeter' . $this->channel . '.' . $this->limit;
        $twitter = new TwitterAPIExchange($this->settings);
        $cache = Yii::app()->getCache();
        if (($tweets = $cache->get($key)) === false) {
            $twitter = new TwitterAPIExchange($this->settings);
            $tweets = $twitter->setGetfield('?screen_name=' . $this->channel . '&count=' . $this->limit)
                 ->buildOauth('http://api.twitter.com/1.1/statuses/user_timeline.json', 'GET')
                 ->performRequest();
            if (!$tweets) {
                $tweets = '[]';
            }
            $tweets = json_decode($tweets, true);
            $cache->set($key, $tweets, 7200);
        }
        $this->_publish();
        $this->render('tweets', compact('tweets'));
    }
    
    protected function _publish()
    {
        
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->getAssetManager()->publish($assets, false, -1, YII_DEBUG);
        if (is_dir($assets)) {
            Yii::app()->getClientScript()
                ->registerScriptFile($baseUrl . '/jquery.vticker-min.js', CClientScript::POS_END)
                ->registerScript('tweets' . $this->getId(), '
                    $("#' . $this->getId() . '").vTicker(' . CJavaScript::encode($this->options) . ');
                ');
        } else {
            throw new CHttpException(500, get_class($this) . ' - Error: Could not find assets to publish.');
        }
    }
}
