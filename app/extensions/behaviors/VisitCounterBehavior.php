<?php

/**
 * Description of VisitCounterBehavior
 *
 * @author mlapko
 */
class VisitCounterBehavior extends CActiveRecordBehavior
{
    public $counterField = 'visitCount';
    public $count = 1;

    public function updateVisitCount()
    {
        $owner = $this->getOwner();
        $session = Yii::app()->getSession();
        $counter = $session->get('counter', array());        
        $ownerClass = get_class($owner);
        if (!isset($counter[$ownerClass]) || !in_array($owner->id, $counter[$ownerClass])) {
            if ($owner->updateCounters(array($this->counterField => $this->count), 'id = ' . $owner->id)) {
                $owner->{$this->counterField} += $this->count;
                $counter[$ownerClass][] = $owner->id;
                $session->add('counter', $counter);
            }
        }
    }
}