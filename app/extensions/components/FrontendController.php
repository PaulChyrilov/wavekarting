<?php


class FrontendController extends EController
{

    public $layout = '//layouts/main';
    
    public $with = array();
    public $modelName;
    
    protected $_model = null;

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function actionView($id)
    {
        $this->render('view', array('model' => $this->loadModel($this->modelName, $id)));
    }
    
    public function actionShow($slug)
    {
        $model = $this->loadModelBySlug($this->modelName, $slug);
        HApplication::attachMetaParams($model);
        $this->render('view', array('model' => $model));
    }

    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider($this->modelName);
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

}
