<?php

/**
 * EController class
 *
 * Has some useful methods for your Controllers
 */
class EController extends CController
{

    public $metaKeywords = '';
    public $metaDescription = '';
    public $breadcrumbs;
    
    public $skipAjaxRequest = false;
    
    /**
     * @return WebUser
     */
    public function getUser()
    {
        static $user;
        if ($user === null) {
            $user = Yii::app()->getUser();
        }
        return $user;
    }

    /**
     * Gets a param
     * @param $name
     * @param null $defaultValue
     * @return mixed
     */
    public function getActionParam($name, $defaultValue = null)
    {
        return Yii::app()->getRequest()->getParam($name, $defaultValue);
    }

    /**
     * Loads the requested data model.
     * @param string the model class name
     * @param integer the model ID
     * @param array additional search criteria
     * @param boolean whether to throw exception if the model is not found. Defaults to true.
     * @return CActiveRecord the model instance.
     * @throws CHttpException if the model cannot be found
     */
    protected function loadModel($class, $id, $criteria = array(), $exceptionOnNull = true)
    {
        if (empty($criteria))
            $model = CActiveRecord::model($class)->findByPk($id);
        else {
            $finder = CActiveRecord::model($class);
            $c = new CDbCriteria($criteria);
            $c->mergeWith(array(
                'condition' => $finder->getTableSchema()->primaryKey . '=:id',
                'params'    => array(':id' => $id),
            ));
            $model = $finder->find($c);
        }
        if (isset($model))
            return $model;
        else if ($exceptionOnNull)
            throw new CHttpException(404, 'Unable to find the requested object.');
    }
    
    protected function loadModelBySlug($class, $slug, $criteria = array(), $exceptionOnNull = true)
    {
        if (empty($criteria))
            $model = CActiveRecord::model($class)->findByAttributes(array('slug' => $slug));
        else {
            $finder = CActiveRecord::model($class);
            $c = new CDbCriteria($criteria);
            $c->mergeWith(array(
                'condition' => '`slug` = :id',
                'params'    => array(':id' => $slug),
            ));
            $model = $finder->find($c);
        }
        if (isset($model)) {
            return $model;
        } else if ($exceptionOnNull) {
            throw new CHttpException(404, 'Unable to find the requested object.');            
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($models, $formId = null)
    {
        if (isset($_POST['ajax']) && ((is_array($models) && $_POST['ajax'] === $formId) || $_POST['ajax'] === $models->getFormId())) {
            echo CActiveForm::validate($models);
            Yii::app()->end();
        }
    }

    /**
     * Outputs (echo) json representation of $data, prints html on debug mode.
     * NOTE: json_encode exists in PHP > 5.2, so it's safe to use it directly without checking
     * @param array $data the data (PHP array) to be encoded into json array
     * @param int $opts Bitmask consisting of JSON_HEX_QUOT, JSON_HEX_TAG, JSON_HEX_AMP, JSON_HEX_APOS, JSON_FORCE_OBJECT.
     */
    public function renderJson($data, $opts = null, $end = true)
    {
        if (YII_DEBUG && isset($_GET['debug']) && is_array($data)) {
            foreach ($data as $type => $v)
                printf('<h1>%s</h1>%s', $type, is_array($v) ? json_encode($v, $opts) : $v);
        } else {
            header('Content-Type: application/json; charset=UTF-8');
            echo json_encode($data, $opts);
            if ($end) {
                Yii::app()->end();
            }
        }
    }

    /**
     * Utility function to ensure the base url.
     * @param $url
     * @return string
     */
    public function baseUrl($url = '')
    {
        static $baseUrl;
        if ($baseUrl === null)
            $baseUrl = Yii::app()->getRequest()->getBaseUrl();
        return $baseUrl . '/' . ltrim($url, '/');
    }
    
    public function render($view, $data = null, $return = false)
    {
        $request = Yii::app()->getRequest();
        
        if (!$this->skipAjaxRequest && $request->getIsAjaxRequest()) {
            Yii::app()->getClientScript()->scriptMap = array(
                'jquery.js'               => false,
                'jquery.min.js'           => false,
                'jquery.ui.js'            => false,
                'jquery.ui.min.js'        => false,
                'jquery.yiiactiveform.js' => false,
                'jquery.yiilistview.js'   => false,
                'bootstrap-yii.css'       => false,
                'bootstrap.css'           => false,
                'jquery-ui-bootstrap.css' => false,
                'bootstrap-notify.css'    => false,
                'bootstrap.bootbox.min.js' => false,
                'bootstrap.notify.js'     => false,
                'bootstrap.js'            => false,
            );
            return parent::renderPartial($view, $data, false, true);
        }
        return parent::render($view, $data, $return);        
    }

}