<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class AdminUserIdentity extends CUserIdentity
{   
    const ERROR_ACCOUNT_BLOCKED = 5;
    
    private $_id;
    
    /**
     * @var User 
     */
    private $_user;

    public function authenticate()
    {
        Yii::app()->getModule('admins');
        $this->_user = Admin::model()->find('`email` = ?', array(strtolower($this->username)));
        if ($this->_user === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif ($this->_user->status == Admin::STATUS_BLOCKED) {
            $this->errorCode = self::ERROR_ACCOUNT_BLOCKED;
        } elseif (!$this->_user->validatePassword($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_id = $this->_user->id;
            $this->setState('username', $this->_user->username);
            $this->errorCode = self::ERROR_NONE;
        }
        return $this->errorCode == self::ERROR_NONE;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }
    
    /**
     * @return User 
     */
    public function getUser()
    {
        return $this->_user;
    }
}