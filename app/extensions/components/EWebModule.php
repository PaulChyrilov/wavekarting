<?php

/**
 * EWebModule class
 */
class EWebModule extends CWebModule
{
    protected function init()
    {
        $app = Yii::app();
        if ($app->asa('runEnd')) {
            $this->setControllerPath($this->getControllerPath() . DIRECTORY_SEPARATOR . $app->getEndName());
            $this->setViewPath($this->getViewPath() . DIRECTORY_SEPARATOR . $app->getEndName());
        }
    }
    
}
