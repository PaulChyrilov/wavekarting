<?php


class BackendController extends EController
{

    public $layout = '//layouts/main';
    
    public $params = array();
    public $photoUpload = false;
    public $scenario = null;
    public $with = array();
    public $redirectTo;
    public $modelName;
    
    protected $_model = null;

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'users'   => array('@'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id)
    {
        $this->render('view', array('model' => $this->loadModel($this->modelName, $id)));
    }

    public function actionCreate()
    {
        /** @var CActiveRecord $model */
        $model = new $this->modelName;
        if ($this->scenario) {
            $model->scenario = $this->scenario;
        }
        $this->performAjaxValidation($model);

        if (isset($_POST[$this->modelName])) {
            $model->setAttributes($_POST[$this->modelName]);
            if ($model->validate()) {
                if ($model->hasAttribute('image') && $model->asa('mImage')) {
                    $model->uploadImage(CUploadedFile::getInstance($model, 'image'), 'image');
                }
                $transaction = $model->getDbConnection()->beginTransaction();
                try {
                    $model->save(false);
                    $transaction->commit();
                    Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('common', 'Successfully created.'));
                    if ($this->redirectTo) {
                        $this->redirect($this->redirectTo);
                    } else {
                        $this->redirect(array('view', 'id' => $model->id));
                    }
                } catch (Exception $err) {
                    $transaction->rollback();
                    Yii::app()->getUser()->setFlash(
                        TbHtml::ALERT_COLOR_ERROR, 
                        Yii::t('common', 'System error: ' . $err->getMessage() . '.')
                    );
                }
            }
        }

        $this->render('create', array_merge(
            array('model' => $model), $this->params
        ));
    }

    public function actionUpdate($id)
    {
        if ($this->_model === null) {
            $model = $this->loadModel($this->modelName, $id);
        } else {
            $model = $this->_model;
        }

        $this->performAjaxValidation($model);

        if (isset($_POST[$this->modelName])) {
            $model->setAttributes($_POST[$this->modelName]);
            if ($model->validate()) {
                if ($model->hasAttribute('image') && $model->asa('mImage')) {
                    $this->photoUpload = $model->uploadImage(CUploadedFile::getInstance($model, 'image'), 'image');
                }
                $transaction = $model->getDbConnection()->beginTransaction();
                try {
                    $model->save(false);
                    $transaction->commit();
                    Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('common', 'Successfully updated.'));
                    if ($this->redirectTo) {
                        $this->redirect($this->redirectTo);
                    } else {
                        $this->redirect(array('view', 'id' => $model->id));
                    }
                } catch (Exception $err) {
                    $transaction->rollback();
                    Yii::app()->getUser()->setFlash(
                        TbHtml::ALERT_COLOR_ERROR, 
                        Yii::t('common', 'System error: ' . $err->getMessage() . '.')
                    );
                }
            }
        }

        $this->render('update', array_merge(array('model' => $model), $this->params));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // we only allow deletion via POST request
            $this->loadModel($this->modelName, $id)->delete();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider($this->modelName);
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin()
    {
        $model = new $this->modelName('search');
        if ($this->scenario) {
            $model->scenario = $this->scenario;
        }
        if ($this->with) {
            $model = $model->with($this->with);
        }
        $model->resetScope();

        $model->unsetAttributes();  // clear any default values
        if (isset($_GET[$this->modelName])) {
            $model->setAttributes($_GET[$this->modelName]);
        }
        $this->render('admin', array_merge(array('model' => $model), $this->params));
    }

//    public function loadModel($id = null)
//    {
//        $model = new $this->modelName;
//
//        if ($id !== null) {
//            if ($this->with) {
//                $model = $model->resetScope()->with($this->with)->findByPk($id);
//            } else {
//                $model = $model->resetScope()->findByPk($id);
//            }
//        }
//        if ($this->scenario) {
//            $model->scenario = $this->scenario;
//        }
//
//        if ($model === null)
//            throw new CHttpException(404, 'The requested page does not exist.');
//        return $model;
//    }

    public function loadModelWith($with)
    {
        if (isset($_GET['id'])) {
            $model = new $this->modelName;
            if ($this->scenario) {
                $model->scenario = $this->scenario;
            }
            if ($model === null) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model->resetScope()->with($with)->findByPk($_GET['id']);
        }
    }

    public function actionMove($id, $dir)
    {
        $model = $this->loadModel($this->modelName, $id);

        if ($dir == 'up' || $dir == 'down') {
            return $model->moveEntry($dir);
        } 
        return $model;
    }

    public function getMinSorter()
    {
        $model = new $this->modelName;
        $minSorter = Yii::app()->db->createCommand()
            ->select('MIN(sorter) as maxSorter')
            ->from($model->tableName())
            ->queryScalar();
        $this->params['minSorter'] = $minSorter;
        return $minSorter;
    }

    public static function returnStatusHtml($data, $tableId, $onclick = 0, $ignore = 0)
    {
        if ($ignore) {
            if (is_array($ignore)) {
                if (in_array($data->id, $ignore)) {
                    return '';
                }
            } else {
                if ($data->id == $ignore) {
                    return '';
                }
            }
        }

        switch ($tableId) {
            case 'images-grid':
                $url = Yii::app()->controller->createUrl("/catalog/backend/images/activate", array("id"     => $data->id, 'action' => ($data->active == 1 ? 'deactivate' : 'activate')));
                break;
            default:
                $url = Yii::app()->controller->createUrl("activate", array("id"     => $data->id, 'action' => ($data->active == 1 ? 'deactivate' : 'activate')));
                break;
        }

        $img = CHtml::image(
                Yii::app()->request->baseUrl . '/images/' . ($data->active ? '' : 'in') . 'active.png', Yii::t('common', $data->active ? 'Inactive' : 'Active'), array('title' => Yii::t('common', $data->active ? 'Деактивировать' : 'Активировать'))
        );
        $options = array();
        if ($onclick) {
            $options = array(
                'onclick' => 'ajaxSetStatus(this, "' . $tableId . '"); return false;',
            );
        }
        return '<div align="center">' . CHtml::link($img, $url, $options) . '</div>';
    }

    public function actionActivate()
    {
        $field = isset($_GET['field']) ? $_GET['field'] : 'active';

        if (isset($_GET['id']) && isset($_GET['action'])) {
            $action = $_GET['action'];
            $model = $this->loadModel($this->modelName, $_GET['id']);
            if ($this->scenario) {
                $model->scenario = $this->scenario;
            }
            if ($model) {
                $model->$field = ($action == 'activate' ? 1 : 0);
                $model->update(array($field));
            }
        }
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    public function actionItemsSelected()
    {
        $idsSelected = Yii::app()->request->getPost('itemsSelected');

        $work = Yii::app()->request->getPost('workWithItemsSelected');

        if ($idsSelected && is_array($idsSelected) && $work) {
            $idsSelected = array_map('intval', $idsSelected);

            foreach ($idsSelected as $id) {
                $model = $this->loadModel($id);
                $model->scenario = 'changeStatus';

                if ($work == 'delete') {
                    $model->delete();
                } elseif ($work == 'activate') {
                    $model->active = 1;
                    $model->update('active');
                } elseif ($work == 'deactivate') {
                    $model->active = 0;
                    $model->update('active');
                }
            }
        }

        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    protected function rememberPage()
    {
        // persist page number
        $pageParam = $this->modelName . '_page';
        if (isset($_GET[$pageParam])) {
            $page = $_GET[$pageParam];
            Yii::app()->user->setState($this->id . '-page', (int) $page);
        } else {
            $page = 1;
            $_GET[$pageParam] = $page;
        }

        $currentSection = Yii::app()->request->getQuery('section_filter');
        $pageParam = $this->modelName . '_section_filter';

        if ($currentSection && Yii::app()->user->getState($pageParam)) {
            if (Yii::app()->user->getState($pageParam) != $currentSection) {
                Yii::app()->user->setState($pageParam, $currentSection);
            } else {
                $currentSection = Yii::app()->user->getState($pageParam);
            }
        } elseif ($currentSection && !Yii::app()->user->getState($pageParam)) {
            Yii::app()->user->setState($pageParam, $currentSection);
        } elseif (!$currentSection && Yii::app()->user->getState($pageParam)) {
            $currentSection = Yii::app()->user->getState($pageParam);
        } else {
            $currentSection = 'all';
        }

        $this->params['currentSection'] = $_GET['section_filter'] = $currentSection;
    }

    public function actionMoveImage()
    {
        if (isset($_GET['id']) && isset($_GET['direction'])) {
            $direction = isset($_GET['direction']) ? $_GET['direction'] : '';

            $model = $this->loadModel($_GET['id']);

            if ($model && ($direction == 'up' || $direction == 'down')) {
                $sorter = $model->sorter;

                if ($direction == 'up') {
                    if ($sorter > 1) {
                        $sql = 'UPDATE ' . $model->tableName() . ' SET sorter="' . $sorter . '" WHERE sorter < "' . ($sorter) . '" AND pid = "' . $model->pid . '" ORDER BY sorter DESC LIMIT 1';
                        Yii::app()->db->createCommand($sql)->execute();
                        $model->sorter--;
                        $model->save(false);
                    }
                }
                if ($direction == 'down') {
                    $maxSorter = Yii::app()->db->createCommand()
                        ->select('MAX(sorter) as maxSorter')
                        ->from($model->tableName())
                        ->where('pid = "' . $model->pid . '"')
                        ->queryScalar();

                    if ($sorter < $maxSorter) {
                        $sql = 'UPDATE ' . $model->tableName() . ' SET sorter="' . $sorter . '" WHERE sorter > "' . ($sorter) . '" AND pid = "' . $model->pid . '" ORDER BY sorter ASC LIMIT 1';
                        Yii::app()->db->createCommand($sql)->execute();
                        $model->sorter++;
                        $model->save(false);
                    }
                }
            }
        }
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    public function getMaxSorterImage($pid)
    {
        $model = new $this->modelName;
        $maxSorter = Yii::app()->db->createCommand()
            ->select('MAX(sorter) as maxSorter')
            ->from('{{catalog_images}}')
            ->where('pid = "' . $pid . '"')
            ->queryScalar();
        $this->params['maxSorter'] = $maxSorter;
        return $maxSorter;
    }

    public function getMinSorterImage($pid)
    {
        $model = new $this->modelName;
        $minSorter = Yii::app()->db->createCommand()
            ->select('MIN(sorter) as maxSorter')
            ->from('{{catalog_images}}')
            ->where('pid = "' . $pid . '"')
            ->queryScalar();
        $this->params['minSorter'] = $minSorter;
        return $minSorter;
    }

}
