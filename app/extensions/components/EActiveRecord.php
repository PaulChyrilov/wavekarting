<?php

/**
 * EActiveRecord class
 *
 * Some cool methods to share amount your models
 */
class EActiveRecord extends CActiveRecord
{
    
    const MOVE_UP = 'up';
    const MOVE_DOWN = 'down';
    
    public $dirtyAttributes;
    
    protected $_nestedRelations = array();
    
    protected $_oldValues = array();
    
    
    /**
     * default form ID for the current model. Defaults to get_class()+'-form'
     */
    private $_formId;

    public function setFormId($value)
    {
        $this->_formId = $value;
    }

    public function getFormId()
    {
        if (null !== $this->_formId)
            return $this->_formId;
        else {
            $this->_formId = strtolower(get_class($this)) . '-form';
            return $this->_formId;
        }
    }

    /**
     * default grid ID for the current model. Defaults to get_class()+'-grid'
     */
    private $_gridId;

    public function setGridId($value)
    {
        $this->_gridId = $value;
    }

    public function getGridId($prefix = true)
    {
        if (null !== $this->_gridId)
            return $this->_gridId;
        else {
            $this->_gridId = strtolower(get_class($this)) . ($prefix ? "-{$this->id}" : '')  . '-grid';
            return $this->_gridId;
        }
    }

    /**
     * default list ID for the current model. Defaults to get_class()+'-list'
     */
    private $_listId;

    public function setListId($value)
    {
        $this->_listId = $value;
    }

    public function getListId()
    {
        if (null !== $this->_listId)
            return $this->_listId;
        else {
            $this->_listId = strtolower(get_class($this)) . '-list';
            return $this->_listId;
        }
    }
    
    public function moveEntry($action)
    {
        $criteria = new CDbCriteria;

        if ($action == self::MOVE_UP) {
            $order = $this->order - 1;
            $criteria->addCondition('`order` = ' . $order);
            $this->updateCounters(array('order' => 1), $criteria);
            $this->saveCounters(array('order' => -1));
        } elseif ($action == self::MOVE_DOWN) {
            $order = $this->order + 1;
            $criteria->addCondition('`order` = ' . $order);
            $this->updateCounters(array('order' => -1), $criteria);
            $this->saveCounters(array('order' => 1));
        }
        return $this;
    }
    
    public function getMaxOrder()
    {
        return Yii::app()->getDb()->createCommand()
            ->select('MAX(`order`) as maxOrder')
            ->from($this->tableName())
            ->queryScalar();
    }
    
    public function populateRecord($attributes, $callAfterFind = true)
	{
		$record = parent::populateRecord($attributes, $callAfterFind);
        if ($record !== null) {
            $record->setOldValues($record->getAttributes());
        }
        return $record;
	}
    
    public function init()
	{
        $this->setOldValues($this->getAttributes());
	}
    
    public function setOldValues($attributes)
    {
        $this->_oldValues = $attributes;
    }
    
    public function getOldValue($attribute)
    {
        if (array_key_exists($attribute, $this->_oldValues)) {
            return $this->_oldValues[$attribute];
        }
        return null;
    }
    
    /**
     * Check object to dirty
     * @return boolean 
     */
    public function isDirty()
    {
        foreach ($this->getAttributes() as $name => $value) {
            if ($this->dirtyAttributes !== null && !in_array($name, $this->dirtyAttributes)) {
                continue;
            }
            if ((!isset($this->_oldValues[$name]) && !array_key_exists($name, $this->_oldValues)) || 
                $this->_oldValues[$name] != $value
            ) {                
                return true;
            }
        }
        return false;
    }
    
    /**
     *
     * @param string $relation
     * @param mixed $data
     * @throws Exception 
     */
    public function setNestedRelation($relation, $data) 
    {
        $modelRelations = $this->relations();
        if (!isset($modelRelations[$relation])) {
            throw new Exception('Could not find "' . $relation . '" relation.');
        } 
        $rel = $modelRelations[$relation];
        if ($rel[0] === self::HAS_MANY) {
            $this->_nestedRelations[$relation] = $rel;
            $this->_setNestedManyRelation($relation, $data, $rel);
        }
    }
    
    /**
     *
     * @param array $data
     * @throws Exception 
     */
    protected function _setNestedManyRelation($relation, $data, $relationData)
    {
        $rels = array();
        $modelClass = $relationData[1];
        $index = isset($relationData['index']) ? $relationData['index'] : 'id';
        
        if ($this->getIsNewRecord()) {
            foreach ($data as $attr) {
                if (!empty($attr['_destroy'])) continue;
                $q = new $modelClass();
                $q->setAttributes($attr);                    
                $rels[$attr[$index]] = $q;                
            }            
        } else {
            $relationIds = array();
            foreach ($data as $attr) {
                if (!empty($attr[$index])) {
                    $relationIds[] = $attr[$index]; 
                }                
            }
            if (count($relationIds) > 0) {
                $relationIds = $this->getRelated($relation, false, array(
                    'condition' => '`' . $index . '` IN ("' . implode('","', $relationIds) . '")',
                    'index'     => $index
                ));
            }
            foreach ($data as $attr) {
                if (!empty($attr[$index]) && isset($relationIds[$attr[$index]])) {
                    $q = $relationIds[$attr[$index]];
                    if (!empty($attr['_destroy'])) {
                        $q->delete();
                        continue;
                    }
                    $q->setAttributes($attr);
                    $rels[$attr[$index]] = $q;
                } elseif (empty($attr['_destroy'])) {
                    $q = new $modelClass();
                    $q->setAttributes($attr);
                    $rels[$attr[$index]] = $q;
                }             
            }            
        }
        $this->$relation = $rels;
    }
    
    /**
     *
     * @param mixed $attributes
     * @param boolean $clearErrors
     * @return boolean 
     */
    public function validate($attributes = null, $clearErrors = true)
    {
        $valid = true;
        if (!empty($this->_nestedRelations)) {
            foreach ($this->_nestedRelations as $rel => $option) {
                if ($option[0] === self::HAS_MANY || $option[0] === self::MANY_MANY) {
                    foreach ($this->$rel as $r) {
                        if ($r->isDirty()) {
                            $valid &= $r->validate();                            
                        }
                    }
                } else {
                    $valid &= $this->{$rel}->validate();
                }
                
            }
        }
        return parent::validate($attributes, $clearErrors) && $valid;
    }
    
    /**
     * Custom after save 
     */
    protected function afterSave()
    {
        if (!empty($this->_nestedRelations)) {
            foreach ($this->_nestedRelations as $rel => $option) {
                if ($option[0] === self::HAS_MANY) {
                    foreach ($this->$rel as $r) {
                        $r->{$option[2]} = $this->getPrimaryKey();
                        $r->isDirty() && $r->save(false);
                    }                    
                }
            }           
        }
        parent::afterSave();
    }
    
    public static function sitemap($class, $lang = null)
    {
        $map = array();
        foreach (CActiveRecord::model($class)->withLang($lang)->findAll(array('select' => 'id, slug, updated_at')) as $model) {
            $map[] = array(
                'id' => $model->id,
                'url' => $model->getUrl(),
                'title' => $model->getI18n('title'),
                'updated_at' => $model->updated_at
            );
        }
        return $map;
    }
    
    /**
     * Logs the record update information.
     * Updates the four columns: create_user_id, create_date, last_update_user_id and last_update_date.
     */
    protected function logUpdate()
    {
        $userId = php_sapi_name() === 'cli' ? -1 : Yii::app()->getUser()->getId();

        foreach (array('create_user_id' => $userId, 'create_date'    => time()) as $attribute => $value)
            $this->updateLogAttribute($attribute, $value, (!($userId === -1 || Yii::app()->user->isGuest) && $this->isNewRecord));

        foreach (array('last_update_user_id' => $userId, 'last_update_date'    => time()) as $attribute => $value)
            $this->updateLogAttribute($attribute, $value, (!($userId === -1 || Yii::app()->user->isGuest) && !$this->isNewRecord));
    }

    /**
     * Helper function to update attributes
     * @param $attribute
     * @param $value
     * @param $check
     */
    protected function updateLogAttribute($attribute, $value, $check)
    {

        if ($this->hasAttribute($attribute) && $check)
            $this->$attribute = $value;
    }
}
