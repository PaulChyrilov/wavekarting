<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    const ERROR_EMAIL_OR_PASSWORD = '10';
    const ERROR_DEACTIVATED       = '11';
    const ERROR_LOCKED_PASSWORD   = '12';
    const ERROR_USER_LOCKED       = '13';
    const ERROR_LOCKED_BY_SYSTEM  = '14';    
    const WRONG_PASSWORD_ATTEMPT = 3;
    
    private $_id;
    
    /**
     * @var User 
     */
    private $_user;

    public function authenticate()
    {
        $this->errorCode = self::ERROR_NONE;
        $this->_id = 1;
        return true;
        
        $this->_user = User::model()->find('email = :username', array(':username' => $this->username));
        if ($this->_user === null || trim($this->_user->password) === '' || $this->_user->is_guest) {
            $this->errorCode = self::ERROR_EMAIL_OR_PASSWORD;
        } elseif (!$this->_user->is_activated) {
            $this->errorCode = self::ERROR_DEACTIVATED;
        } elseif ($this->_user->is_locked_password) {
            $this->errorCode = self::ERROR_LOCKED_PASSWORD;
        } elseif ($this->_user->is_locked_system) {
            $this->errorCode = self::ERROR_LOCKED_BY_SYSTEM;
        } elseif ($this->_user->password !== HUtil::encryptPassword($this->password)) {
            $this->errorCode = self::ERROR_EMAIL_OR_PASSWORD;
            $this->_user->wrong_password_counter += 1;
            if ($this->_user->wrong_password_counter == self::WRONG_PASSWORD_ATTEMPT) {
                $this->_user->wrong_password_token = HUtil::generateCode();
                $this->_user->is_locked_password = time();
                $this->errorCode = self::ERROR_USER_LOCKED;
            }
            $this->_user->save(false, array('wrong_password_counter', 'wrong_password_token', 'is_locked_password', 'modified'));
        } else {
            $this->_id = $this->_user->id;
            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }
    
    /**
     * @return User 
     */
    public function getUser()
    {
        return $this->_user;
    }
}