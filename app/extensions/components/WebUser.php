<?php

class WebUser extends CWebUser
{
    private $_model = null;
    
    /**
     * Get user model
     * @return User 
     */
    public function getModel()
    {
        if (!$this->getIsGuest() && $this->_model === null) {
            $this->_model = User::getById($this->getId());
        }
        return $this->_model;
    }
}