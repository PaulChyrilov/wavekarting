<?php

class ChangePasswordForm extends CFormModel
{
    public $needActualPassword = false;
    
    public $password;
    public $repeatPassword;
    public $actualPassword;
    
    /**
     * @var User 
     */
    private $_u;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        $rules = array(
            array('password, repeatPassword', 'required'),                        
            array('password, repeatPassword', 'length', 'min' => 5),
            array(
                'password', 'compare', 
                'compareAttribute' => 'repeatPassword',
            ),
        );
        if ($this->needActualPassword) {
            $rules[] = array('actualPassword', 'required');
            $rules[] = array('actualPassword', 'verify');            
        }
    }
    
    /**
     * Should be changed if needed
     * @param type $attribute
     * @param type $params 
     */
    public function verify($attribute, $params)
    {
        if ($this->_u->password !== md5($this->actualPassword)) {
            $this->addError('actualPassword', 'Actual password is incorrect');
        }
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'password'       => 'Password',
            'repeatPassword' => 'Password Repeat',
            'actualPassword' => 'Actual Password'
        );
    }
    
    /**
     * Unset model values 
     */
    public function refresh()
    {
        $this->actualPassword = $this->password = $this->repeatPassword = null;
    }
    
    /**
     * @return User
     */
    public function getUser()
    {
        return $this->_u;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->_u = $user;
        return $this;
    }
    
    /**
     * 
     * @return boolean
     */
    public function perform()
    {     
        if ($this->validate()) {
            return false;
        }        
        $this->_u->password = md5($this->password);
        $this->_u->forgot_password_token = null;
        
        return $this->_u->save(false, array('password', 'forgot_password_token'));
    }

}