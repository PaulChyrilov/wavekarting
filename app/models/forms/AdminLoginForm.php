<?php

/**
 * Login form 
 */
class AdminLoginForm extends CFormModel
{

    public $username;
    public $password;
    public $rememberMe;
    private $_i;

        /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('username, password', 'required'),
            array('username', 'email'),
            // rememberMe needs to be a boolean
            array('rememberMe', 'boolean'),
            // password needs to be authenticated
            array('password', 'authenticate'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'username'   => 'Email',
            'password'   => 'Password',
            'rememberMe' => 'Remember me',
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->_i = new AdminUserIdentity($this->username, $this->password);
            if (!$this->_i->authenticate())
                if ($this->_i === AdminUserIdentity::ERROR_ACCOUNT_BLOCKED) {
                    $this->addError('password', 'Your account is blocked.');                    
                } else {
                    $this->addError('password', 'Incorrect username or password.');                    
                }
            }
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {
        if ($this->_i === null) {
            $this->_i = new AdminUserIdentity($this->username, $this->password);
            $this->_i->authenticate();
        }
        if ($this->_i->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
            Yii::app()->getUser()->login($this->_i, $duration);
            return true;
        }
        return false;
    }

}
