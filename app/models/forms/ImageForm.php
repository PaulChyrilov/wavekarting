<?php
/**
 * Description of FileForm
 *
 * @author mlapko
 */
class ImageForm extends CFormModel 
{
    /**
     * @var CUploadedFile 
     */
    public $file;
    
    protected $_url;

    public function rules() 
    {
        return array(
            array(
                'file', 'ext.components.image_processor.MImageValidator', 'allowEmpty' => false,
                'types' => array('jpg', 'png', 'jpeg'), 'maxSize' => 5 * 1024 * 1024, 
//                'minWidth' => 100, 'minHeight' => 100
            ),
        );
    }
    
    public function perform()
    {
        $this->file = CUploadedFile::getInstanceByName('file');
        if ($this->validate()) {
            $imageName = uniqid() . '.' . $this->file->getExtensionName();
            $image = Yii::app()->getComponent('image');
            $image->getImageHandler()
                ->load($this->file->getTempName())
                ->resize(800, false)
                ->save($image->createDir('/redactor') . '/' . $imageName);
            $this->_url = $image->imageUrl . 'redactor/' . $imageName;
            return true;
        }
        return false;
    }
    
    public function getUrl()
    {
        return $this->_url;
    }
}