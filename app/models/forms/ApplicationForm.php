<?php

class ApplicationForm extends CFormModel
{
    public $email;
    public $username;
    public $phone;
    public $description;
    public $file;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
           array('email, username, phone', 'required'),
           array('description', 'safe'),
           array('file', 'file', 'allowEmpty' => true),
           array('email', 'email'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'email' => 'E-mail',
            'username' => 'Username',
            'phone' => 'Phone',
            'description' => 'A few word about you',
            'file' => 'Upload CV',
        );
    }
    
    /**
     * Need to refactor
     * @return boolean
     */
    public function perform()
    {
        if ($this->validate()) {
            $mail = Yii::app()->getModule('mail');
            $message = $mail->createMessage();
            $message->addTo(param('application_form.email'));
            
            $message->setBody('application_form', array(
                '{email}' => $this->email,
                '{username}' => $this->username,
                '{phone}' => $this->phone,
                '{description}' => $this->description,
            ));
            if (($file = CUploadedFile::getInstance($this, 'file')) !== null) {
                $message->attach($mail->createAttach($file->getTempName(), $file->getName()));
            }
            $mail->send($message);
            return true;            
        }
        return false;
    }
}