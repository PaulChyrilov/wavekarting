<?php

/**
 * Login form 
 */
class LoginForm extends CFormModel
{

    public $username;
    public $password;
    public $rememberMe;
    private $_i;
    
    /**
     * @var CController 
     */
    private $_controller;
    
    public function setController($controller)
    {
        $this->_controller = $controller;
    }

        /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('username, password', 'required'),
            array('username', 'email'),
            // rememberMe needs to be a boolean
            array('rememberMe', 'boolean'),
            // password needs to be authenticated
            array('password', 'authenticate'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'username'   => 'Email',
            'password'   => 'Password',
            'rememberMe' => 'Remember me',
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->_i = new UserIdentity($this->username, $this->password);
            if (!$this->_i->authenticate()) {
                $error = $this->_i->errorCode;
                $user = $this->_i->getUser();
                if ($error === UserIdentity::ERROR_EMAIL_OR_PASSWORD) {
                    $this->addError('password', Yii::t('backend', 'Incorrect email or password.'));
                } elseif ($error === UserIdentity::ERROR_DEACTIVATED) {
                    HUtil::setPopupAction($user->is_email_changed ? 'emailchanged' : 'inactive', array('id' => $user->id));
                } elseif ($error === UserIdentity::ERROR_LOCKED_PASSWORD) {
                    $this->addError(
                        'password', 
                        Yii::t('backend', 'This account is locked. Please follow link {SEND_AGAIN_EMAIL_LINK}.', array(
                            '{SEND_AGAIN_EMAIL_LINK}' => Yii::app()->createUrl('/users/resendunlockpasswordemail', array('id' => $user->id))                            
                        )) 
                    );
                } elseif ($error === UserIdentity::ERROR_LOCKED_BY_SYSTEM) {
                    $this->addError('password', Yii::t('backend', 'The account was locked by sytem.'));
                } elseif ($error === UserIdentity::ERROR_USER_LOCKED) { // if user has last wrong attempt and locked now
                    $message = new SFMailMessage;        
                    $link = $this->_controller->createAbsoluteUrl(
                        '/users/unlockpassword', array('code' => $user->wrong_password_token)
                    );
                    $message->setBody(User::MAIL_UNLOCK_PASSWORD, null, array(
                        '{REAL_NAME}' => $user->real_name,
                        '{LINK}'      => CHtml::link($link, $link)
                    ));
                    $message->addTo($user->email);
                    Yii::app()->mail->send($message);
                    $this->addError('password', Yii::t('backend', 'The account locked.'));
                }               
            }
        }
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {
        if ($this->_i === null) {
            $this->_i = new UserIdentity($this->username, $this->password);
            $this->_i->authenticate();
        }
        if ($this->_i->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
            Yii::app()->getUser()->login($this->_i, $duration);
            return true;
        }
        return false;
    }

}
