<?php

class ForgotPasswordForm extends CFormModel
{
    public $email;
    
    private $_u;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
           array('email', 'required'),
           array('email', 'email'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'email' => 'Email',
        );
    }
    
    /**
     * Need to refactor
     * @return boolean
     */
    public function perform()
    {
        if ($this->validate() && ($this->_u = User::model()->findByAttributes(array('email' => $this->email))) !== null) {
            $this->_u->updateByPk($this->_u->id, array('forgot_password_token' => uniqid()));
            return true;            
        }
        return false;
    }
    
    /**
     * @return User
     */
    public function getUser()
    {
        return $this->_u;
    }
}