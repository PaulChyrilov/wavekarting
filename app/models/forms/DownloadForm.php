<?php

/**
 * Login form 
 */
class DownloadForm extends CFormModel
{

    public $firstname;
    public $lastname;
    public $email;

        /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('firstname, lastname, email', 'required'),
            array('email', 'email'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'firstname'   => 'First Name',
            'lastname'   => 'Last Name',
            'email' => 'Email',
        );
    }

}
