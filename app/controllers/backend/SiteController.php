<?php
/**
 *
 * SiteController class
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
class SiteController extends EController
{
	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
    
    public function actionLogin()
    {
        if (!$this->getUser()->getIsGuest()) {
            $this->redirect(array('site/index'));
        }
        $model = new AdminLoginForm;        
        if (isset($_POST['AdminLoginForm'])) {
            $model->setAttributes($_POST['AdminLoginForm']);
            if ($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->getUser()->getReturnUrl());
            }                
        }
        $this->render('login', array('model' => $model));
    }
    
    public function actionLogout()
    {
        $this->getUser()->logout();
		$this->redirect(array('login'));
    }
    
    public function actionResetCache()
    {
        var_dump(Yii::app()->getCache()->flush());
    }
        
}