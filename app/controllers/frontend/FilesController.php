<?php
/**
 *
 * FilesController class
 *
 */
class FilesController extends EController
{
    public function actionLogin()
    {
        $model = new DownloadForm;
        $session = Yii::app()->getSession();
        $session->add('user_files', null);
        if (isset($_POST['DownloadForm'])) {
            $model->setAttributes($_POST['DownloadForm']);
            if ($model->validate()) {
                $session->add('user_files', $model->getAttributes());
                $this->forward('industry');
                Yii::app()->end();
            }
        }
        $this->render('step1', compact('model'));
    }
    
    public function actionIndustry()
    {
        $this->_checkSession();
        if (!empty($_POST['industry'])) {
            $session = Yii::app()->getSession();            
            $session->add('user_files', array_merge(
                $session->get('user_files'),
                array('industry' => $_POST['industry'])
            ));
            $this->forward('type');
        }
        $this->render('step2');
    }
    
    public function actionType()
    {
        $this->_checkSession();
        if (!empty($_POST['type'])) {
            $session = Yii::app()->getSession();            
            $session->add('user_files', array_merge(
                $session->get('user_files'),
                array('type' => $_POST['type'])
            ));
            $this->forward('download');
        }
        $this->render('step3');
    }
    
    public function actionDownload()
    {
        $this->_checkSession();
        $session = Yii::app()->getSession();
        $data = $session->get('user_files');
        $manager = Yii::app()->getModule('filemanager');
        if (isset($_POST['files']) && ($result = $manager->download($_POST['files'])) !== false) {
            
            $mail = Yii::app()->getModule('mail');
            $message = $mail->createMessage();
            $message->addTo(param('download_file.email'));
            
            $fileTitles = array_map(function($item) {
                return $item->getI18n('title') . ' (' . $item->file . ')';
            }, $result['files']);
            
            $industry = Yii::app()->getModule('categories')->getByCode($data['industry']);
            $message->setBody('download_file', array(
                '{email}' => $data['email'],
                '{firstname}' => $data['firstname'],
                '{lastname}' => $data['lastname'],
                '{industry}' => $industry ? $industry->getI18n('name') : 'none',
                '{files}' => implode(', ', $fileTitles)
            ));
            $mail->send($message);
            
            $session->add('user_files', null);
            Yii::app()->getRequest()->sendFile($result['filename'], $result['content']);
            Yii::app()->end();
        }
        $this->render('step4', array(
            'files' => $manager->getFiles($data['industry'], $data['type'])
        ));
    }
    
    public function actionThankyou()
    {
        $this->render('step5');
    }


    protected function _checkSession()
    {
        if (!Yii::app()->getSession()->get('user_files')) {
            $this->redirect(Yii::app()->getHomeUrl());
        }
    }

}