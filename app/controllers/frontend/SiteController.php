<?php
/**
 *
 * SiteController class
 *
 */
class SiteController extends EController
{

    public function actionIndex()
    {
        $this->render('index');
    }
    
    public function actionSitemap()
    {
        $app = Yii::app();
        if (($xml = $app->getCache()->get('sitemap-xml')) === false) {    
            
            $sitemap = new Sitemap();
            
            $data = $this->_getSitemapData();
            
            $sitemap->addModels($data['blog'], Sitemap::DAILY, 0.8);
            $sitemap->addModels($data['pages'], Sitemap::WEEKLY, 0.3);
            $sitemap->addModels($data['solutions'], Sitemap::WEEKLY, 0.6);
            $sitemap->addModels($data['services'], Sitemap::WEEKLY, 0.6);
            $sitemap->addModels($data['careers'], Sitemap::WEEKLY);
 
            $xml = $sitemap->render();
            $app->getCache()->set('sitemap-xml', $xml, 3600 * 6);
        }
        header("Content-type: text/xml");
        echo $xml;
        Yii::app()->end();
    }
    
    public function actionMap() 
    {
        $this->render('sitemap', array('data' => $this->_getSitemapData()));
    }
    
    protected function _getSitemapData()
    {
        $app = Yii::app();
        $lang = $app->getLanguage();
        $key = 'sitemap-data-' . $lang; 
        if (($result = $app->getCache()->get($key)) === false) {
            $mapping = array(
                'blog' => 'Post',
                'pages' => 'Page',
                'solutions' => 'Solution',
                'careers' => 'Career',
                'services' => 'Service',
            );
            $result = array();
            foreach ($mapping as $key => $className) {
                $app->getModule($key);
                $result[$key] = $className::sitemap($className);
            }
            $app->getCache()->set($key, $result, 3600 * 6);
        }
        return $result;
    }
    
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionLogin()
    {
        if (!$this->getUser()->getIsGuest()) {
            $this->redirect(array('site/index'));
        }
        $model = new LoginForm;
        if (isset($_POST['LoginForm'])) {
            $model->setAttributes($_POST['LoginForm']);
            if ($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->getUser()->getReturnUrl());
            }
        }
        $this->render('login', array('model' => $model));
    }
    
    public function actionLogout()
    {
        $this->getUser()->logout();
        $this->redirect(Yii::app()->getHomeUrl());
    }
    
    public function actionForgotPassword()
    {
        $model = new ForgotPasswordForm;
        if (isset($_POST['ForgotPasswordForm'])) {
            $model->setAttributes($_POST['ForgotPasswordForm']);
            if ($model->validate()) {
                
            }
        }
        $this->render('forgot_password', compact('model'));
    }
    
    public function actionSearch($q)
    {
        $q = trim($q);
        $this->render('search', array('q' => $q, 'data' => HApplication::search($q)));
    }

    public function actionFaq()
    {
        $this->render('faq');
    }
}