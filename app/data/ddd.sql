/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50531
Source Host           : localhost:3306
Source Database       : ddd

Target Server Type    : MYSQL
Target Server Version : 50531
File Encoding         : 65001

Date: 2013-06-03 11:07:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admins`
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email_idx` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES ('1', 'Main Admin', 'admin@admin.com', '$2a$10$JTJf6/XqC94rrOtzuF397OHa4mbmZrVTBOQCmYD9U.obZRUut4BoC', '1', '2013-05-29 11:33:45', '2013-05-29 11:33:45');

-- ----------------------------
-- Table structure for `career_i18ns`
-- ----------------------------
DROP TABLE IF EXISTS `career_i18ns`;
CREATE TABLE `career_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `short_content` text NOT NULL,
  `meta_title` varchar(255) DEFAULT '',
  `meta_keywords` text,
  `meta_description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`),
  KEY `parent_id_idx` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of career_i18ns
-- ----------------------------
INSERT INTO `career_i18ns` VALUES ('1', 'en', '1', 'Audit Committee', '<p>Company to populare beliorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief<br></p><p>Company to populare beliorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief<br></p>', 'Company to populare beliorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief', '', '', '');

-- ----------------------------
-- Table structure for `careers`
-- ----------------------------
DROP TABLE IF EXISTS `careers`;
CREATE TABLE `careers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_idx` (`order`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of careers
-- ----------------------------
INSERT INTO `careers` VALUES ('1', '', 'audit-committee', '1', '2013-05-30 15:45:03', '2013-05-30 15:45:03');

-- ----------------------------
-- Table structure for `categories`
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code_idx` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'page_category', 'Page categories', '2013-05-29 12:25:44');
INSERT INTO `categories` VALUES ('2', 'blog_category', 'Blog Categories', '2013-05-29 12:25:56');
INSERT INTO `categories` VALUES ('3', 'service_category', 'Service categories', '2013-05-29 16:14:40');

-- ----------------------------
-- Table structure for `category_item_i18ns`
-- ----------------------------
DROP TABLE IF EXISTS `category_item_i18ns`;
CREATE TABLE `category_item_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `lang_id` varchar(2) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`),
  KEY `parent_id_idx` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category_item_i18ns
-- ----------------------------
INSERT INTO `category_item_i18ns` VALUES ('1', '1', 'en', 'root');
INSERT INTO `category_item_i18ns` VALUES ('2', '2', 'en', 'root');
INSERT INTO `category_item_i18ns` VALUES ('3', '3', 'en', 'root');
INSERT INTO `category_item_i18ns` VALUES ('4', '4', 'en', 'Production');
INSERT INTO `category_item_i18ns` VALUES ('5', '5', 'en', 'Custom');
INSERT INTO `category_item_i18ns` VALUES ('6', '6', 'en', 'First category');
INSERT INTO `category_item_i18ns` VALUES ('7', '7', 'en', 'Second category');
INSERT INTO `category_item_i18ns` VALUES ('8', '8', 'en', 'About');

-- ----------------------------
-- Table structure for `category_items`
-- ----------------------------
DROP TABLE IF EXISTS `category_items`;
CREATE TABLE `category_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `root` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id_idx` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category_items
-- ----------------------------
INSERT INTO `category_items` VALUES ('1', '1', 'root', '', '1', '1', '4', '1', '2013-05-29 12:25:44');
INSERT INTO `category_items` VALUES ('2', '2', 'root', '', '2', '1', '6', '1', '2013-05-29 12:25:56');
INSERT INTO `category_items` VALUES ('3', '3', 'root', '', '3', '1', '6', '1', '2013-05-29 16:14:40');
INSERT INTO `category_items` VALUES ('4', '3', 'production', '', '3', '2', '3', '2', '2013-05-29 16:14:58');
INSERT INTO `category_items` VALUES ('5', '3', 'custom', '', '3', '4', '5', '2', '2013-05-29 16:15:07');
INSERT INTO `category_items` VALUES ('6', '2', 'first-category', '', '2', '2', '3', '2', '2013-05-29 18:23:59');
INSERT INTO `category_items` VALUES ('7', '2', 'second-category', '', '2', '4', '5', '2', '2013-05-29 18:24:15');
INSERT INTO `category_items` VALUES ('8', '1', 'about', '', '1', '2', '3', '2', '2013-05-31 11:57:07');

-- ----------------------------
-- Table structure for `content_block_i18ns`
-- ----------------------------
DROP TABLE IF EXISTS `content_block_i18ns`;
CREATE TABLE `content_block_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`),
  KEY `parent_id_idx` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content_block_i18ns
-- ----------------------------
INSERT INTO `content_block_i18ns` VALUES ('1', 'en', '1', '<div class=\"col3\">\r\n                <article>\r\n                    <h2 class=\"our-services\">OUR <span>SERVICES</span></h2>\r\n                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting.Lorem Ipsum is simply dummy text of the printing and typesetting.Lorem Ipsum is simply dummy text of the printing and typesetting.Lorem Ipsum is simply dummy text of the printing and typesetting.</p>\r\n                    <a href=\"{SERVICE_URL}\">read more &raquo;</a>\r\n                </article>\r\n            </div>\r\n            <div class=\"col3\">\r\n                <article>\r\n                    <h2 class=\"our-solutions\">OUR <span>SOLUTION</span></h2>\r\n                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting.Lorem Ipsum is simply dummy text of the printing and typesetting.Lorem Ipsum is simply dummy text of the printing and typesetting.Lorem Ipsum is simply dummy text of the printing and typesetting.</p>\r\n                    <a href=\"{SOLUTION_URL}\">read more &raquo;</a>\r\n                </article>\r\n            </div>\r\n            <div class=\"col3\">\r\n                <article>\r\n                    <h2 class=\"our-techology\">OUR <span>technology</span></h2>\r\n                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting.Lorem Ipsum is simply dummy text of the printing and typesetting.Lorem Ipsum is simply dummy text of the printing and typesetting.Lorem Ipsum is simply dummy text of the printing and typesetting.</p>\r\n                    <a href=\"{TECNOLOGY_URL}\">read more &raquo;</a>\r\n                </article>\r\n            </div>');
INSERT INTO `content_block_i18ns` VALUES ('2', 'en', '2', '<div class=\"col3\">\r\n    <article class=\"offset30\">\r\n        <h2>Audit Committee</h2>\r\n        <p>Company to populare beliorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief</p>\r\n        <a href=\"{LOGIN_LINK}\" class=\"button\">login and download</a>\r\n    </article>\r\n</div>\r\n<div class=\"col3\">\r\n    <article class=\"offset30\">\r\n        <h2>Audit Committee</h2>\r\n        <p>Company to populare beliorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief</p>\r\n        <a href=\"{LOGIN_LINK}\" class=\"button\">login and download</a>\r\n    </article>\r\n</div>\r\n<div class=\"col3\">\r\n    <article class=\"offset30\">\r\n        <h2>Audit Committee</h2>\r\n        <p>Company to populare beliorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief</p>\r\n        <a href=\"{LOGIN_LINK}\" class=\"button\">login and download</a>\r\n    </article>\r\n</div>');
INSERT INTO `content_block_i18ns` VALUES ('3', 'en', '3', '<h3>Connect with Us</h3>\r\n<ul>\r\n    <li class=\"rss\"><a href=\"#\">RSS</a></li>\r\n    <li class=\"twitter\"><a href=\"#\">Twitter</a></li>\r\n    <li class=\"linkedin\"><a href=\"#\">Linkedin</a></li>\r\n    <li class=\"facebook\"><a href=\"#\">Facebook</a></li>\r\n    <li class=\"youtube\"><a href=\"#\">YouTube</a></li>\r\n</ul>');
INSERT INTO `content_block_i18ns` VALUES ('4', 'en', '4', '&copy; 2009-2013 All rights reserved.');
INSERT INTO `content_block_i18ns` VALUES ('5', 'en', '5', '<div style=\"display:inline-block;width: 85px;\">\r\n<div class=\"fb-like\" data-send=\"false\" data-layout=\"button_count\" data-width=\"250\" data-show-faces=\"true\"></div>\r\n</div>\r\n<script>(function(d, s, id) {\r\n  var js, fjs = d.getElementsByTagName(s)[0];\r\n  if (d.getElementById(id)) return;\r\n  js = d.createElement(s); js.id = id;\r\n  js.src = \"//connect.facebook.net/en_En/all.js#xfbml=1&appId=135369619847527\";\r\n  fjs.parentNode.insertBefore(js, fjs);\r\n}(document, \'script\', \'facebook-jssdk\'));</script>\r\n\r\n<!-- tweeter -->\r\n<div style=\"display:inline-block;width: 85px;\">\r\n<a  href=\"https://twitter.com/share\" class=\"twitter-share-button\">Tweet</a>\r\n</div>\r\n<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');</script>\r\n\r\n<!-- pinterest -->\r\n<a href=\"//pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F&media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg&description=Next%20stop%3A%20Pinterest\" data-pin-do=\"buttonPin\" data-pin-config=\"none\"><img src=\"//assets.pinterest.com/images/pidgets/pin_it_button.png\" /></a>\r\n<script type=\"text/javascript\">\r\n(function(d){\r\n  var f = d.getElementsByTagName(\'SCRIPT\')[0], p = d.createElement(\'SCRIPT\');\r\n  p.type = \'text/javascript\';\r\n  p.async = true;\r\n  p.src = \'//assets.pinterest.com/js/pinit.js\';\r\n  f.parentNode.insertBefore(p, f);\r\n}(document));\r\n</script>');

-- ----------------------------
-- Table structure for `content_blocks`
-- ----------------------------
DROP TABLE IF EXISTS `content_blocks`;
CREATE TABLE `content_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT '',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code_idx` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content_blocks
-- ----------------------------
INSERT INTO `content_blocks` VALUES ('1', 'mainpage_section', '', '3', '2013-05-30 13:17:34');
INSERT INTO `content_blocks` VALUES ('2', 'investor_proposition', '', '3', '2013-05-30 15:01:16');
INSERT INTO `content_blocks` VALUES ('3', 'social_contacts', 'Social contacts (main page)', '3', '2013-05-30 15:13:26');
INSERT INTO `content_blocks` VALUES ('4', 'footer_copyright', '', '3', '2013-05-30 15:15:14');
INSERT INTO `content_blocks` VALUES ('5', 'post_social_buttons', 'Social buttons for post', '3', '2013-05-31 12:05:58');

-- ----------------------------
-- Table structure for `galleries`
-- ----------------------------
DROP TABLE IF EXISTS `galleries`;
CREATE TABLE `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `settings` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code_idx` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of galleries
-- ----------------------------
INSERT INTO `galleries` VALUES ('1', 'Main Slider', 'mail_slider', '{\r\n\"slider_image\": {  \"adaptiveThumb\": { \"width\": 980, \"height\": 370 }  }\r\n}', '2013-05-30 13:07:01', '2013-05-30 13:07:01');

-- ----------------------------
-- Table structure for `glossary_item_i18ns`
-- ----------------------------
DROP TABLE IF EXISTS `glossary_item_i18ns`;
CREATE TABLE `glossary_item_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `letter` varchar(1) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`),
  KEY `parent_id_idx` (`parent_id`),
  KEY `letter_idx` (`letter`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of glossary_item_i18ns
-- ----------------------------
INSERT INTO `glossary_item_i18ns` VALUES ('1', 'en', '1', 'A', 'A glossary word', 'A glossary text  A glossary text  A glossary text  A glossary text  A glossary text  A glossary text ');
INSERT INTO `glossary_item_i18ns` VALUES ('2', 'en', '2', 'G', 'Game', 'Game game');
INSERT INTO `glossary_item_i18ns` VALUES ('3', 'de', '2', 'S', 'Spiel', 'Spiel Spiel Spiel Spiel SpielSpiel Spiel Spiel Spiel Spiel Spiel Spiel ');
INSERT INTO `glossary_item_i18ns` VALUES ('4', 'en', '3', 'A', 'Albatros', 'Albatros Albatros Albatros');
INSERT INTO `glossary_item_i18ns` VALUES ('5', 'en', '4', 'A', 'Anaconda', 'Anaconda Anaconda Anaconda Anaconda');
INSERT INTO `glossary_item_i18ns` VALUES ('6', 'en', '5', 'A', 'Air', 'Air Air Air Air Air');
INSERT INTO `glossary_item_i18ns` VALUES ('7', 'en', '6', 'A', 'Apple', 'Apple Apple Apple Apple');
INSERT INTO `glossary_item_i18ns` VALUES ('8', 'en', '7', 'A', 'Address', 'Address Address Address Address');
INSERT INTO `glossary_item_i18ns` VALUES ('9', 'en', '8', 'A', 'Auto', 'Auto Auto Auto Auto');
INSERT INTO `glossary_item_i18ns` VALUES ('10', 'en', '9', 'B', 'Banana', 'Yellow fruit');
INSERT INTO `glossary_item_i18ns` VALUES ('11', 'de', '9', 'B', 'Banane', 'Gelbe Früchte');

-- ----------------------------
-- Table structure for `glossary_items`
-- ----------------------------
DROP TABLE IF EXISTS `glossary_items`;
CREATE TABLE `glossary_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of glossary_items
-- ----------------------------
INSERT INTO `glossary_items` VALUES ('1', '2013-05-30 18:21:39', '2013-05-30 18:21:39');
INSERT INTO `glossary_items` VALUES ('2', '2013-05-30 18:23:42', '2013-05-30 18:23:42');
INSERT INTO `glossary_items` VALUES ('3', '2013-05-31 09:58:58', '2013-05-31 09:58:58');
INSERT INTO `glossary_items` VALUES ('4', '2013-05-31 09:59:17', '2013-05-31 09:59:17');
INSERT INTO `glossary_items` VALUES ('5', '2013-05-31 09:59:39', '2013-05-31 09:59:39');
INSERT INTO `glossary_items` VALUES ('6', '2013-05-31 10:00:08', '2013-05-31 10:00:08');
INSERT INTO `glossary_items` VALUES ('7', '2013-05-31 10:00:41', '2013-05-31 10:00:41');
INSERT INTO `glossary_items` VALUES ('8', '2013-05-31 10:00:54', '2013-05-31 10:00:54');
INSERT INTO `glossary_items` VALUES ('9', '2013-05-31 11:39:33', '2013-05-31 11:39:33');

-- ----------------------------
-- Table structure for `images`
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) DEFAULT NULL,
  `model_type` varchar(32) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `url` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_type_model_id_idx` (`model_type`,`model_id`),
  KEY `order_idx` (`order`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of images
-- ----------------------------
INSERT INTO `images` VALUES ('1', '1', 'Gallery', '51a725164bb52.jpg', 'Water', 'Water Water', '', null, '2013-05-30 13:08:22', '2013-05-30 13:09:44');
INSERT INTO `images` VALUES ('2', '1', 'Gallery', '51a7259e3db4e.png', 'Woman', 'Woman', '', null, '2013-05-30 13:10:38', '2013-05-30 13:12:53');

-- ----------------------------
-- Table structure for `languages`
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` varchar(2) NOT NULL,
  `name` varchar(32) NOT NULL,
  `locale` varchar(32) DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `is_default_idx` (`is_default`),
  KEY `is_active_idx` (`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO `languages` VALUES ('de', 'DE', '', '4', '0', '1', '2013-05-29 11:56:29', '2013-05-29 11:56:29');
INSERT INTO `languages` VALUES ('en', 'EN', '', '1', '1', '1', '2013-05-29 11:33:52', '2013-05-29 11:55:43');
INSERT INTO `languages` VALUES ('fr', 'FR', '', '3', '0', '1', '2013-05-29 11:56:13', '2013-05-29 11:56:13');
INSERT INTO `languages` VALUES ('nl', 'NL', '', '2', '0', '1', '2013-05-29 11:55:59', '2013-05-29 11:55:59');

-- ----------------------------
-- Table structure for `location_i18ns`
-- ----------------------------
DROP TABLE IF EXISTS `location_i18ns`;
CREATE TABLE `location_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`),
  KEY `parent_id_idx` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of location_i18ns
-- ----------------------------
INSERT INTO `location_i18ns` VALUES ('1', 'en', '1', 'First Office', 'Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Nam liber tempor cum soluta nobis eleifend option congue nihil ');
INSERT INTO `location_i18ns` VALUES ('2', 'en', '2', 'Second Office', 'Second Office Second Office Second Office Second Office Second Office Second Office Second Office Second Office Second Office Second Office Second Office');
INSERT INTO `location_i18ns` VALUES ('3', 'en', '3', 'Greenice', 'Greenice Greenice Greenice Greenice Greenice');

-- ----------------------------
-- Table structure for `locations`
-- ----------------------------
DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skype` varchar(255) DEFAULT NULL,
  `embed_code` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_idx` (`order`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of locations
-- ----------------------------
INSERT INTO `locations` VALUES ('1', 'ivan.greenice', '<iframe width=\"205\" height=\"205\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C+%D1%81%D0%B8%D0%BB%D0%B0%D0%B5%D0%B2%D0%B0+5%D0%B0&amp;aq=t&amp;sll=37.0625,-95.677068&amp;sspn=59.076726,114.169922&amp;ie=UTF8&amp;hq=&amp;hnear=%D1%83%D0%BB.+%D0%9F%D0%B0%D0%B2%D0%BB%D0%B0+%D0%A1%D0%B8%D0%BB%D0%B0%D0%B5%D0%B2%D0%B0,+5%D0%90,+%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C,+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4+%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C,+%D0%A3%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0&amp;t=m&amp;ll=44.57659,33.516369&amp;spn=0.012533,0.017509&amp;z=14&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=ru&amp;geocode=&amp;q=%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C+%D1%81%D0%B8%D0%BB%D0%B0%D0%B5%D0%B2%D0%B0+5%D0%B0&amp;aq=t&amp;sll=37.0625,-95.677068&amp;sspn=59.076726,114.169922&amp;ie=UTF8&amp;hq=&amp;hnear=%D1%83%D0%BB.+%D0%9F%D0%B0%D0%B2%D0%BB%D0%B0+%D0%A1%D0%B8%D0%BB%D0%B0%D0%B5%D0%B2%D0%B0,+5%D0%90,+%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C,+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4+%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C,+%D0%A3%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0&amp;t=m&amp;ll=44.57659,33.516369&amp;spn=0.012533,0.017509&amp;z=14&amp;iwloc=A\" style=\"text-align:left\">Просмотреть увеличенную карту</a></small>', 'first-office', '1', '2013-05-30 17:07:36', '2013-05-30 17:07:36');
INSERT INTO `locations` VALUES ('2', 'greenice', '<iframe width=\"205\" height=\"205\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=Florida,+United+States&amp;aq=0&amp;oq=Florida&amp;sll=44.565141,33.525918&amp;sspn=0.013178,0.027874&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%A4%D0%BB%D0%BE%D1%80%D0%B8%D0%B4%D0%B0&amp;t=m&amp;ll=27.665285,-81.515808&amp;spn=0.249338,0.280151&amp;z=10&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=ru&amp;geocode=&amp;q=Florida,+United+States&amp;aq=0&amp;oq=Florida&amp;sll=44.565141,33.525918&amp;sspn=0.013178,0.027874&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%A4%D0%BB%D0%BE%D1%80%D0%B8%D0%B4%D0%B0&amp;t=m&amp;ll=27.665285,-81.515808&amp;spn=0.249338,0.280151&amp;z=10&amp;iwloc=A\" style=\"text-align:left\">Просмотреть увеличенную карту</a></small>', 'second-office', '2', '2013-05-30 17:11:38', '2013-05-30 17:11:38');
INSERT INTO `locations` VALUES ('3', 'greenice', '<iframe width=\"205\" height=\"205\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=%D0%A4%D0%B8%D0%BE%D0%BB%D0%B5%D0%BD%D1%82%D0%BE%D0%B2%D1%81%D0%BA%D0%BE%D0%B5+%D1%88%D0%BE%D1%81%D1%81%D0%B5,+%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C,+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4+%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C,+%D0%A3%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0&amp;aq=0&amp;oq=%D1%84%D0%B8%D0%BE%D0%BB%D0%B5%D0%BD%D1%82&amp;sll=37.0625,-95.677068&amp;sspn=59.076726,114.169922&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%A4%D0%B8%D0%BE%D0%BB%D0%B5%D0%BD%D1%82%D0%BE%D0%B2%D1%81%D0%BA%D0%BE%D0%B5+%D1%88.,+%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C,+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4+%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C,+%D0%A3%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0&amp;t=m&amp;ll=44.559836,33.458605&amp;spn=0.012537,0.017509&amp;z=14&amp;iwloc=A&amp;output=embed\"></iframe><br /><small><a href=\"https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=ru&amp;geocode=&amp;q=%D0%A4%D0%B8%D0%BE%D0%BB%D0%B5%D0%BD%D1%82%D0%BE%D0%B2%D1%81%D0%BA%D0%BE%D0%B5+%D1%88%D0%BE%D1%81%D1%81%D0%B5,+%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C,+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4+%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C,+%D0%A3%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0&amp;aq=0&amp;oq=%D1%84%D0%B8%D0%BE%D0%BB%D0%B5%D0%BD%D1%82&amp;sll=37.0625,-95.677068&amp;sspn=59.076726,114.169922&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%A4%D0%B8%D0%BE%D0%BB%D0%B5%D0%BD%D1%82%D0%BE%D0%B2%D1%81%D0%BA%D0%BE%D0%B5+%D1%88.,+%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C,+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4+%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C,+%D0%A3%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0&amp;t=m&amp;ll=44.559836,33.458605&amp;spn=0.012537,0.017509&amp;z=14&amp;iwloc=A\" style=\"text-align:left\">Просмотреть увеличенную карту</a></small>', 'greenice', '3', '2013-05-31 11:36:19', '2013-05-31 11:36:19');

-- ----------------------------
-- Table structure for `mail_template_i18ns`
-- ----------------------------
DROP TABLE IF EXISTS `mail_template_i18ns`;
CREATE TABLE `mail_template_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `content` text,
  `content_plain` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`),
  KEY `parent_id_idx` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mail_template_i18ns
-- ----------------------------
INSERT INTO `mail_template_i18ns` VALUES ('1', 'en', '1', 'Application form', '<b>Email</b>: {email}&nbsp;<div><b>Name</b>: {username}&nbsp;<br><span style=\"line-height: 1.45em;\"><b>Phone</b>: {phone} <br><b>About user</b>: <br>{description}</span></div>', '');

-- ----------------------------
-- Table structure for `mail_templates`
-- ----------------------------
DROP TABLE IF EXISTS `mail_templates`;
CREATE TABLE `mail_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(50) NOT NULL,
  `from` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_unique_idx` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mail_templates
-- ----------------------------
INSERT INTO `mail_templates` VALUES ('1', 'application_form', 'test@test.com', 'Application form', '', '2013-05-31 11:25:18', '2013-05-31 11:29:48');

-- ----------------------------
-- Table structure for `menu_item_i18ns`
-- ----------------------------
DROP TABLE IF EXISTS `menu_item_i18ns`;
CREATE TABLE `menu_item_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `lang_id` varchar(2) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`),
  KEY `parent_id_idx` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu_item_i18ns
-- ----------------------------
INSERT INTO `menu_item_i18ns` VALUES ('1', '1', 'en', 'root');
INSERT INTO `menu_item_i18ns` VALUES ('2', '2', 'en', 'About Us');
INSERT INTO `menu_item_i18ns` VALUES ('3', '3', 'en', 'Solutions');
INSERT INTO `menu_item_i18ns` VALUES ('4', '4', 'en', 'Services');
INSERT INTO `menu_item_i18ns` VALUES ('5', '5', 'en', 'Technology');
INSERT INTO `menu_item_i18ns` VALUES ('6', '6', 'en', 'Social/Media');
INSERT INTO `menu_item_i18ns` VALUES ('7', '7', 'en', 'Blog/news');
INSERT INTO `menu_item_i18ns` VALUES ('8', '8', 'en', 'root');
INSERT INTO `menu_item_i18ns` VALUES ('9', '9', 'en', 'About us');
INSERT INTO `menu_item_i18ns` VALUES ('10', '10', 'en', 'Our mission');
INSERT INTO `menu_item_i18ns` VALUES ('11', '11', 'en', 'Our vision');
INSERT INTO `menu_item_i18ns` VALUES ('12', '12', 'en', 'Owners');
INSERT INTO `menu_item_i18ns` VALUES ('13', '13', 'en', 'root');
INSERT INTO `menu_item_i18ns` VALUES ('14', '14', 'en', 'About Us');
INSERT INTO `menu_item_i18ns` VALUES ('15', '15', 'en', 'Investors');
INSERT INTO `menu_item_i18ns` VALUES ('16', '16', 'en', 'Careers');
INSERT INTO `menu_item_i18ns` VALUES ('17', '17', 'en', 'Locations');
INSERT INTO `menu_item_i18ns` VALUES ('18', '18', 'en', 'Contact Us');
INSERT INTO `menu_item_i18ns` VALUES ('19', '19', 'en', 'root');
INSERT INTO `menu_item_i18ns` VALUES ('20', '20', 'en', 'Google');
INSERT INTO `menu_item_i18ns` VALUES ('21', '21', 'en', 'Facebook');
INSERT INTO `menu_item_i18ns` VALUES ('22', '22', 'en', 'root');
INSERT INTO `menu_item_i18ns` VALUES ('23', '23', 'en', 'Privacy Policy');
INSERT INTO `menu_item_i18ns` VALUES ('24', '24', 'en', 'Terms & Conditions');
INSERT INTO `menu_item_i18ns` VALUES ('25', '25', 'en', 'Sitemap');
INSERT INTO `menu_item_i18ns` VALUES ('26', '26', 'en', 'Glossary');
INSERT INTO `menu_item_i18ns` VALUES ('27', '23', 'de', 'Datenschutz');

-- ----------------------------
-- Table structure for `menu_items`
-- ----------------------------
DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `root` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `visible_condition` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_id_idx` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu_items
-- ----------------------------
INSERT INTO `menu_items` VALUES ('1', '1', '#', '1', '1', '14', '1', null, '2013-05-29 11:54:46');
INSERT INTO `menu_items` VALUES ('2', '1', '/pages/about-us', '1', '2', '3', '2', null, '2013-05-29 11:55:31');
INSERT INTO `menu_items` VALUES ('3', '1', '/solutions', '1', '4', '5', '2', null, '2013-05-29 11:59:57');
INSERT INTO `menu_items` VALUES ('4', '1', '/services', '1', '6', '7', '2', null, '2013-05-29 12:00:18');
INSERT INTO `menu_items` VALUES ('5', '1', '/pages/technology', '1', '8', '9', '2', null, '2013-05-29 12:00:45');
INSERT INTO `menu_items` VALUES ('6', '1', '/pages/community', '1', '10', '11', '2', null, '2013-05-29 12:01:16');
INSERT INTO `menu_items` VALUES ('7', '1', '/blog', '1', '12', '13', '2', null, '2013-05-29 12:01:45');
INSERT INTO `menu_items` VALUES ('8', '2', '#', '8', '1', '10', '1', null, '2013-05-29 12:47:07');
INSERT INTO `menu_items` VALUES ('9', '2', '/pages/about-us', '8', '2', '3', '2', null, '2013-05-29 12:49:17');
INSERT INTO `menu_items` VALUES ('10', '2', '/pages/our-mission', '8', '4', '5', '2', null, '2013-05-29 12:49:33');
INSERT INTO `menu_items` VALUES ('11', '2', '/pages/our-vision', '8', '6', '7', '2', null, '2013-05-29 12:49:44');
INSERT INTO `menu_items` VALUES ('12', '2', '/pages/owners', '8', '8', '9', '2', null, '2013-05-29 12:49:52');
INSERT INTO `menu_items` VALUES ('13', '3', '#', '13', '1', '12', '1', null, '2013-05-30 15:08:23');
INSERT INTO `menu_items` VALUES ('14', '3', '/pages/about-us', '13', '2', '3', '2', null, '2013-05-30 15:08:52');
INSERT INTO `menu_items` VALUES ('15', '3', '/pages/investors', '13', '4', '5', '2', null, '2013-05-30 15:09:15');
INSERT INTO `menu_items` VALUES ('16', '3', '/careers', '13', '6', '7', '2', null, '2013-05-30 15:09:32');
INSERT INTO `menu_items` VALUES ('17', '3', '/locations', '13', '8', '9', '2', null, '2013-05-30 15:09:49');
INSERT INTO `menu_items` VALUES ('18', '3', '/pages/contact-us', '13', '10', '11', '2', null, '2013-05-30 15:10:20');
INSERT INTO `menu_items` VALUES ('19', '4', '#', '19', '1', '6', '1', null, '2013-05-30 15:18:02');
INSERT INTO `menu_items` VALUES ('20', '4', 'http://www.google.com', '19', '2', '3', '2', null, '2013-05-30 15:18:26');
INSERT INTO `menu_items` VALUES ('21', '4', 'http://facebook.com', '19', '4', '5', '2', null, '2013-05-30 15:25:17');
INSERT INTO `menu_items` VALUES ('22', '5', '#', '22', '1', '10', '1', null, '2013-05-30 18:45:32');
INSERT INTO `menu_items` VALUES ('23', '5', '/pages/privacy-policy', '22', '2', '3', '2', null, '2013-05-30 18:45:52');
INSERT INTO `menu_items` VALUES ('24', '5', '#', '22', '4', '5', '2', null, '2013-05-30 18:46:09');
INSERT INTO `menu_items` VALUES ('25', '5', '#', '22', '6', '7', '2', null, '2013-05-30 18:46:24');
INSERT INTO `menu_items` VALUES ('26', '5', '/glossary', '22', '8', '9', '2', null, '2013-05-30 18:46:44');

-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code_idx` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'header_menu', 'Header Menu', '2013-05-29 11:54:46');
INSERT INTO `menus` VALUES ('2', 'about_us', 'About Us Page', '2013-05-29 12:47:07');
INSERT INTO `menus` VALUES ('3', 'company_info', 'Company info', '2013-05-30 15:08:22');
INSERT INTO `menus` VALUES ('4', 'other_links', 'Other links', '2013-05-30 15:18:02');
INSERT INTO `menus` VALUES ('5', 'footer_menu', 'Footer menu', '2013-05-30 18:45:32');

-- ----------------------------
-- Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `short_content` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published_at` date NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published_at_idx` (`published_at`),
  KEY `slug_idx` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news
-- ----------------------------

-- ----------------------------
-- Table structure for `page_i18ns`
-- ----------------------------
DROP TABLE IF EXISTS `page_i18ns`;
CREATE TABLE `page_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `meta_title` varchar(255) DEFAULT '',
  `meta_keywords` text,
  `meta_description` text,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`),
  KEY `parent_id_idx` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of page_i18ns
-- ----------------------------
INSERT INTO `page_i18ns` VALUES ('1', 'en', '1', 'About Us', '', '', '', '<div class=\"col2\">\r\n                <div class=\"offset30 line\">\r\n                    <h2>Subtitle</h2>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"col2\">\r\n                <div class=\"offset30\">\r\n                    <h2>Subtitle</h2>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                </div>\r\n            </div>');
INSERT INTO `page_i18ns` VALUES ('2', 'en', '2', 'Our mission', '', '', '', '<div class=\"col2\">\r\n                <div class=\"offset30 line\">\r\n                    <h2>Subtitle</h2>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"col2\">\r\n                <div class=\"offset30\">\r\n                    <h2>Subtitle</h2>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                </div>\r\n            </div>');
INSERT INTO `page_i18ns` VALUES ('3', 'en', '3', 'Our vision', '', '', '', '<div class=\"col2\">\r\n                <div class=\"offset30 line\">\r\n                    <h2>Subtitle</h2>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"col2\">\r\n                <div class=\"offset30\">\r\n                    <h2>Subtitle</h2>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                </div>\r\n            </div>');
INSERT INTO `page_i18ns` VALUES ('4', 'en', '4', 'Owners', '', '', '', '<div class=\"col2\">\r\n                <div class=\"offset30 line\">\r\n                    <h2>Subtitle</h2>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"col2\">\r\n                <div class=\"offset30\">\r\n                    <h2>Subtitle</h2>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                    <p>Why isn\'t there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>\r\n                </div>\r\n            </div>');
INSERT INTO `page_i18ns` VALUES ('5', 'en', '5', 'Technology', '', '', '', '<p><div>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div><div>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. At vero eos et accusam et justo duo dolores et ea rebum.</div><br></p>');
INSERT INTO `page_i18ns` VALUES ('6', 'en', '6', 'Investors', 'Investors', '', '', '<img src=\"/images/no-image-investors.jpg\"/>\r\n            <p>Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is </p>\r\n            <p>Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is Company to populare belief, Lorem Ipsum is </p>\r\n            ');
INSERT INTO `page_i18ns` VALUES ('7', 'en', '7', 'Privacy Policy', '', '', '', '<p>Privacy Policy&nbsp;Privacy Policy&nbsp;Privacy Policy<br></p>');
INSERT INTO `page_i18ns` VALUES ('8', 'de', '7', 'Datenschutz', '', '', '', '<p>DatenschutzDatenschutzDatenschutzDatenschutzDatenschutzDatenschutz<br></p>');
INSERT INTO `page_i18ns` VALUES ('9', 'en', '8', 'Contact Us', '', '', '', '<p>this is text</p>');
INSERT INTO `page_i18ns` VALUES ('10', 'en', '9', 'Community', '', '', '', '<p><div>Stet clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</div><div>Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. At vero eos et accusam et justo duo dolores et ea rebum. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.</div><br></p>');

-- ----------------------------
-- Table structure for `pages`
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `template` varchar(255) NOT NULL DEFAULT 'view',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code_idx` (`code`),
  KEY `category_id_idx` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES ('1', 'about', 'about-us', 'about_us', 'about_us_view', '1', '2013-05-29 12:26:52', '2013-05-31 11:57:30');
INSERT INTO `pages` VALUES ('2', 'about', 'our-mission', 'our_mission', 'about_us_view', '1', '2013-05-29 12:54:10', '2013-05-31 11:57:38');
INSERT INTO `pages` VALUES ('3', 'about', 'our-vision', 'our_vision', 'about_us_view', '1', '2013-05-29 12:55:10', '2013-05-31 11:57:46');
INSERT INTO `pages` VALUES ('4', 'about', 'owners', 'owners', 'about_us_view', '1', '2013-05-29 12:55:49', '2013-05-31 11:57:54');
INSERT INTO `pages` VALUES ('5', '', 'technology', 'technology', 'view', '1', '2013-05-29 17:22:27', '2013-05-31 11:59:00');
INSERT INTO `pages` VALUES ('6', '', 'investors', 'investors', 'investors_view', '1', '2013-05-30 15:04:42', '2013-05-30 15:04:42');
INSERT INTO `pages` VALUES ('7', '', 'privacy-policy', 'privacy_policy', 'view', '1', '2013-05-30 19:02:20', '2013-05-30 19:03:26');
INSERT INTO `pages` VALUES ('8', '', 'contact-us', 'contact_us', 'contact_us_view', '1', '2013-05-31 11:19:52', '2013-05-31 11:19:52');
INSERT INTO `pages` VALUES ('9', '', 'community', 'community', 'community_view', '1', '2013-06-03 10:35:44', '2013-06-03 10:35:44');

-- ----------------------------
-- Table structure for `post_i18ns`
-- ----------------------------
DROP TABLE IF EXISTS `post_i18ns`;
CREATE TABLE `post_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `short_content` text NOT NULL,
  `content` text NOT NULL,
  `meta_title` varchar(255) DEFAULT '',
  `meta_keywords` text,
  `meta_description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`),
  KEY `parent_id_idx` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post_i18ns
-- ----------------------------
INSERT INTO `post_i18ns` VALUES ('1', 'en', '1', 'Hot code changing', '<p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh&nbsp;<br></p>', '<p><p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.</p><p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est. Stet clita kasd gubergren, no sea takimata sanctus est.</p><br></p>', '', '', '');
INSERT INTO `post_i18ns` VALUES ('2', 'en', '2', 'Australia: China spy agency hack claims \'will not hit ties\'', '<p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.<br></p>', '<p><p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p><p>Lorem ipsum dolor sit amet. At vero eos et accusam et justo duo dolores et ea rebum. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. At vero eos et accusam et justo duo dolores et ea rebum.</p><br></p>', '', '', '');

-- ----------------------------
-- Table structure for `post_tags`
-- ----------------------------
DROP TABLE IF EXISTS `post_tags`;
CREATE TABLE `post_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `frequency` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post_tags
-- ----------------------------

-- ----------------------------
-- Table structure for `posts`
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `category_id` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `published_at` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published_at_idx` (`published_at`),
  KEY `category_id_idx` (`category_id`),
  KEY `slug_idx` (`slug`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('1', null, 'hot-code-changing', '51a62284a44bb.jpg', 'first-category', '1', '2013-05-29', '2013-05-29 18:45:08', '2013-05-29 18:45:08');
INSERT INTO `posts` VALUES ('2', null, 'australia-china-spy-agency-hack-claims-will-not-hit-ties', '51a700b99f6e3.jpg', 'second-category', '1', '2013-05-30', '2013-05-30 10:33:13', '2013-05-30 10:33:13');

-- ----------------------------
-- Table structure for `posts_tags`
-- ----------------------------
DROP TABLE IF EXISTS `posts_tags`;
CREATE TABLE `posts_tags` (
  `post_id` int(11) unsigned NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`post_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of posts_tags
-- ----------------------------

-- ----------------------------
-- Table structure for `service_i18ns`
-- ----------------------------
DROP TABLE IF EXISTS `service_i18ns`;
CREATE TABLE `service_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `short_content` text NOT NULL,
  `meta_title` varchar(255) DEFAULT '',
  `meta_keywords` text,
  `meta_description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`),
  KEY `parent_id_idx` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of service_i18ns
-- ----------------------------
INSERT INTO `service_i18ns` VALUES ('1', 'en', '1', 'First service title ', '<h2>This page can be used for service details (if needed), career details (also if needed), solution details and for every text page etc.</h2><p>Notice also that we include a BreadCrumbs widget at the beginning of the view. The widget is a customly developed to display breadcrumbs (navigation path) of the current page. The reference $this stands for the current controller, and we use its crumbs property to store the breadcrumbs data.</p><p>Notice also that we include a BreadCrumbs widget at the beginning of the view. The widget is a customly developed to display breadcrumbs (navigation path) of the current page. The reference $this stands for the current controller, and we use its crumbs property to store the breadcrumbs data.</p><p>Notice also that we include a BreadCrumbs widget at the beginning of the view. The widget is a customly developed to display breadcrumbs (navigation path) of the current page. The reference $this stands for the current controller, and we use its crumbs property to store the breadcrumbs data.</p><p>Notice also that we include a BreadCrumbs widget at the beginning of the view. The widget is a customly developed to display breadcrumbs (navigation path) of the current page. The reference $this stands for the current controller, and we use its crumbs property to store the breadcrumbs data.</p><p>Notice also that we include a BreadCrumbs widget at the beginning of the view. The widget is a customly developed to display breadcrumbs (navigation path) of the current page. The reference $this stands for the current controller, and we use its crumbs property to store the breadcrumbs data.</p><p>Notice also that we include a BreadCrumbs widget at the beginning of the view. The widget is a customly developed to display breadcrumbs (navigation path) of the current page. The reference $this stands for the current controller, and we use its crumbs property to store the breadcrumbs data.</p><p>Notice also that we include a BreadCrumbs widget at the beginning of the view. The widget is a customly developed to display breadcrumbs (navigation path) of the current page. The reference $this stands for the current controller, and we use its crumbs property to store the breadcrumbs data.</p>\r\n', 'Contrary to popular belief, Lorem ipsum is not simply random Contrary to popular belief, Lorem ipsum is not simply random Contrary to popular belief, Lorem ipsum is not simply random Contrary to popular belief, Lorem ipsum is not simply  ', '', '', '');
INSERT INTO `service_i18ns` VALUES ('2', 'en', '2', 'Second service title ', '<p>Second service title &nbsp;Second service title &nbsp;Second service title&nbsp;<br></p>', 'Second service title  Second service title  Second service title Second service title  Second service title  Second service title Second service title  Second service title  Second service title Second service title  Second service title  Second service title ', 'Second service title', '', '');

-- ----------------------------
-- Table structure for `services`
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id_idx` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of services
-- ----------------------------
INSERT INTO `services` VALUES ('1', 'production', '51a6008969e91.jpg', 'first-service-title', '1', '2013-05-29 16:20:09', '2013-05-29 16:46:47');
INSERT INTO `services` VALUES ('2', 'custom', '51a60a58753cc.jpg', 'second-service-title', '2', '2013-05-29 17:02:00', '2013-05-29 17:02:00');

-- ----------------------------
-- Table structure for `solution_i18ns`
-- ----------------------------
DROP TABLE IF EXISTS `solution_i18ns`;
CREATE TABLE `solution_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `short_content` text NOT NULL,
  `meta_title` varchar(255) DEFAULT '',
  `meta_keywords` text,
  `meta_description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`),
  KEY `parent_id_idx` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of solution_i18ns
-- ----------------------------
INSERT INTO `solution_i18ns` VALUES ('1', 'en', '1', 'Duis autem vel', '<p><div>At vero eos et accusam et justo duo dolores et ea rebum.. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. At vero eos et accusam et justo duo dolores et ea rebum. At vero eos et accusam et justo duo dolores et ea rebum.</div><div>At vero eos et accusam et justo duo dolores et ea rebum. At vero eos et accusam et justo duo dolores et ea rebum. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. At vero eos et accusam et justo duo dolores et ea rebum.</div><br></p>', 'Contry to popular belief, Contry to popular belief, Contry to popular belief, Contry to popular belief, Contry to popular belief, Contry to popular belief, Contry to popular belief,', '', '', '');

-- ----------------------------
-- Table structure for `solutions`
-- ----------------------------
DROP TABLE IF EXISTS `solutions`;
CREATE TABLE `solutions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_idx` (`order`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of solutions
-- ----------------------------
INSERT INTO `solutions` VALUES ('1', '51a70f8b23513.jpg', 'duis-autem-vel', '1', '2013-05-30 11:36:27', '2013-05-30 11:36:27');

-- ----------------------------
-- Table structure for `tbl_migration`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_migration`;
CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_migration
-- ----------------------------
INSERT INTO `tbl_migration` VALUES ('m000000_000000_base', '1369816405');
INSERT INTO `tbl_migration` VALUES ('m130520_112156_menu_tables', '1369816427');
INSERT INTO `tbl_migration` VALUES ('m130521_112157_blog_tables', '1369816431');
INSERT INTO `tbl_migration` VALUES ('m130522_112157_news_tables', '1369816429');
INSERT INTO `tbl_migration` VALUES ('m130522_142157_pages_tables', '1369816430');
INSERT INTO `tbl_migration` VALUES ('m130523_124357_languages_tables', '1369816432');
INSERT INTO `tbl_migration` VALUES ('m130525_163157_images_tables', '1369816433');
INSERT INTO `tbl_migration` VALUES ('m130527_101457_galleries_tables', '1369816434');
INSERT INTO `tbl_migration` VALUES ('m130527_133256_categories_tables', '1369816435');
INSERT INTO `tbl_migration` VALUES ('m130528_113957_admins_tables', '1369816425');
INSERT INTO `tbl_migration` VALUES ('m130528_151557_mail_tables', '1369816427');
INSERT INTO `tbl_migration` VALUES ('m130529_103057_content_blocks_tables', '1369816437');
INSERT INTO `tbl_migration` VALUES ('m130529_112157_members_tables', '1369823264');
INSERT INTO `tbl_migration` VALUES ('m130529_142157_add_page_template', '1369820779');
INSERT INTO `tbl_migration` VALUES ('m130530_170757_glossary_tables', '1369926667');
INSERT INTO `tbl_migration` VALUES ('m130531_122156_add_conditon', '1370246426');
INSERT INTO `tbl_migration` VALUES ('m160529_142157_services_tables', '1369832868');
INSERT INTO `tbl_migration` VALUES ('m160530_111757_solutions_tables', '1369902911');
INSERT INTO `tbl_migration` VALUES ('m160530_151757_careers_tables', '1369917731');
INSERT INTO `tbl_migration` VALUES ('m160530_160757_locations_tables', '1369922496');

-- ----------------------------
-- Table structure for `team_members`
-- ----------------------------
DROP TABLE IF EXISTS `team_members`;
CREATE TABLE `team_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of team_members
-- ----------------------------
INSERT INTO `team_members` VALUES ('1', 'Arthur O\'Brian', 'CEO', '51a5d90cf3e77.jpg', '2013-05-29 13:31:41', '2013-05-29 16:05:50');
