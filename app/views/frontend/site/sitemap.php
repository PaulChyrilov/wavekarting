<?php 
$aboutUs = array();
foreach ($data['pages'] as $i => $e) {
    if ($e['category'] == 'about') {
        $aboutUs[] = $e;
        unset($data['pages'][$i]);
    }
}
$data['about_us'] = $aboutUs;
$mapping = array(
    'about_us' => 'About Us',
    'solutions' => 'Solutions',
    'services' => 'Services',
    'blog' => 'Blog',
    'careers' => 'Careers',
    'pages' => 'Site'
);
$i = 1;
?>

<section class="body-page">

    <header>
        <h2><?= Yii::t('mainpage', 'Site map') ?></h2>
    </header>

    <section class="content-block backgrounded">
        <div class="sitemap-list">
            <div class="list-row">
            <?php foreach ($mapping as $key => $value) { ?>
                <?php if ($i % 6 === 0) { ?></div><div class="list-row"><?php } ?>
                <section>
                    <header><?= Yii::t('mainpage', $value) ?></header>
                    <ul>
                    <?php foreach ($data[$key] as $e) { ?> 
                        <li><?= CHtml::link($e['title'], $e['url'])?></li>
                    <?php } ?>
                    </ul>
                </section>
                <?php ++$i; ?>
            <?php } ?>
            </div>

        </div>
    </section>

</section>