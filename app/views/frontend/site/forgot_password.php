<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form TbActiveForm  */

$this->pageTitle = param('project_name') . ' - ' . Yii::t('mainpage', 'Forgot password');

Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/css/pages/index.css')
?>
<section class="body-page">

    <header>
        <h2><?= Yii::t('mainpage', 'Forgot password') ?></h2>
    </header>

    <section class="content-block">
        <div class="login-block">
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id'     => 'login-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                )
            )); ?>
                <?= $form->labelEx($model, 'email'); ?>
                <?= $form->textField($model, 'email', array('placeholder' => 'E-mail')); ?>
                <?= $form->error($model, 'email'); ?>

                <button class="submit-button" type="submit"><?= Yii::t('login', 'Reset')?></button>
            <?php $this->endWidget(); ?>
        </div>

    </section>

</section>