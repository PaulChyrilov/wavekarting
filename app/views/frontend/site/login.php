<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form TbActiveForm  */

$this->pageTitle = param('project_name') . ' - ' . Yii::t('mainpage', 'Login');

Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/css/pages/index.css')
?>
<section class="body-page">

    <header>
        <h2>Login</h2>
    </header>

    <section class="content-block">
        <div class="login-block">
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id'     => 'login-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                )
            )); ?>
                <?= $form->labelEx($model, 'username'); ?>
                <?= $form->textField($model, 'username', array('placeholder' => 'Username')); ?>
                <?= $form->error($model, 'username'); ?>
                
                <?= $form->labelEx($model, 'password'); ?>
                <?= $form->passwordField($model, 'password', array('placeholder' => 'Password')); ?>
                <?= $form->error($model, 'password'); ?>

                <button class="submit-button" type="submit"><?= Yii::t('login', 'Login')?></button>

                <?= CHtml::link(Yii::t('login', 'Forgot your password?'), array('/site/forgotpassword'), array(
                    'class' => 'forgot',
                )) ?>
            <?php $this->endWidget(); ?>
        </div>

    </section>

</section>