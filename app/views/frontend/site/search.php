<?php 
$this->setPageTitle('Site searching "' . $q . '"');
$count = count($data);
?>

<section class="body-page">

    <header>
        <h2><?= Yii::t('mainpage', 'Search results') ?></h2>
    </header>

    <section class="content-block backgrounded">
        <div class="search-info">
            <div><?= Yii::t('mainpage', 'Search results for:') ?> <span><?= CHtml::encode($q) ?></span></div>
            <div><?= Yii::t('mainpage', 'Found:') ?> <span><?= $count ?></span></div>
        </div>
        <?php if ($count > 0) { ?>
        <ul class="results-list">
            <?php foreach ($data as $entity) { ?>
             <li>
                <?= CHtml::link(CHtml::encode($entity->getI18n('title')), $entity->getUrl()) ?>
                <p><?= HString::limitChars(strip_tags($entity->getI18n('content')), 200) ?></p>
            </li>
            <?php } ?>
        </ul>
        <?php } else { ?>
            <span style="margin-top: 20px;" class="empty"><?= Yii::t('zii', 'No results found.')?></span>
        <?php } ?>
    </section>

</section>

 

