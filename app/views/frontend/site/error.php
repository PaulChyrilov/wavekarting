<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = Yii::app()->name . ' - Error';
Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/css/pages/index.css');
?>

<section class="body-page">

        <header>
            <h2>Error <?php echo $code; ?></h2>
        </header>

        <section class="content-block">
            <?php if ($code == 403) { ?>
                <div class="login-block">
                    <p class="access-error"><?= CHtml::encode($message); ?></p>
                    <?= CHtml::link('Secure login', '#', array(
                        'class' => 'secure-login'
                    )) ?>
                    <?= CHtml::link('Logout', '#', array(
                        'class' => 'logout'
                    )) ?>
                </div>
            <?php } else { ?>
                <p><?= CHtml::encode($message); ?></p>
            <?php } ?>
        </section>

</section>