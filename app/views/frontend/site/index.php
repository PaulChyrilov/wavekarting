<?php 
$this->setPageTitle(Yii::t('mainpage', 'Main page'));
?>
<?php if (false) { ?>
<div id="partner-site">
    <ul>
        <li class="blue">
            <a href="#">
                <div class="span">Our music</div>
                <div class="image" style="background-image:url(http://theme.crumina.net/onetouch/wp-content/uploads/2012/12/pppp1.png);"></div>
            </a>
        </li>
        <li class="green">
            <a href="#">
                <div class="span">Our life</div>
                <div class="image" style="background-image:url(http://theme.crumina.net/onetouch/wp-content/uploads/2013/02/Landscape.png);"></div>
            </a>
        </li>
        <li class="orange">
            <a href="#">
                <div class="span">Our rest</div>
                <div class="image" style="background-image:url(http://theme.crumina.net/onetouch/wp-content/uploads/2012/12/pppp1.png);"></div>
            </a>
        </li>
        <li class="blue">
            <a href="#">
                <div class="span">Site 5</div>
                <div class="image" style="background-image:url(http://theme.crumina.net/onetouch/wp-content/uploads/2012/12/pppp1.png);"></div>
            </a>
        </li>
        <li class="grey">
            <a href="#">
                <div class="span">Site 4</div>
                <div class="image" style="background-image:url(http://theme.crumina.net/onetouch/wp-content/uploads/2013/02/Landscape.png);"></div>
            </a>
        </li>
        <li class="orange last">
            <a href="#">
                <div class="span">Our rest</div>
                <div class="image" style="background-image:url(http://theme.crumina.net/onetouch/wp-content/uploads/2012/12/pppp1.png);"></div>
            </a>
        </li>
        
    </ul>
</div>
<?php } ?>

<div class="slider-block">
    <?php $this->widget('application.modules.gallery.widgets.SliderWidget', array(
        'code' => 'mail_slider',
        'id' => 'slider',
        'sliderOptions' => array(
            'loop' => true,
            'pagination' => '#slider .pagination',
            'createPagination' => false,
            'autoPlay' => 11000,
            'speed' => 700,
        ),
    )) ?>
</div>

<div class="our-info-block">
    <section>
        <?php $this->widget('application.modules.gallery.widgets.SwiperWidget', array(
            'id' => 'main-section',
            'options' => array(
                'createPagination' => false,
                'autoPlay' => 15000,
                'slideClass' => 'col3',
                'slidesPerSlide' => 3,
                'scrollbar' => array('hide' => false, 'container' => '#main-section .swiper-scrollbar')
            ),
        )) ?>
        <div class="wrapper swiper-container" id="main-section">
            <div class="swiper-wrapper" style="height: 185px;">                
            <?= block('mainpage_section', array(
                '{SERVICE_URL}' => $this->createUrl('/services/services/category', array('code' => 'production')),
                '{SOLUTION_URL}' => $this->createUrl('/solutions/solutions/index'),
                '{TECNOLOGY_URL}' => $this->createUrl('/pages/pages/show', array('slug' => 'technology')),
            )) ?>
            </div>
            <div class="swiper-scrollbar"></div>
        </div>
    </section>
</div>

<div class="info-block">
    <section>
        <div class="wrapper">
            <div class="col3">
                <article class="twitter-block">
                    <?php $this->widget('ext.widgets.twitter.TweetWidget', array(
                        'limit' => 10, 
                        'channel' => param('twitter.channel'),
                        'settings' => array(
                            'oauth_access_token' => param('twitter.oauth_access_token'),
                            'oauth_access_token_secret' => param('twitter.oauth_access_token_secret'),
                            'consumer_key' => param('twitter.consumer_key'),
                            'consumer_secret' => param('twitter.consumer_secret')
                        ),
                        'id' => 'tweets'
                    )) ?>
                </article>
            </div>
            <div class="col3">
                <article class="news-block">
                    <?php $this->widget('application.modules.blog.widgets.LatestPostWidget', array('limit' => 1, 'view' => 'single_latest_post')) ?>
                </article>
            </div>
            <div class="col3">
                <figure class="video-link-block">
                    <?= block('mainpage_video') ?>
                </figure>
            </div>
        </div>
    </section>
</div>
<?php 
Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/css/pages/index.css');     
?>