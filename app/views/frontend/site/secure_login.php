<section class="body-page">

    <header>
        <h2>Login</h2>
    </header>

    <section class="content-block">

        <div class="secure-login-block">

            <div class="secure-login">
                <h2>Secure login</h2>
                <? $this->renderPartial('__form', array(
                    'action' => '#'
                )) ?>
            </div>


            <div class="anonymous-login">
                <h2>Anonymous login</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                <?= CHtml::link('Anonymous login', '#', array())?>
            </div>

            <div class="clear"></div>
        </div>

    </section>

</section>

<? Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/pages/index.css') ?>