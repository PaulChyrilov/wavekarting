<div class="box-modal step2">
    <section>

        <header><b>Step 2:</b></header>
        
        <?= CHtml::dropDownList(
            'industry', '', 
            Yii::app()->getModule('categories')->getListData('file_industry'), 
            array(
                'encode' => false, 
                'prompt' => 'Select your industry ...',
                'data-action' => $this->createUrl('/files/industry'),
                'id' => 'file-industry'
            )
        )?>

    </section>

</div>

