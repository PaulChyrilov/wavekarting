<div class="box-modal step4">
    <section>
        <header><b>Step 4:</b> Please make your selection and download the pdf file</header>
        <?php if (count($files) > 0) { ?>
        <?= CHtml::form(array('/files/download'), 'post', array('id' => 'file-download-form')) ?>
            <div class="selections">
                <?php foreach ($files as $file) { ?>
                <label><?= CHtml::checkBox('files[]', false, array('value' => $file->id)) . CHtml::encode($file->getI18n('title'))?></label>
                <?php } ?>
            </div>
            <div class="errorMessage" id="file-download-form-em" style="display: none"><?= Yii::t('investors', 'Please select files.') ?></div>
            <div class="control">
                <button><?= Yii::t('common', 'Continue') ?></button>
            </div>
        <?= CHtml::endForm() ?>
        <?php } else { ?>
            <div class="selections">
                <?= Yii::t('investors', 'The files were not found.') ?>
            </div>
        <?php } ?>

    </section>
</div>
