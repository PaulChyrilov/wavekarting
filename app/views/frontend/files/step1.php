<div class="box-modal step1">
<div class="box-modal_close arcticmodal-close">X</div>

    <section>
        <header><b>Step 1:</b></header>

        <p>Complete the fields and download your complimentary white papers, case study's, regulatory and brochures.</p>

        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'     => 'first-step-form',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'afterValidate' => 'js:function(form, data, hasError) {
                    if (!hasError) {
                        if (form.data("sending")) {
                            return false;
                        }
                        form.data("sending", 1);
                        $.post(form.attr("action"), form.serialize(), function(rsp) {
                            $.arcticmodal("close");
                            $.arcticmodal({content: rsp});
                        });
                    }
                    
                    return false;
                }'
            )
        )); ?>
            <table>
                <tr>
                    <td><?= $form->label($model, 'firstname'); ?></td>
                    <td><?= $form->label($model, 'lastname'); ?></td>
                </tr>
                <tr>
                    <td>
                        <?= $form->textField($model, 'firstname'); ?>
                        <?= $form->error($model, 'firstname'); ?>
                    </td>
                    <td>
                        <?= $form->textField($model, 'lastname'); ?>
                        <?= $form->error($model, 'lastname'); ?>
                    </td>
                </tr>
                <tr class="offset">
                    <td><?= $form->label($model, 'email'); ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <?= $form->textField($model, 'email'); ?>
                        <?= $form->error($model, 'email'); ?>
                    </td>
                    <td>
                        <button type="submit"><?= Yii::t('common', 'Continue') ?></button>
                    </td>
                </tr>
            </table>
        <?php $this->endWidget() ?>

    </section>

</div>
