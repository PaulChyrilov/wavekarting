<div class='box-modal step3'>
    <section>

        <header><b>Step 3:</b> make your selection</header>

        <?= CHtml::form(array('/files/type'), 'post', array('id' => 'file-type-form')) ?>
            <div class="selections">
                <?php foreach (Yii::app()->getModule('categories')->getListData('file_type') as $key => $text) { ?>
                <label><input type="checkbox" name="type[]" value="<?= $key ?>"/><?= CHtml::encode(strtr($text, array('&nbsp;' => '')))?></label>
                <?php } ?>
            </div>
            <div class="errorMessage" id="file-type-form-em" style="display: none"><?= Yii::t('investors', 'Please select entry.') ?></div>
            <div class="control">
                <button type="submit"><?= Yii::t('common', 'Continue') ?></button>
            </div>
        <?= CHtml::endform() ?>

    </section>

</div>
