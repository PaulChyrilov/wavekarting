<?php $this->beginContent('//layouts/main') ?>

<section class="body-page">
    <header>
        <h2><?= Yii::t('site', 'Blog')?></h2>
    </header>

    <section class="content-block backgrounded">

        <div class="blog">
            <section class="content">
                <div class="wrapper">

                    <?= $content ?>

                </div>
            </section>
            <section class="sidebar">
                <?= $this->clips['blog_sidebar'] ?>                
            </section>

            <div class="clear"></div>
        </div>

    </section>

</section>

<?php Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl.'/css/pages/blog.css') ?>


<?php $this->endContent(); ?>