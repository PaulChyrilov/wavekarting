<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />

<!--    <meta name="viewport" content="width=device-width, user-scalable=yes, minimum-scale=0.1, maximum-scale=2.0, initial-scale=0.1" />-->

    <meta http-equiv="clear type" content="on" />

    <link type="text/css" href="/css/reset.css" rel="stylesheet" media="all"/>
    <link type="text/css" href="/css/main.css?2" rel="stylesheet" media="all"/>

	<!--[IF lt IE 9]><link type="text/css" href="/css/ie.css" rel="stylesheet" media="all"/><![ENDIF]-->

    <?php
        Yii::app()->getClientScript()
            ->registerCoreScript('yiiactiveform')
            ->registerCoreScript('articmodal')
            ->registerScriptFile('/js/lib/html5shiv.js', CClientScript::POS_HEAD)
            ->registerScriptFile('/js/main.js?2', CClientScript::POS_END)
            ->registerScriptFile('/js/lib/menu/jquery.hoverIntent.minified.js', CClientScript::POS_END)
            ->registerScriptFile('/js/lib/menu/jquery.dcmegamenu.1.2.js', CClientScript::POS_END)
            ->registerScript('menu', "
                $('#mega-menu').dcMegaMenu({
                    rowItems: '3',
                    speed: 'fast',
                    effect: 'fade'
                });
            ", CClientScript::POS_END)
            ->registerMetaTag($this->metaKeywords, 'keywords')
            ->registerMetaTag($this->metaDescription, 'description')

    ?>

    <title><?= CHtml::encode($this->getPageTitle() . ' / ' . param('project_name')); ?></title>
</head>
<body>

<div id="wrapper">

    <header>
        <section>

            <div class="logotype">
                <figure>
                    <a href="/" id="exampleModal3">
                        <img src="<?= param('logo') ?>" />                        
                    </a>
                </figure>
            </div>

            <nav>
                <?php $this->widget('ext.widgets.langselector.SelectorWidget'); ?>
                <div class='group-login'>
                    <div class="left-radius"></div>
                    <ul class="top-login-menu">
                        <?php if ($this->getUser()->getIsGuest()) { ?>
                        <li><?= CHtml::link(Yii::t('mainpage', 'Login'), array('/site/login'))?></li>
                        <li><?= CHtml::link(Yii::t('mainpage', 'Register'), array('/site/index'))?></li>
                        <?php } else { ?>
                        <li><?= CHtml::link(Yii::t('mainpage', 'Logout'), array('/site/logout'))?></li>
                        <?php } ?>
                    </ul>
                    <div class="right-radius"></div>
                </div>
            </nav>

            <nav class="top-navigate-menu">
                <?php $this->widget('zii.widgets.CMenu', array(
                    'id' => 'mega-menu',
                    'items' => Yii::app()->getModule('menu')->getByCode('header_menu')
                )) ?>
            </nav>

        </section>
    </header>

    <section id='content'>

        <?= $content ?>

    </section>

</div>

<footer>
    <section id="footer-section">

        <div class="links">
            <div class="wrapper">
                <div class="col3">
                    <nav>
                        <h3><?= Yii::t('site', 'Company info') ?></h3>
                        <?php $this->widget('zii.widgets.CMenu', array(
                            'items' => Yii::app()->getModule('menu')->getByCode('company_info')
                        )) ?>
                    </nav>
                </div>
                <div class="col3">
                    <nav>
                        <h3><?= Yii::t('site', 'Our Companies') ?></h3>
                        <?php $this->widget('zii.widgets.CMenu', array(
                            'items' => Yii::app()->getModule('menu')->getByCode('other_links')
                        )) ?>
                    </nav>
                </div>
                <div class="col3">
                    <nav>
                        <?= block('social_contacts') ?>
                    </nav>
                </div>
            </div>
        </div>

        <div class="functional-block">
            <div class="search-block">
                <?= CHtml::form(array('/site/search'), 'get') ?>
                    <input type="text" name="q" placeholder="Search word..."/>
                    <div class="background-button">
                        <button type="submit"></button>
                    </div>
                <?= CHtml::endForm() ?>
            </div>
            <div class="newsletter-block">
                <?php $this->widget('ext.widgets.subscribe.SubscribeWidget'); ?>
            </div>
        </div>

    </section>

    <div class="bottom-info-line">
        <section>
            <p><?= block('footer_copyright') ?></p>
            <?php $this->widget('zii.widgets.CMenu', array(
                'items' => Yii::app()->getModule('menu')->getByCode('footer_menu'),
                'htmlOptions' => array('class' => 'footer-bottom-menu')
            )) ?>
        </section>
    </div>

</footer>

</body>
</html>