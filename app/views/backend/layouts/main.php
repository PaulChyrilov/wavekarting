<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?= $this->getPageTitle(); ?></title>
	<meta name="viewport" content="width=device-width">

	<style>
		body {
			padding-top: 60px;
			padding-bottom: 40px;
		}
	</style>
    <?php
        Yii::app()->getClientScript()
            ->registerCssFile('/css/backend.css')
            ->registerCoreScript('yiiactiveform')
            ->registerScriptFile('/js/main.js', CClientScript::POS_END)
            ->registerMetaTag($this->metaKeywords, 'keywords')
            ->registerMetaTag($this->metaDescription, 'description')
    ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
	your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
	improve your experience.</p>
<![endif]-->

<!-- This code is taken from http://twitter.github.com/bootstrap/examples/hero.html -->
<?php 
$isGuest = $this->getUser()->getIsGuest();
$this->widget('bootstrap.widgets.TbNavbar', array( 
    'brand' => Yii::app()->name,
    'htmlOptions' => array('id' => 'main-navbar'),
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbMenu',
            'id' => 'content-menu',
            'items' => array(
                array(
                    'label' => 'Site',
                    'url' => '#',
                    'visible' => !$isGuest,
                    'submenuOptions' => array(
                        'id' => 'content-menu2'
                    ),
                    'items' => array(
                        array('label' => 'Services', 'url' => array('/services/services/admin')),
                        array('label' => 'Solutions', 'url' => array('/solutions/solutions/admin')),
                        array('label' => 'Careers', 'url' => array('/careers/careers/admin')),
                        array('label' => 'Team members', 'url' => array('/team/members/admin')),
                        array('label' => 'Locations', 'url' => array('/locations/locations/admin')),
                        array('label' => 'Glossary', 'url' => array('/glossary/items/admin')),                       
                        array('label' => 'Files', 'url' => array('/filemanager/files/admin')),                       
                        array('label' => 'FAQ', 'url' => array('/faq/faqs/admin')),                       
                    )
                ),
                array(
                    'label' => 'Content',
                    'url' => '#',
                    'visible' => !$isGuest,
                    'items' => array(
                        array('label' => 'Menus', 'url' => array('/menu/menus/admin')),
                        array('label' => 'Categories', 'url' => array('/categories/categories/admin')),
                        array('label' => 'Pages', 'url' => array('/pages/pages/admin')),
                        array('label' => 'Blog', 'url' => array('/blog/posts/admin')),
                        array('label' => 'Galleries', 'url' => array('/gallery/galleries/admin')),
//                        array('label' => 'News', 'url' => array('/news/news/admin')),
                        array('label' => 'Content blocks', 'url' => array('/contentblock/contents/admin')),
                    )
                ),
                array(
                    'label' => 'System',
                    'url' => '#',
                    'visible' => !$isGuest,
                    'items' => array(
                        array('label' => 'Admins', 'url' => array('/admins/admins/admin')),
                        array('label' => 'Languages', 'url' => array('/languages/languages/admin')),
                        array('label' => 'Email templates', 'url' => array('/mail/mailtemplates/admin')),
                        array('label' => 'Settings', 'url' => array('/settings/settings/admin')),
                    )
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbMenu',
            'htmlOptions'=>array('class' => 'pull-right'),
            'items' => array(
                array('label' => 'Login', 'url' => array('/site/login'), 'visible' => $isGuest),
                array('label' => CHtml::encode('Welcome, ' . $this->getUser()->getState('username')), 'url' => '#', 'visible' => !$isGuest),
                array('label' => 'Logout', 'url' => array('/site/logout'), 'visible' => !$isGuest)
            )
        ),
    ),
));new TbNavbar;
?>
    <div class="container">
        <?php $this->widget('bootstrap.widgets.TbAlert'); ?>
        <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
            'links' => $this->breadcrumbs
        )); ?>
        <?= $content; ?>    
    </div>
    
<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'popup-modal')); ?>
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4 id="popup-title">Modal header</h4>
    </div>

    <div class="modal-body" id="popup-content"></div>
<?php $this->endWidget(); ?>

</body>
</html>