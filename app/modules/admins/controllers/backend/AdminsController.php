<?php

class AdminsController extends BackendController
{
	public $modelName = 'Admin';
	public $redirectTo = array('admin');
    public $defaultAction = 'admin';
    
    public function actionUpdate($id)
    {
        $model = $this->loadModel('Admin', $id);
        $oldPassword = $model->password;
        if (isset($_POST['Admin'])) {
            $model->setAttributes($_POST['Admin']);
            if ($model->validate()) {
                if (trim($model->password) === '') {
                    $model->password = $oldPassword;
                } else {
                    $model->password = $model->hashPassword($model->password);
                }
                $model->save(false);
                Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('common', 'Successfully updated.'));
                $this->redirect($this->redirectTo);
            }
        } else {
            $model->password = null;
        }

        $this->render('update', array('model' => $model));
    }
    
}