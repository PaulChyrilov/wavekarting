<?php
$this->pageTitle = 'Create admin';
$this->breadcrumbs=array(
	'Manage admins' => array('admin'),
	'Create admin',
);

$this->renderPartial('_form', array('model' => $model)); 
?>