<?php
$this->pageTitle = 'Manage admins';

$this->breadcrumbs=array(
	'Admins',
);
?>

<h1>Manage admins</h1>

<?= TbHtml::link(Yii::t('admins.backend', 'Create a new admin'), array('create'), array('class' => 'btn')); ?>
<br />
<?php
$provider = $model->search();

$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => $model->getGridId(),
	'dataProvider' => $provider,
	'filter' => $model,
    'template' => "{items}\n{pager}",
	'columns' => array(
		'username',
		'email',
		array(
			'name' => 'status',
			'value' => 'Admin::statuses($data->status)',
            'filter' => Admin::statuses()
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} {delete}', 
			'deleteConfirmation' => 'Are you sure to delete this user?',
            'buttons' => array(
                'delete' => array(
                    'visible' => $provider->getTotalItemCount() . ' > 1',
                ),
            )
		),
	),
)); 
?>