<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
    ));
?>
<fieldset>
	<?= $form->errorSummary($model); ?>
    
    <?= $form->textFieldRow($model, 'username', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'email', array('class' => 'span8')); ?>
    <?= $form->textFieldRow($model, 'password', array('class' => 'span8')); ?> 
    <?= $form->dropDownListRow($model, 'status', $model::statuses()); ?>
    
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(t('admins.backend', $model->getIsNewRecord() ? 'Create' : 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
))?>


<?php $this->endWidget(); ?>
