<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
?>
<fieldset>
	<?= $form->errorSummary($model); ?>
    
    <?= $form->textFieldRow($model, 'name', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'position', array('class' => 'span8')); ?>
    <?= $form->fileFieldRow($model, 'image', array('hint' => 'image size: 222x222 <br/>' . ($model->image ? CHtml::image($model->getImageUrl('image', 'image_preview'), '') : ''))); ?>
    
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(t('team.backend', $model->getIsNewRecord() ? 'Create' : 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
))?>


<?php $this->endWidget(); ?>
