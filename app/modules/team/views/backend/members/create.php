<?php
$this->pageTitle = 'Create team member';
$this->breadcrumbs=array(
	'Manage team members' => array('admin'),
	'Create team member',
);

$this->renderPartial('_form', array('model' => $model)); 
?>