<?php
$this->pageTitle = 'Update team member';
$this->breadcrumbs=array(
	'Manage team members' => array('admin'),
	'Update team member',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->name) . '"' ?> team member</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>