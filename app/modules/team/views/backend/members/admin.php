<?php
$this->pageTitle= 'Manage team members';

$this->breadcrumbs=array(
	'Team members',
);
?>

<h1>Manage team members</h1>

<?= TbHtml::link(Yii::t('team.backend', 'Create a new team member'), array('create'), array('class' => 'btn')); ?>
<br />

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => $model->getGridId(),
	'dataProvider' => $model->search(),
	'filter' => $model,
    'template' => "{items}\n{pager}",
	'columns' => array(
        array('header' => 'Image', 'value' => 'CHtml::image($data->getImageUrl("image", "image_preview"))', 'type' => 'raw', 'htmlOptions' => array('style' => 'width:110px;')),
		'name',
        'position',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'deleteConfirmation' => 'Are you sure to delete this member?',
			'template' => '{update} {delete}',
		),
	),
)); 
?>