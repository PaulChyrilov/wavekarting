<?php
$this->breadcrumbs=array(
    'News' => array('/news/news/index'),
    $model->title
);
?>

<div class="news-item">
    <p class="date"><?= $model->published_at; ?></p>
    <p class="title"><h2><?= CHtml::encode($model->title); ?></h2></p>
    <div class="desc">
        <?= $model->content; ?>
    </div>
</div>