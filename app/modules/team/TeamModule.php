<?php

class TeamModule extends EWebModule 
{
    public $defaultController = 'members';
    
	public function init()
	{
        parent::init();
        Yii::app()->setComponents(array(
            'image' => array(
                'presets' => array(
                    'team_view' => array(
                        'adaptiveThumb' => array('width'  => 222, 'height' => 222)
                    ),
                )
            )
        ));
        
		$this->setImport(array(
			'team.models.*',
		));
	}
    
}
