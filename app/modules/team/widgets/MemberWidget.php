<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MemberWidget
 *
 * @author mlapko
 */
class MemberWidget extends CWidget
{
    public $limit = 10;
    
    public function run()
    {
        Yii::app()->getModule('team');
        $this->render('members', array('members' => Member::model()->findAll(array(
            'limit' => $this->limit
        ))));
    }
    
}
