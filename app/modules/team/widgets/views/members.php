<div class="team">
    <?php foreach ($members as $member) { ?>
        <div class="col4">
            <figure>
                <?php // CHtml::image($member->getImageUrl('image', 'team_view'), $member->name)?>
                <h3><?= CHtml::encode($member->name)?></h3>
                <p><?= CHtml::encode($member->position)?></p>
            </figure>
        </div>        
    <?php } ?>
</div>
