<?php

class Member extends EActiveRecord
{
    public $name;
    public $position;
    public $created_at;
    public $updated_at;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'team_members';
    }

    public function rules()
    {
        return array(
            array('name, position', 'required'),
            array(
                'image', 'ext.components.image_processor.MImageValidator',
                'types' => array('jpg', 'png', 'jpeg'), 'maxSize' => 5 * 1024 * 1024, 
                'minWidth' => 100, 'minHeight' => 100, 'allowEmpty' => true //!$this->getIsNewRecord()
            ),
            array('name, position', 'length', 'max' => 255),
            array('name, position', 'safe', 'on' => 'search'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'               => 'ID',
            'name'             => 'Name',
            'position'         => 'Position',
            'created_at'       => 'Create date',
            'updated_at'       => 'Update date',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('name', $this->name, true);
        $criteria->compare('position', $this->position, true);

        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created_at',
                'updateAttribute'   => 'updated_at',
                'setUpdateOnCreate' => true
            ),
            'mImage' => array(
                'class'          => 'ext.components.image_processor.MImageBehavior',
                'imageProcessor' => 'image' // image processor component name 
            )
        );
    }

}