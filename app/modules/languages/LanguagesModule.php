<?php

class LanguagesModule extends EWebModule 
{
    public $defaultController = 'languages';
    
    public function init()
    {
        parent::init();
        
        $this->setImport(array(
            'languages.models.*',
        ));
    }
    
    public function getDefault()
    {
        return Language::getDefault();
    }
    
    public function listing()
    {
        return Language::listing();
    }
    
    public function setLanguage($lang = null)
    {
        if ($lang === null) {
            $lang = $this->getLanguage();            
        }
        $app = Yii::app();
        $app->getSession()->add('language', $lang);
        $cookie = new CHttpCookie('language', $lang);
        $cookie->expire = time()+60*60*24*180; 
        $app->getRequest()->cookies['language'] = $cookie;
        $app->setLanguage($lang);
        return $lang;
    }
    
    public function getLanguage()
    {
        $key = 'language';
        $request = Yii::app()->getRequest();
        if (isset($_REQUEST[$key]) && !empty($_REQUEST[$key])) {
            $lang = $_REQUEST[$key];
        } elseif (Yii::app()->getSession()->contains($key)) {
            $lang = Yii::app()->getSession()->get($key);
        } elseif (isset($request->cookies['language'])) {
            $lang = $request->cookies['language']->value;
        } elseif ($request->getPreferredLanguage()) {
            $lang = $request->getPreferredLanguage();
        } else {
            $lang = $this->getDefault();
        }
        
        if (!array_key_exists($lang, $this->listing())) {
            if (strpos($lang, '_') !== false) {
                $lang = substr($lang, 0, 2);
                if (!array_key_exists($lang, $this->listing())) {
                    $lang = $this->getDefault();                    
                }
            } elseif (isset($_REQUEST[$key])) {
                throw new CHttpException(404, Yii::t('yii', 'Your request is invalid.'));
            } else {
                $lang = $this->getDefault();
            }
        }
        return $lang;
    }
    
}
