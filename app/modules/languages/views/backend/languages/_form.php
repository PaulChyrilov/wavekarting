<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
    ));
?>
<fieldset>
	<?= $form->errorSummary($model); ?>
    
    <?= $form->textFieldRow($model, 'id', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'name', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'locale', array('class' => 'span8')); ?>
    <?= $form->toggleButtonRow($model, 'is_default'); ?>
    <?= $form->toggleButtonRow($model, 'is_active'); ?>
    
    
    
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(t('languages.backend', $model->getIsNewRecord() ? 'Create' : 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
))?>


<?php $this->endWidget(); ?>
