<?php
$this->pageTitle = 'Update language';
$this->breadcrumbs=array(
	'Manage languages' => array('admin'),
	'Update language',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->id) . '"' ?> language</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>