<?php
$this->pageTitle = 'Create language';
$this->breadcrumbs=array(
	'Manage languages' => array('admin'),
	'Create language',
);

$this->renderPartial('_form', array('model' => $model)); 
?>