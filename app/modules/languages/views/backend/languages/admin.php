<?php
$this->pageTitle= 'Manage Languages';

$this->breadcrumbs=array(
	'Languages',
);
?>

<h1>Manage Languages</h1>

<?= TbHtml::link(Yii::t('languages.backend', 'Create a new language'), array('create'), array('class' => 'btn')); ?>
<br />

<?php $this->renderPartial('_list', compact('model')) ?>