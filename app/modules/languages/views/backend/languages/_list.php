<?php
$dataProvider = $model->search();
$maxOrder = $model->getMaxOrder();
//new TbGridView;
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => $model->getGridId(false),
	'dataProvider' => $dataProvider,
	'filter' => $model,
    'ajaxUrl' => array('/languages/languages/admin'),
    'template' => "{items}\n{pager}",
	'columns' => array(
		array(
			'name' => 'id',
            'sortable' => false,
		),
		array(
			'name' => 'name',
			'value' => '$data->name',
			'sortable' => false,
		),
		array(
			'name' => 'is_default',
            'value' => '$data->is_default ? "Yes" : "No"',
			'filter' => false,
            'sortable' => false,
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'deleteConfirmation' => 'Are you sure to delete this language?',
			'template' => '{up} {down} {update} {delete}',
			'htmlOptions' => array('class' => 'buttons_column'),
            'buttons' => array(
                'delete' => array(
                    'visible' => '!$data->is_default',
                ),
                'up' => array(
                    'url' => 'Yii::app()->createUrl("languages/languages/move", array("id" => $data["id"], "dir" => "up"))',
                    'visible' => '$data->order != 1',
                    'label' => Yii::t('languages.backend', 'Up'),
                    'options' => array('data-op' => 'ajax'),
                    'icon' => 'chevron-up'
                ),
                'down' => array(
                    'url' => 'Yii::app()->createUrl("languages/languages/move", array("id" => $data["id"], "dir" => "down"))',
                    'visible' => '$data->order != ' . $maxOrder,
                    'label' => Yii::t('languages.backend', 'Down'),
                    'options' => array('data-op' => 'ajax'),
                    'icon' => 'chevron-down'
                ),
            )
		),
	),
)); 
?>