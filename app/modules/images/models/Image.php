<?php

class Image extends EActiveRecord
{
    public $id;
    public $image;
    public $order;
    public $title;
    public $description;
    public $url;
    public $style;
    public $model_type;
    public $model_id;
    
    public $created_at;
    public $updated_at;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'images';
    }

    public function rules()
    {
        return array(
            array('model_type, model_id', 'required'),
            array('url, title, description, style', 'safe'),
            array(
                'image', 'ext.components.image_processor.MImageValidator', 
                'types' => array('jpg', 'png', 'jpeg'), 'maxSize' => 5 * 1024 * 1024, 'on' => array('insert')
            ),
            array('model_type, model_id', 'safe', 'on' => 'search'),
        );
    }
    
    public function behaviors ()
    {
        return array(
            'CTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created_at',
                'updateAttribute'   => 'updated_at',
                'setUpdateOnCreate' => true
            ),
            'mImage' => array(
                'class'          => 'ext.components.image_processor.MImageBehavior',
                'imageProcessor' => 'image' // image processor component name 
            )
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'               => 'ID',
            'title'            => 'Title',
            'image'            => 'Image',
            'url'              => 'Url',
            'style'            => 'Custom style',
            'description'      => 'Description',
            'model_type'       => 'Model type',
            'model_id'         => 'Model ID',
            'created_at'       => 'Create date',
            'updated_at'       => 'Update date',
            'order'            => 'Order',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('model_type', $this->model_type);
        $criteria->compare('model_id', $this->model_id);

        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'sort'       => array(
                'defaultOrder' => 'model_type ASC',
            ),
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }
    
    protected function afterDelete()
    {
        parent::afterDelete();
        $class = $this->model_type;
        $presents = array('image_preview', 'orig');
        if (@class_exists($class)) {
            $presents = array_merge($presents, $class::getImagePresets());            
        }
        $this->deleteImage('image', $presents);
    }
}