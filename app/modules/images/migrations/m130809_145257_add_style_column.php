<?php

class m130809_145257_add_style_column extends CDbMigration
{

    public function safeUp()
    {
        $sql = <<< EOD
ALTER TABLE `images`
ADD COLUMN `style`  text NULL AFTER `url`;
EOD;
        $this->execute($sql);
    }

    public function down()
    {
        
    }
}