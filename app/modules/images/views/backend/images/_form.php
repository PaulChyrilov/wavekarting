<div class="image-item media <?= "image-{$model->id}" ?>">
    <a class="close" data-op="ajax" class="image-delete" href="<?= $this->createUrl('/images/images/delete', array('id' => $model->id)) ?>" data-confirm="Are you ready to delete?">&times;</a>
    <div class="pull-left">
        <?= CHtml::image($model->getImageUrl('image', 'image_preview'), '', array('class' => 'media-object')) ?>
    </div>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => "image-{$model->id}-form",
    'htmlOptions' => array('class' => 'image-item-form'),
    'action' => array('/images/images/update', 'id' => $model->id)
)); ?>
    <div class="media-body">
        <?= $form->textFieldRow($model, 'title', array('class' => 'span8', 'id' => "title-{$model->id}")); ?>
        <?= $form->textAreaRow($model, 'description', array('class' => 'span8', 'rows' => 3, 'id' => "description-{$model->id}")); ?>
        <?= $form->textFieldRow($model, 'url', array('class' => 'span8', 'id' => "url-{$model->id}")); ?>
        <?= $form->textAreaRow($model, 'style', array('class' => 'span8', 'rows' => 3, 'id' => "style-{$model->id}")); ?>
        <div>
            <?= TbHtml::submitButton(Yii::t('images.backend', 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY)); ?>            
        </div>
    </div>    
<?php $this->endWidget(); ?>
</div>

<?php Yii::app()->getClientScript()->registerScript('image_item_edit', '
    CMS.$body.on("submit.ajax", ".image-item-form", function() {
        CMS.submitForm($(this), function(err, data) {
            console.log(err, data);
        });
        return false;
    });
') ?>

