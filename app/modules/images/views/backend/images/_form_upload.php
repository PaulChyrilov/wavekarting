<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'image-form',
    'type' => 'horizontal',
)); ?>
<fieldset>
    <div id="dropbox"></div>
    <?php
        Yii::import('ext.components.fuploader.FUploader');
        FUploader::add('dropbox', array(
            'action' => Yii::app()->createUrl('/images/images/upload', array('id' => $model->id, 'type' => get_class($model))),
            'name'   => 'Image[image]',
            'onComplete' => 'js: function(id, filename, response) {
                if (response.success) {
                    $("' . $target . '").append(response.html);
                } else {
                    alert(response.message);
                }
                $(".qq-upload-list li.qq-upload-success,.qq-upload-list li.qq-upload-fail").remove();
            }',
            'template' =>
                '<div class="dropbox">
                    <div class="inner"></div>
                    <div class="qq-upload-button button">Select file ...</div>
                    <div class="drop-area" id="drag-zone">'. Yii::t('images.backend', 'Drop image here to upload') .'</div>
                    <ul class="qq-upload-list"></ul>
                    <span class="clearfix"></span>
                </div>',
            'classes' => array(
                'dropActive' => 'drop-area-active',
                'list' => 'qq-upload-list',
                'file' => 'qq-upload-file',
                'spinner' => 'qq-upload-spinner',
                'size' => 'qq-upload-size',
                'cancel' => 'qq-upload-cancel',
                'success' => 'qq-upload-success',
                'fail' => 'qq-upload-fail',
                'drop' => 'drop-area',
                'button' => 'button',
            ),
            'fileTemplate' =>
                '<li>
                    <div>
                        <span class="qq-upload-spinner"></span>
                        <span class="qq-upload-size"></span>
                    </div>
                    <a class="qq-upload-cancel" href="#" style="display:none">Cancel</a>
                    <span class="qq-upload-failed-text" style="display:none">Error</span-->
                    <span class="qq-upload-file"></span>
                </li>',
        )); ?>
</fieldset>
<?php $this->endWidget(); ?>
