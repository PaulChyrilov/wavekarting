<?php

class ImagesController extends BackendController
{
    public $modelName = 'Image';
    public $redirectTo = array('admin');
    public $defaultAction = 'admin';
    
    public function actionUpload($type, $id)
    {
        $image = new Image;
        $image->model_type = $type;
        $image->model_id = $id;
        
        if ($image->validate()) {
            $image->uploadImage(CUploadedFile::getInstance($image, 'image'), 'image');            
            if ($image->save(false)) {
                $this->renderJson(array(
                    'success' => true, 
                    'id' => $image->id, 
                    'html' => $this->renderPartial('_form', array('model' => $image), true)
                ));               
            }
        } else {
            $this->renderJson(array('success' => false, 'message' => $image->getError('image')));
        }
    }
    
    public function actionUpdate($id)
    {
        $model = $this->loadModel($this->modelName, $id);
        
        if (isset($_POST[$this->modelName])) {
            $model->setAttributes($_POST[$this->modelName]);
            $this->renderJson(array('success' => $model->save()));
        }
        $this->renderJson(array('success' => false));
    }
    
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($this->modelName, $id)->delete();
            $this->renderJson(array('success' => true, 'html' => '', 'target' => ".image-$id"));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }
    
    public function actionRedactorUpload()
    {
        $file = new ImageForm();        
        if ($file->perform()) {
            $this->renderJson(array('success' => true, 'filelink' => $file->getUrl()));            
        } else {
            $this->renderJson(array('success' => false, 'error' => $file->getError('file')));
        }
    }
    
    public function actionRedactorIndex()
    {
        $image = Yii::app()->getComponent('image');
        $url = $image->imageUrl . 'redactor/';
        $result = array();
        foreach (CFileHelper::findFiles(Yii::getPathOfAlias($image->imagePath) . '/redactor') as $file) {
            $result[] = array(
                'thumb' => $url . basename($file),
                'image' => $url . basename($file),
            );
        }
        $this->renderJson($result);
    }
    
}