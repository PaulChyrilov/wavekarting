<?php

class Gallery extends EActiveRecord
{
    public $id;
    public $name;
    public $code;
    public $settings;    
    public $created_at;
    public $updated_at;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'galleries';
    }

    public function rules()
    {
        return array(
            array('name, code', 'required'),
            array('code', 'unique'),
            array('settings', 'safe'),
            array('name, code', 'safe', 'on' => 'search'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'               => 'ID',
            'name'             => 'Name',
            'code'             => 'Code',
            'settings'         => 'Settings'
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('name', $this->name, true);
        $criteria->compare('code', $this->code, true);

        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created_at',
                'updateAttribute'   => 'updated_at',
                'setUpdateOnCreate' => true
            ),
        );
    }
    
    public static function getByCode($code)
    {
        return self::model()->findByAttributes(array('code' => $code));
    }
    
    public function getImages()
    {
        return Yii::app()->getModule('images')->getImages($this);
    }
    
    public function jsonSettings()
    {
        return CJSON::decode($this->settings);
    }

}