<?php

class GalleriesController extends BackendController
{
	public $modelName = 'Gallery';
	public $redirectTo = array('admin');
    public $defaultAction = 'admin';
    
    public function actionImages()
    {
        $id = Yii::app()->getRequest()->getParam('id');        
        $gallery = $this->loadModel('Gallery', $id);
        
        $provider = new CArrayDataProvider($gallery->getImages());
        $this->render('_images', compact('gallery', 'provider'));
    }
    
}