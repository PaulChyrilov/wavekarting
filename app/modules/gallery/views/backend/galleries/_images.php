<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'type'=>'striped bordered',
	'dataProvider' => $provider,
	'template' => "{items}",
    'id' => $gallery->getGridId(),
	'columns' => array(
        array('header' => 'Image', 'value' => 'CHtml::image($data->getImageUrl("image", "image_preview"))', 'type' => 'raw', 'htmlOptions' => array('style' => 'width:110px;')),
        'title',
        'description'
//        array(
//            'class' => 'bootstrap.widgets.TbButtonColumn',
//            'template' => '{delete}',
//            'htmlOptions' => array('style' => 'width:80px;'),
//            'buttons' => array(                
//                'delete' => array(
//                    'url' => 'Yii::app()->createUrl("/images/images/delete", array("id" => $data->id))',
//                ),
//                'up' => array(
//                    'url' => 'Yii::app()->createUrl("menu/items/move", array("id" => $data["id"], "dir" => "up"))',
//                    'visible' => '$data->level != 1',
//                    'label' => Yii::t('menu.backend', 'Up'),
//                    'options' => array('data-op' => 'ajax'),
//                    'icon' => 'chevron-up'
//                ),
//                'down' => array(
//                    'url' => 'Yii::app()->createUrl("menu/items/move", array("id" => $data["id"], "dir" => "down"))',
//                    'visible' => '$data->level != 1',
//                    'label' => Yii::t('menu.backend', 'Down'),
//                    'options' => array('data-op' => 'ajax'),
//                    'icon' => 'chevron-down'
//                ),
//            )
//        )
    ),
));