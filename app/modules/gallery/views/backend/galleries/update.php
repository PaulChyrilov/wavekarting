<?php
/* @var $this GalleriesController */
/* @var $model Gallery */

$this->breadcrumbs = array(
    'Galleries' => array('admin'),
    'Update',
);
$imagePath = Yii::app()->getModule('images')->getViewPath();
?>

<h1>Update gallery <?= CHtml::encode('"' . $model->name . '"'); ?></h1>

<?= $this->renderPartial('_form', array('model' => $model)); ?>

<?= $this->renderFile($imagePath . '/images/_form_upload.php', array('model' => $model, 'target' => '#images')); ?>

<div id="images">
<?php foreach ($model->getImages() as $image) { ?>
<?= $this->renderFile($imagePath . '/images/_form.php', array('model' => $image)); ?>
<?php } ?> 
</div>