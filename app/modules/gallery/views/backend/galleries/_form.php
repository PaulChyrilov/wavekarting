<?php
/* @var $this GalleriesController */
/* @var $model Gallery */
/* @var $form TbActiveForm */
?>
<br />
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'   => $model->getFormId(),
    'type' => TbHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>
<fieldset>
	<?= $form->errorSummary($model); ?>
    
    <?= $form->textFieldRow($model, 'name', array('maxlength' => 128)); ?>
    <?= $form->textFieldRow($model, 'code', array('maxlength' => 64)); ?>
    <?= $form->textAreaRow($model, 'settings', array('class' => 'span8', 'rows' => 5)); ?>

</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(Yii::t('gallery.backend', $model->getIsNewRecord() ? 'Create' : 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY)),
)); ?>
<?php $this->endWidget(); ?>