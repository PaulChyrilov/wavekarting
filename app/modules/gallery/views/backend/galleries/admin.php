<?php
/* @var $this GalleriesController */
/* @var $model Gallery */

$this->breadcrumbs = array(
	'Galleries' => array('admin'),
	'Manage',
);
?>

<h1>Manage galleries</h1>

<?= TbHtml::link(Yii::t('gallery.backend', 'Create a new gallery'), array('create'), array('class' => 'btn')); ?>
<br />

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'template' => "{items}\n{pager}",
    'id' => $model->getGridId(),
    'columns' => array(
        array(
            'class'=>'bootstrap.widgets.TbRelationalColumn',
            'name' => 'name',
            'url' => $this->createUrl('/gallery/galleries/images'),
            'afterAjaxUpdate' => 'js:function(tr, rowid, data) {}',
            'sortable' => false,
        ),
        array('name' => 'code', 'sortable' => false),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update} {delete}',
        )
    ),
)); ?>
