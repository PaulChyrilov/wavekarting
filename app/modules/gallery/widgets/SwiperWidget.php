<?php

class SwiperWidget extends CWidget
{   
    public $options = array();
    
    
    public function run()
    {
        $this->_publish();
    }
    
    protected function _publish()
    {
        
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->getAssetManager()->publish($assets, false, -1, YII_DEBUG);
        if (is_dir($assets)) {
            $cs = Yii::app()->getClientScript()
                ->registerCoreScript('jquery')
                ->registerScriptFile($baseUrl . '/swiper.js', CClientScript::POS_END)
                ->registerCssFile($baseUrl . '/swiper.css');
            if (isset($this->options['scrollbar'])) {
                $cs->registerScriptFile($baseUrl . '/scrollbar.js', CClientScript::POS_END);
            }
            $cs->registerScript('swiper' . $this->getId(), '
                $("#' . $this->getId() . '").swiper(' . CJavaScript::encode($this->options) . ');
            ');
        } else {
            throw new CHttpException(500, get_class($this) . ' - Error: Could not find assets to publish.');
        }
    }
}