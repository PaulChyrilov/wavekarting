<?php

class SliderWidget extends CWidget
{   
    public $code;
    public $view = 'slider';
    public $sliderOptions = array();
    
    
    public function run()
    {
        $gallery = Yii::app()->getModule('gallery')->getByCode($this->code);
        if ($gallery === null) {
            return;
        }
        $image = Yii::app()->getComponent('image');
        $image->presets = array_merge($image->presets, $gallery->jsonSettings());
        $this->render($this->view, array('images' => $gallery->getImages()));
    }
}
