<div id="slider" class="swiper-container">
    <div class="swiper-wrapper">
        <?php foreach ($images as $img) { ?>
        <div class="swiper-slide">
            <img src="<?= $img->getImageUrl('image', 'slider_image') ?>" alt=""/>
            <section<?php if ($img->style) echo ' style="' . $img->style . '"'; ?>>
                <header><?= CHtml::encode($img->title) ?></header>
                <p><?= CHtml::encode($img->description) ?></p>
                <?php if ($img->url) { ?>
                    <?= CHtml::link(Yii::t('common', 'read more &raquo;'), HApplication::createMenuUrl($img->url), array('class' => 'button')) ?>
                <?php } ?>
            </section>
        </div>
        <?php } ?>
    </div>
    <ol class="pagination">
        <?php for ($i = 0; $i < count($images); ++$i) { ?>
        <li class="swiper-pagination-switch"><a href="#"></a></li>
        <?php } ?>
    </ol>
</div>
<?php
$baseUrl = Yii::app()->baseUrl;
$this->getController()->widget('application.modules.gallery.widgets.SwiperWidget', array('id' => $this->getId(), 'options' => $this->sliderOptions));

Yii::app()->getClientScript()->registerCssFile($baseUrl . '/css/pages/slider.css?3')
    ->registerScript('pager-swiper' . $this->getId(), "
        var slider = $('#slider');
        slider.find('.swiper-pagination-switch').click(function(e) {
            e.preventDefault();
            slider.data('swiper').swipeTo($(this).index()); 
        });
        
        slider.find('section').each(function() {
            var el = $(this);
            el.css({top: Math.round((370 - el.height()) / 2) - 10 + 'px'});
        });

    ");

?>