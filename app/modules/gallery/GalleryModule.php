<?php

class GalleryModule extends EWebModule 
{
    public $defaultController = 'galleries';
    
	public function init()
	{
        parent::init();
        
		$this->setImport(array(
			'gallery.models.*',
		));
        Yii::app()->getModule('images');
	}
    
    public function getByCode($code, $exceptionOnNull = false)
    {
        $gallery = Gallery::getByCode($code);
        if ($exceptionOnNull && $gallery === null) {
            throw new CHttpException(404, 'Unable to find the requested object.');
        }
        return $gallery;
    }
    
}
