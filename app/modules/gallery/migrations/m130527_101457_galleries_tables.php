<?php

class m130527_101457_galleries_tables extends CDbMigration
{

    public function safeUp()
    {
        $sql = <<< EOD
DROP TABLE IF EXISTS `galleries`;
CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL, 
  `settings` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code_idx` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;    
EOD;
        $this->execute($sql);
    }

    public function down()
    {
        
    }
}