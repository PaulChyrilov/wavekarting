<?php
$this->pageTitle= 'Manage content blocks';

$this->breadcrumbs=array(
	'Content blocks',
);
?>

<h1>Manage content blocks</h1>

<?= TbHtml::link(Yii::t('contentblock.backend', 'Create a new content block'), array('create'), array('class' => 'btn')); ?>
<br />

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => $model->getGridId(),
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'template'     => "{items}\n{pager}",
    'columns'      => array(
        'code',
        array(
            'name'   => 'type',
            'value'  => 'ContentBlock::types($data->type)',
            'filter' => ContentBlock::types()
        ),
        array('name' => 'description', 'sortable' => false),
        array(
            'class'              => 'bootstrap.widgets.TbButtonColumn',
            'deleteConfirmation' => 'Are you sure to delete this block?',
            'template' => '{update} {delete}'
        ),
    ),
)); 
?>