<?php
$this->pageTitle = 'Update content block';
$this->breadcrumbs=array(
	'Manage content blocks' => array('admin'),
	'Update content block',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->code) . '"' ?> content block</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>