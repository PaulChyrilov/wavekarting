<?php

class ContentblockModule extends EWebModule 
{
    public $defaultController = 'contents';
    
    public function init()
    {
        parent::init();

        $this->setImport(array(
            'contentblock.models.*',
        ));
    }
    
    public function render($code, $data = array(), $lang = null)
    {
        $block = ContentBlock::getByCode($code, $lang);
        if ($block === null) {
            return '';
        }
        return strtr($block->show(), $data); 
    }
}
