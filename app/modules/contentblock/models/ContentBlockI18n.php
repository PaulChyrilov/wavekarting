<?php

class ContentBlockI18n extends EActiveRecord
{
    public $content;
    
    public $dirtyAttributes = array('content');

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'content_block_i18ns';
    }

    public function rules()
    {
        return array(
            array('content, lang_id', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'content' => 'Content',
        );
    }
}