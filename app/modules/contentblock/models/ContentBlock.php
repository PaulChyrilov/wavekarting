<?php

class ContentBlock extends I18nActiveRecord
{
    const TYPE_SIMPLE_TEXT = '1';
    const TYPE_PHP_CODE    = '2';
    const TYPE_HTML_TEXT   = '3';
    
    public $id;
    public $code;
    public $description;
    public $type;
    public $created_at;
    
    
    public $i18nModel = 'ContentBlockI18n';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'content_blocks';
    }

    public function rules()
    {
        return array(
            array('code', 'filter', 'filter' => 'trim'),
            array('code, type', 'required'),
            array('description', 'length', 'max' => 255),
            array('code', 'unique'),
            array('translateattrs', 'safe'),
            array('code, type, description', 'safe', 'on' => 'search'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'          => 'ID',
            'code'        => 'Code',
            'type'        => 'Type',
            'description' => 'Description',
            'created_at'  => 'Create date',       
        );
    }
    
    public function relations()
    {
        return array(
            'i18ns' => array(self::HAS_MANY, 'ContentBlockI18n', 'parent_id', 'index' => 'lang_id')
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('code', $this->code, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('type', $this->type);

        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'           => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => null
            ),
        );
    }
    
    public function setTranslateAttrs($i18ns)
    {
        $this->setNestedRelation('i18ns', $i18ns);
        $this->clearDirtyAttributes($this->i18ns);
    }
    
    public function show()
    {
        $output = '';
        switch ($this->type) {
            case self::TYPE_HTML_TEXT:
                $output = $this->getI18n('content');
                break;
            case self::TYPE_SIMPLE_TEXT:
                $output = CHtml::encode($this->getI18n('content'));
                break;
            case self::TYPE_PHP_CODE:
                $output = eval($this->getI18n('content'));
                break;
        }
        return $output;
    }
    
    public static function types($type = null)
    {
        static $types = array(
            self::TYPE_SIMPLE_TEXT => 'Simple text',
            self::TYPE_HTML_TEXT => 'Html code',
            self::TYPE_PHP_CODE => 'Php code',
        );
        return $type === null ? $types : $types[$type];
    }
    
    public static function getByCode($code, $lang = null)
    {
        static $blocks = array();
        $key = $code . '.' . $lang;
        if (!isset($blocks[$key])) {
            return $blocks[$key] = self::model()->withLang($lang)->findByAttributes(array('code' => $code));
        }
        return $blocks[$key];
    }
}