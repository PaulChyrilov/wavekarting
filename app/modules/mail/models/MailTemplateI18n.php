<?php

/**
 * This is the model class for table "mail_template_i18ns".
 *
 * The followings are the available columns in table 'mail_template_i18ns':
 * @property integer $id
 * @property string $lang_id
 * @property integer $parent_id
 * @property string $subject
 * @property string $content
 * @property string $content_plain
 */
class MailTemplateI18n extends EActiveRecord
{
    public $lang_id;
    public $parent_id;
    public $subject;
    public $content;
    public $content_plain;
    
    public $dirtyAttributes = array('subject', 'content', 'content_plain');
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MailTemplateI18n the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'mail_template_i18ns';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('content, subject, lang_id', 'required'),
            array('subject', 'length', 'max' => 128),
            array('content, content_plain', 'length', 'max' => 65534),
            array('content_plain', 'safe'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'subject'       => 'Subject',
            'content'       => 'Content',
            'content_plain' => 'Plain Content',
        );
    }
}