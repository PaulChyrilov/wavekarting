<?php
$this->pageTitle= 'Manage mail templates';

$this->breadcrumbs=array(
	'Mail templates',
);
?>

<h1>Manage mail templates</h1>

<?// TbHtml::link(Yii::t('news.backend', 'Create a new page'), array('create'), array('class' => 'btn')); ?>
<br />

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => $model->getGridId(),
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'template'     => "{items}\n{pager}",
    'columns'      => array(
        array(
            'header'   => 'Subject',
            'type'     => 'raw',
            'name'     => 'searchSubject',
            'value'    => 'CHtml::encode($data->getI18n("subject"))',
            'sortable' => false,
        ),
        'token',
        array(
            'class'              => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}'
        ),
    ),
)); 
?>