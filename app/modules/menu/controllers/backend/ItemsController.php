<?php

class ItemsController extends BackendController 
{    
    public function actionIndex() 
    {
        $id = Yii::app()->getRequest()->getParam('id');
        $menu = $this->loadModel('Menu', $id);
        $provider = $this->_getProvider($menu->getTree());
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            $this->renderPartial('index', compact('menu', 'provider'));
            Yii::app()->end();
        }
        $this->render('index', compact('menu', 'provider'));
    }
    
    public function actionEdit($menuId, $id = null)
    {
        $menu = $this->loadModel('Menu', $menuId);
        if ($id == null) {
            $item = new MenuItem;
        } else {
            $item = $this->loadModel('MenuItem', $id);
            $item->populateParent();
        }
        $item->menu_id = $menu->id;
        $ajax = false;
        if (isset($_POST['MenuItem'])) {
            $item->setAttributes($_POST['MenuItem']);
            if ($item->validate()) {
                $response = array('success' => false, 'target' => '#' . $menu->getGridId());
                $node = $this->loadModel('MenuItem', $item->parent_id);
                if ($item->getIsNewRecord()) {
                    if ($item->appendTo($node)) {
                        Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('common', 'Inserted successfully.'));
                        $response['success'] = true;
                    }
                } else {
                    if ($node->equals($item->parent()->find())) {
                        $item->saveNode(false);
                    } else {
                        $item->moveAsLast($node);
                    }
                    Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('common', 'Updated successfully.'));
                    $response['success'] = true;
                }
                if ($response['success']) {
                    $response['html'] = $this->renderPartial('_list', array(
                        'menu' => $menu, 'provider' => $this->_getProvider($menu->getTree())
                    ), true);
                    
                    $this->renderJson($response);
                }
                
            } else {
                $ajax = true;
            }
        }
        $params = array(
            'item' => $item,
            'parents' => $menu->listData() 
        );
        if ($ajax) {
            $this->renderJson(array(
                'success' => false,
                'target' => '#' . $item->getFormId(),
                'html' => $this->renderPartial('_form', $params, true)
            ));
        } else {
            $this->render('_form', $params);            
        }
    }
    
    public function actionMove($id, $dir)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            $current = $this->loadModel('MenuItem', $id);
            if ($dir == $current::MOVE_UP) {           
                $prev = $current->prev()->find();
                if ($prev) {
                    $current->moveBefore($prev);
                }
            } elseif ($dir == $current::MOVE_DOWN) {
                $next = $current->next()->find();
                if ($next) {
                    $current->moveAfter($next);
                }
            }
            $menu = $current->menu;        
            $this->renderJson(array(
                'html' => $this->renderPartial('_list', array(
                    'menu' => $menu, 'provider' => $this->_getProvider($menu->getTree())
                ), true),
                'success' => true,
                'target' => '#' . $menu->getGridId()
            ));
        } else {
            throw new CHttpException(400, Yii::t('backend', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    
    public function actionDelete($id)
    {
        $this->loadModel('MenuItem', $id)->deleteNode();
        echo true;        
    }
    
    protected function _getProvider($data)
    {
        return new CArrayDataProvider($data, array(
            'pagination' => array('pageSize' => 1000)
        ));
    }
}