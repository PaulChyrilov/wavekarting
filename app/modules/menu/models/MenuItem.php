<?php

class MenuItem extends I18nActiveRecord 
{
    public $parent_id;
    public $visible_condition;
    public $i18nModel = 'MenuItemI18n';


    public static function model($className = __CLASS__) 
    {
        return parent::model($className);
    }

    public function tableName() 
    {
        return 'menu_items';
    }

    public function rules() 
    {
        return array(
            array('url, parent_id', 'required'),
            array('url', 'length', 'max' => 255),
            array('translateattrs, visible_condition', 'safe'),
            array('id, url', 'safe', 'on' => 'search'),
        );
    }
    
    public function behaviors()
    {
        return array(
            'nestedSetBehavior' => array(
                'class' => 'ext.behaviors.NestedSetBehavior',
                'hasManyRoots' => true,
                'rootAttribute' => 'root',
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'levelAttribute' => 'level',
            ),
            'CTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created_at',
                'updateAttribute'   => null
            )
        );
    }
    
    public function relations()
    {
        return array(
            'menu' => array(self::BELONGS_TO, 'Menu', 'menu_id'),
            'i18ns' => array(self::HAS_MANY, 'MenuItemI18n', 'parent_id', 'index' => 'lang_id')
        );
    }

    public function attributeLabels() 
    {
        return array(
            'id' => Yii::t('menu.backend', 'ID'),
            'url' => Yii::t('menu.backend', 'Url'),
            'parent_id' => Yii::t('menu.backend', 'Parent'),
        );
    }
    
    public function setTranslateAttrs($i18ns)
    {
        $this->setNestedRelation('i18ns', $i18ns);
        $this->clearDirtyAttributes($this->i18ns);
    }

    public function search() 
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('url', $this->url, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public function populateParent()
    {
        $this->parent_id = $this->parent()->find()->id;
    }
}