<?php

class m130531_122156_add_conditon extends CDbMigration
{

    public function safeUp()
    {
        $sql = <<< EOD
ALTER TABLE `menu_items`
ADD COLUMN `visible_condition`  text NULL AFTER `level`;
EOD;
        $this->execute($sql);
    }

    public function down()
    {
        
    }
}