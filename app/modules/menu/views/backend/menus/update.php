<?php
/* @var $this MenusController */
/* @var $model Menu */

$this->breadcrumbs = array(
    'Menus' => array('admin'),
    'Update',
);
?>

<h1>Update Menu <?= CHtml::encode('"' . $model->name . '"'); ?></h1>

<?= $this->renderPartial('_form', array('model' => $model)); ?>

<?php $this->widget('bootstrap.widgets.TbNavbar', array(
    'brand' => 'Title',
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbMenu',
            'items' => Yii::app()->getModule('menu')->getByCode($model->code)
        )
    )
));?>
