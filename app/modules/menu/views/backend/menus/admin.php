<?php
/* @var $this MenusController */
/* @var $model Menu */

$this->breadcrumbs = array(
	'Menus' => array('admin'),
	'Manage',
);
?>

<h1>Manage Menus</h1>

<?= TbHtml::link(Yii::t('menu.backend', 'Create a new menu'), array('create'), array('class' => 'btn')); ?>
<br />

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'template' => "{items}\n{pager}",
    'id' => $model->getGridId(),
    'columns' => array(
        array(
            'class'=>'bootstrap.widgets.TbRelationalColumn',
            'name' => 'code',
            'url' => $this->createUrl('/menu/items/index'),
            'afterAjaxUpdate' => 'js:function(tr, rowid, data) {}',
            'sortable' => false,
        ),
        array('name' => 'name', 'sortable' => false),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update} {delete}',
        )
    ),
)); ?>
