<?php
/* @var $this MenusController */
/* @var $model Menu */

$this->breadcrumbs=array(
    'Menus'=>array('admin'),
    'Create',
);
?>

<h1>Create Menu</h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>