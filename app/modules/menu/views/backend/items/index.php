<?= TbHtml::link(
    Yii::t('menu.backend', 'Create a new item'), 
    array('/menu/items/edit', 'menuId' => $menu->id), 
    array('class' => 'btn btn-success', 'data-op' => 'modal', 'data-title' => 'Edit', 'data-skip' => 1)
); ?>
<br />
<?php $this->renderPartial('_list', compact('menu', 'provider')) ?>