<?php

class Location extends I18nActiveRecord
{
    public $created_at;
    public $updated_at;
    public $slug;
    public $order;
    public $searchTitle;
    public $skype;
    public $ember_code;
    
    
    public $i18nModel = 'LocationI18n';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'locations';
    }

    public function rules()
    {
        return array(
            array('embed_code', 'required'),
            array('slug', 'unique'),
            array('searchTitle', 'safe', 'on' => 'search'),
            array('translateattrs, slug, skype', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'               => 'ID',
            'slug'             => 'Slug',
            'skype'            => 'Skype',
            'embed_code'       => 'Map code',
            'created_at'       => 'Create date',
            'updated_at'       => 'Update date',            
        );
    }
    
    public function relations()
    {
        return array(
            'i18ns' => array(self::HAS_MANY, 'LocationI18n', 'parent_id', 'index' => 'lang_id')
        );
    }
    
    public function scopes()
    {
        $a = $this->getTableAlias(false, false);
        return array(
            'ordered' => array('order' => "$a.`order` ASC"),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        
        $criteria->select = 't.*';
        $criteria->join = 'INNER JOIN location_i18ns ON location_i18ns.parent_id = t.id';
        $criteria->group = 't.id';
        $criteria->compare('location_i18ns.title', $this->searchTitle, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->scopes = array('ordered');
        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'           => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'setUpdateOnCreate' => true
            ),
            'slugable' => array(
                'class' => 'ext.behaviors.SlugBehavior',
                'scenarios' => array('insert'),
                'sourceAttribute' => function($owner) {
                    return $owner->getI18n('title', false, Yii::app()->getModule('languages')->getDefault());
                }
            )
        );
    }
    
    public function setTranslateAttrs($i18ns)
    {
        $this->setNestedRelation('i18ns', $i18ns);
        $this->clearDirtyAttributes($this->i18ns);
    }
    
    public function getUrl()
    {
        return Yii::app()->createUrl('/locations/locations/show', array(
            'slug' => $this->slug,
        ));
    }
    
    public static function getBySlug($slug)
    {
        return self::model()->findByAttributes(array('slug' => $slug));
    }
    
    protected function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->order = $this->getMaxOrder() + 1;
        }
        return parent::beforeSave();
    }
}