<?php

class LocationI18n extends EActiveRecord
{
    public $title;
    public $content;
    
    public $dirtyAttributes = array('title', 'content');

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'location_i18ns';
    }

    public function rules()
    {
        return array(
            array('title, content, lang_id', 'required'),
            array('title', 'length', 'max' => 255),
        );
    }

    public function attributeLabels()
    {
        return array(
            'title'   => 'Title',
            'content' => 'Description',
        );
    }
}