<div class="col4">
    <article class="offset20">
        <h2><?= $data->getI18n('title'); ?></h2>
        <p><?= $data->getI18n('content'); ?></p>
        <div class="skype-call-us">
            <div class="icon"></div>
            <p><a href="skype:<?= $data->skype?>?call"><?= Yii::t('locations', 'Call us') ?></a></p>
        </div>
        <div class="map">
            <?= $data->embed_code ?>
        </div>
    </article>
</div>