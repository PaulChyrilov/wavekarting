<section class="body-page">
    <header>
        <h2><?= Yii::t('locations', 'Locations') ?></h2>
    </header>
    <section class="content-block backgrounded">
        <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'id' => 'location-list',
                'itemView' => '_view',
                'template' => "{items}\n{pager}",
                'itemsCssClass' => 'contacts'
            ));
        ?>
    </section>

</section>

<?php 
$this->setPageTitle(Yii::t('locations', 'Locations'));
Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl.'/css/pages/contacts.css');
?>