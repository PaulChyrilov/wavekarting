<?php
$this->pageTitle= 'Manage locations';

$this->breadcrumbs=array(
	'Locations',
);
?>

<h1>Manage locations</h1>

<?= TbHtml::link(Yii::t('locations.backend', 'Create a new location'), array('create'), array('class' => 'btn')); ?>
<br />

<?php
$this->renderPartial('_list', compact('model')); 
?>