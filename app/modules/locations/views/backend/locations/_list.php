<?php
$dataProvider = $model->search();
$maxOrder = $model->getMaxOrder();
$this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => $model->getGridId(false),
    'dataProvider' => $dataProvider,
    'ajaxUrl' => array('/locations/locations/admin'),
    'filter'       => $model,
    'template'     => "{items}\n{pager}",
    'columns'      => array(
        array(
            'header'   => 'Title',
            'type'     => 'raw',
            'name'     => 'searchTitle',
            'value'    => 'CHtml::encode($data->getI18n("title"))',
            'sortable' => false,
        ),
        array(
            'name' => 'skype',
            'sortable' => false,
        ),
        array(
            'class'              => 'bootstrap.widgets.TbButtonColumn',
            'deleteConfirmation' => 'Are you sure to delete this location?',
            'template' => '{up} {down} {update} {delete}',
            'buttons' => array(
                'up' => array(
                    'url' => 'Yii::app()->createUrl("/locations/locations/move", array("id" => $data->id, "dir" => "up"))',
                    'visible' => '$data->order != 1',
                    'label' => Yii::t('locations.backend', 'Up'),
                    'options' => array('data-op' => 'ajax'),
                    'icon' => 'chevron-up'
                ),
                'down' => array(
                    'url' => 'Yii::app()->createUrl("/locations/locations/move", array("id" => $data->id, "dir" => "down"))',
                    'visible' => '$data->order != ' . $maxOrder,
                    'label' => Yii::t('locations.backend', 'Down'),
                    'options' => array('data-op' => 'ajax'),
                    'icon' => 'chevron-down'
                ),
            )
        ),
    ),
));