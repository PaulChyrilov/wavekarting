<?php $prefix = 'Location[translateattrs][' . $lang['id'] . ']'; ?>
<?= CHtml::hiddenField($prefix . '[lang_id]', $lang['id'])?>
<?= $form->textFieldRow($model, 'title', array('class' => 'span8', 'id' => "title-{$lang['id']}", 'name' => $prefix . '[title]')); ?>        
<?= $form->textAreaRow($model, 'content', array('class' => 'span8', 'rows' => 5, 'id' => "content-{$lang['id']}", 'name' => $prefix . '[content]')); ?>
