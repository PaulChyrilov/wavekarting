<?php
$this->pageTitle = 'Update location';
$this->breadcrumbs=array(
	'Manage locations' => array('admin'),
	'Update location',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->getI18n('title')) . '"' ?> location</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>