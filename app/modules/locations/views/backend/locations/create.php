<?php
$this->pageTitle = 'Create location';
$this->breadcrumbs=array(
	'Manage locations' => array('admin'),
	'Create location',
);

$this->renderPartial('_form', array('model' => $model)); 
?>