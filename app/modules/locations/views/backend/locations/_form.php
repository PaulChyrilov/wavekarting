<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
?>
<fieldset>    
    <?php $this->renderPartial('_i18ns', array('model' => $model, 'form' => $form)) ?>       
    <?= $form->textAreaRow($model, 'embed_code', array('class' => 'span8', 'rows' => 5)); ?>        
    <?= $form->textFieldRow($model, 'skype', array('class' => 'span8')); ?>        
    
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(Yii::t('locations.backend', $model->getIsNewRecord() ? 'Create' : 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
))?>


<?php $this->endWidget(); ?>
