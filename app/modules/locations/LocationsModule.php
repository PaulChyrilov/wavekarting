<?php

class LocationsModule extends EWebModule 
{
    public $defaultController = 'locations';
    
    public function init()
    {
        parent::init();

        $this->setImport(array(
            'locations.models.*',
        ));
    }
}
