<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MemberWidget
 *
 * @author mlapko
 */
class LocationWidget extends CWidget
{
    public $limit = 10;
    
    public function run()
    {
        Yii::app()->getModule('locations');
        $this->render('locations', array('locations' => Location::model()->withLang()->findAll(array(
            'limit' => $this->limit
        ))));
    }
    
}
