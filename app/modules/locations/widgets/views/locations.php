<div id="locations" class="contacts wrapper swiper-container">  
    <div class="swiper-wrapper">
        <?php $lCount = count($locations); ?>)
        <?php foreach ($locations as $i => $data) { ?>
        <div class="col4">
            <article class="offset20<?= $i + 1 == $lCount? ' last' : ''?> <?= $i + 1 >= 4 ? ' next-loc' : ''?>">
                    <h2><?= $data->getI18n('title'); ?></h2>
                    <p><?= nl2br($data->getI18n('content')); ?></p>
                    <div class="botom-information">
                        <div class="skype-call-us">
                            <div class="icon"></div>
                            <p><a href="skype:<?= $data->skype?>?call"><?= Yii::t('locations', 'Call us') ?></a></p>
                        </div>
                        <div class="map">
                            <?= $data->embed_code ?>
                        </div>
                    </div>
                </article>
            </div>        
        <?php } ?>
    </div>
    <div class="swiper-scrollbar"></div>
</div>
<?php 
Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl.'/css/pages/contacts.css')->registerScript('locations', '
    var $locations = $("#locations"),
        $article = $locations.find("article"),
        maxHeight = 0;
    $article.each(function(){
      if ( $(this).height() > maxHeight ) 
      {
        maxHeight = $(this).height();
      }
    });
    $article.height(maxHeight);
    $article.find(".botom-information").css("position","absolute");
    $locations.find(".swiper-wrapper").height(maxHeight);
    
');
?>
<?php $this->widget('application.modules.gallery.widgets.SwiperWidget', array(
    'id' => 'locations',
    'options' => array(
        'createPagination' => false,
        'autoPlay' => 15000,
        'slideClass' => 'col4',
        'slidesPerSlide' => 4,
        'scrollbar' => array('hide' => false, 'container' => '#locations .swiper-scrollbar')
    ),
)); ?>

