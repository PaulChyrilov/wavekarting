<?php
$this->pageTitle = 'Create solution';
$this->breadcrumbs=array(
	'Manage solutions' => array('admin'),
	'Create solution',
);

$this->renderPartial('_form', array('model' => $model)); 
?>