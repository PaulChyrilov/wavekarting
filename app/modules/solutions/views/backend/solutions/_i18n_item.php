<?php 
$prefix = 'Solution[translateattrs][' . $lang['id'] . ']'; 
$redactor = HApplication::getEditor(array('convertDivs' => false));
?>
<?= CHtml::hiddenField($prefix . '[lang_id]', $lang['id'])?>
<?= $form->textFieldRow($model, 'title', array('class' => 'span8', 'id' => "title-{$lang['id']}", 'name' => $prefix . '[title]')); ?>        
<?= $form->$redactor['method']($model, 'content', array(
    'id' => "content-{$lang['id']}", 
    'name' => $prefix . '[content]',
    'options' => $redactor['options']
)); ?>
<?= $form->textAreaRow($model, 'short_content', array('class' => 'span8', 'rows' => 5, 'id' => "meta_keywords-{$lang['id']}", 'name' => $prefix . '[short_content]')); ?>
<?= $form->textFieldRow($model, 'meta_title', array('class' => 'span8', 'id' => "meta_title-{$lang['id']}", 'name' => $prefix . '[meta_title]')); ?>        
<?= $form->textAreaRow($model, 'meta_keywords', array('class' => 'span8', 'rows' => 5, 'id' => "meta_keywords-{$lang['id']}", 'name' => $prefix . '[meta_keywords]')); ?>
<?= $form->textAreaRow($model, 'meta_description', array('class' => 'span8', 'rows' => 5, 'id' => "meta_description-{$lang['id']}", 'name' => $prefix . '[meta_description]')); ?>