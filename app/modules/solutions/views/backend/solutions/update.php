<?php
$this->pageTitle = 'Update solution';
$this->breadcrumbs=array(
	'Manage solutions' => array('admin'),
	'Update solution',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->getI18n('title')) . '"' ?> solution</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>