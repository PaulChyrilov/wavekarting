<?php
$this->pageTitle= 'Manage solutions';

$this->breadcrumbs=array(
	'Solutions',
);
?>

<h1>Manage solutions</h1>

<?= TbHtml::link(Yii::t('solutions.backend', 'Create a new solution'), array('create'), array('class' => 'btn')); ?>
<br />

<?php
$this->renderPartial('_list', compact('model')); 
?>