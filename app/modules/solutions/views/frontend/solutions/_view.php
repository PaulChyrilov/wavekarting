<div class="col4">
    <article class="offset30">
        <figure>
            <?= CHtml::image($data->getImageUrl('image', 'solution_preview'), $data->getI18n('title'), array('width' => 188, 'height' => 117))?>
        </figure>
        <h2><?= $data->getI18n('title'); ?></h2>
        <p><?= $data->getI18n('short_content'); ?></p>
        <?= CHtml::link(Yii::t('common', 'read more &raquo;'), $data->getUrl(), array('class' => 'button'))?>
    </article>
</div>