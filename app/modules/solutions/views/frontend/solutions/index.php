<section class="body-page">
    <header>
        <h2><?= Yii::t('solutions', 'Solutions') ?></h2>
    </header>

    <section class="content-block">
        <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'id' => 'solution-list',
                'itemView' => '_view',
                'template' => "{items}\n{pager}",
                'itemsCssClass' => 'solution'
            ));
        ?>
    </section>

</section>

<?php 
$this->setPageTitle(Yii::t('solutions', 'Solutions'));
Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/css/pages/solution.css')->registerScript('solutions', '
    var $solutions = $("#solution-list"),
        $articles = $solutions.find("article");
    $articles.height($solutions.height() - 60);
');
?>