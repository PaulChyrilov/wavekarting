<?php

class SolutionsModule extends EWebModule 
{
    public $defaultController = 'solutions';
    
    public function init()
    {
        parent::init();
        Yii::app()->setComponents(array(
            'image' => array(
                'presets' => array(
                    'solution_preview' => array(
                        'adaptiveThumb' => array('width' => 188, 'height' => 117)
                    ),
                )
            )
        ));

        $this->setImport(array(
            'solutions.models.*',
        ));
    }
}
