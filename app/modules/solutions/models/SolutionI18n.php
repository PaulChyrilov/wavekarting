<?php

class SolutionI18n extends EActiveRecord
{
    public $title;
    public $content;
    public $short_content;
    public $meta_title;
    public $meta_keywords;
    public $meta_description;
    
    public $dirtyAttributes = array('title', 'content', 'short_content', 'meta_title', 'meta_keywords', 'meta_description');

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'solution_i18ns';
    }

    public function rules()
    {
        return array(
            array('title, content, short_content, lang_id', 'required'),
            array('title', 'length', 'max' => 255),
            array('meta_title, meta_keywords, meta_description', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'title'   => 'Title',
            'content' => 'Content',
            'short_content' => 'Short Content',
            'meta_title'       => 'Title (SEO)',
            'meta_keywords'    => 'Keywords (SEO)',
            'meta_description' => 'Description (SEO)',
        );
    }
}