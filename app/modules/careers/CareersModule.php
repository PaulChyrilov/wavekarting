<?php

class CareersModule extends EWebModule 
{
    public $defaultController = 'careers';
    
    public function init()
    {
        parent::init();
        Yii::app()->setComponents(array(
            'image' => array(
                'presets' => array(
                    'career_preview' => array(
                        'adaptiveThumb' => array('width' => 188, 'height' => 117)
                    ),
                )
            )
        ));

        $this->setImport(array(
            'careers.models.*',
        ));
    }
}
