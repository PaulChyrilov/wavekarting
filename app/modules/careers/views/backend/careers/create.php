<?php
$this->pageTitle = 'Create career';
$this->breadcrumbs=array(
	'Manage careers' => array('admin'),
	'Create career',
);

$this->renderPartial('_form', array('model' => $model)); 
?>