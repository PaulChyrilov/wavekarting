<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
?>
<fieldset>    
    <?php $this->renderPartial('_i18ns', array('model' => $model, 'form' => $form)) ?>       
    <?= $form->textFieldRow($model, 'slug', array('class' => 'span8')); ?>        
    <?= $form->fileFieldRow($model, 'image', array('hint' => 'image size: 188x117 <br/>' . ($model->image ? CHtml::image($model->getImageUrl('image', 'image_preview'), '') : ''))); ?>
    
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(Yii::t('careers.backend', $model->getIsNewRecord() ? 'Create' : 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
))?>


<?php $this->endWidget(); ?>
