<?php
$this->pageTitle = 'Update career';
$this->breadcrumbs=array(
	'Manage careers' => array('admin'),
	'Update career',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->getI18n('title')) . '"' ?> career</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>