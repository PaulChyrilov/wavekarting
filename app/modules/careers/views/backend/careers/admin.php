<?php
$this->pageTitle= 'Manage careers';

$this->breadcrumbs=array(
	'Careers',
);
?>

<h1>Manage careers</h1>

<?= TbHtml::link(Yii::t('careers.backend', 'Create new career'), array('create'), array('class' => 'btn')); ?>
<br />

<?php
$this->renderPartial('_list', compact('model')); 
?>