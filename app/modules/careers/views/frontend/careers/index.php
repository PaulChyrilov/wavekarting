<section class="body-page">
    <header>
        <h2><?= Yii::t('careers', 'Careers') ?></h2>
    </header>

    <section class="content-block backgrounded">
        <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'id' => 'career-list',
                'itemView' => '_view',
                'template' => "{items}\n{pager}",
                'itemsCssClass' => 'careers'
            ));
        ?>
        <div class="common">
            <hr />
            <?php $this->widget('ext.widgets.contact.ContactUsWidget') ?>
        </div>
    </section>

</section>

<?php
$this->setPageTitle(Yii::t('careers', 'Careers'));
Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/css/pages/careers.css')
        ->registerCssFile(Yii::app()->baseUrl . '/css/pages/common.css')
        ->registerScript('careers', '
    var $careers = $("#career-list"),
        $articles = $careers.find(".career-item");
    $articles.height($careers.height() - 15);
');
?>