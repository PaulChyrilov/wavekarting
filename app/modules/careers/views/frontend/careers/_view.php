<div class="col3">
    <article class="offset30<?= (($index + 1) % 3) === 0 ? ' last' : ''?>">
        <div class="career-item">            
            <figure>
                <?= $data->image ? CHtml::image($data->getImageUrl('image', 'career_preview'), $data->getI18n('title'), array('height' => 117)) : ''?>
            </figure>
            <h2><?= $data->getI18n('title'); ?></h2>
            <p><?= $data->getI18n('short_content'); ?></p>
        </div>
        <?= CHtml::link(Yii::t('common', 'read more &raquo;'), $data->getUrl(), array('class' => 'button'))?>
    </article>
</div>