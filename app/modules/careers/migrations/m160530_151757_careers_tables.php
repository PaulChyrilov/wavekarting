<?php

class m160530_151757_careers_tables extends CDbMigration
{

    public function up()
    {
        $sql = <<< EOD
DROP TABLE IF EXISTS `careers`;
CREATE TABLE IF NOT EXISTS `careers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1', 
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_idx` (`order`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
            
DROP TABLE IF EXISTS `career_i18ns`;
CREATE TABLE IF NOT EXISTS `career_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `short_content` text NOT NULL,
  `meta_title` varchar(255) DEFAULT '',
  `meta_keywords` text DEFAULT '',
  `meta_description` text DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `parent_id_idx` (`parent_id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`) 
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
EOD;
        $this->execute($sql);
    }

    public function down()
    {
        
    }
}