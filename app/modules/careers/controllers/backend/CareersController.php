<?php

class CareersController extends BackendController
{
    public $modelName = 'Career';
    public $redirectTo = array('admin');
    public $defaultAction = 'admin';
    
    public function actionMove($id, $dir)
    {
        $model = parent::actionMove($id, $dir);
        $this->renderJson(array(
            'html' => $this->renderPartial('_list', array('model' => new Career('search')), true),
            'success' => true,
            'target' => '#' . $model->getGridId(false)
        ));
    }
    
}