<?php

class FaqsController extends FrontendController
{
    public $modelName = 'Faq';
    
    public function actionIndex()
    {        
        $this->render('index', array(
            'models' => Faq::model()->ordered()->with('i18ns')->findAll()
        ));
    }
}