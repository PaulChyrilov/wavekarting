<?php

class Faq extends I18nActiveRecord
{
    public $created_at;
    public $updated_at;
    public $order;
    public $searchQuestion;
    
    
    public $i18nModel = 'FaqI18n';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'faqs';
    }

    public function rules()
    {
        return array(
            array('searchQuestion', 'safe', 'on' => 'search'),
            array('translateattrs', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'         => 'ID',
            'created_at' => 'Create date',
            'updated_at' => 'Update date',            
        );
    }
    
    public function relations()
    {
        return array(
            'i18ns' => array(self::HAS_MANY, 'FaqI18n', 'parent_id', 'index' => 'lang_id')
        );
    }
    
    public function scopes()
    {
        $a = $this->getTableAlias(false, false);
        return array(
            'ordered' => array('order' => "$a.`order` ASC"),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        
        $criteria->select = 't.*';
        $criteria->join = 'INNER JOIN faq_i18ns ON faq_i18ns.parent_id = t.id';
        $criteria->group = 't.id';
        $criteria->compare('faq_i18ns.question', $this->searchQuestion, true);
        $criteria->scopes = array('ordered');
        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'           => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'setUpdateOnCreate' => true
            ),
        );
    }
    
    public function setTranslateAttrs($i18ns)
    {
        $this->setNestedRelation('i18ns', $i18ns);
        $this->clearDirtyAttributes($this->i18ns);
    }
    
    protected function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->order = $this->getMaxOrder() + 1;
        }
        return parent::beforeSave();
    }
}