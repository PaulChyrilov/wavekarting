<?php

class FaqI18n extends EActiveRecord
{
    public $question;
    public $answer;
    
    public $dirtyAttributes = array('question', 'answer');

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'faq_i18ns';
    }

    public function rules()
    {
        return array(
            array('question, answer, lang_id', 'required'),
            array('question', 'length', 'max' => 255),
        );
    }

    public function attributeLabels()
    {
        return array(
            'question' => 'Question',
            'answer' => 'Answer',
        );
    }
}