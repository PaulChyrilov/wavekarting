<?php
$dataProvider = $model->search();
$maxOrder = $model->getMaxOrder();
$this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => $model->getGridId(false),
    'dataProvider' => $dataProvider,
    'ajaxUrl' => array('/faq/faqs/admin'),
    'filter'       => $model,
    'template'     => "{items}\n{pager}",
    'columns'      => array(
        array(
            'header'   => 'Question',
            'type'     => 'raw',
            'name'     => 'searchQuestion',
            'value'    => 'CHtml::encode($data->getI18n("question"))',
            'sortable' => false,
        ),
        array(
            'class'              => 'bootstrap.widgets.TbButtonColumn',
            'deleteConfirmation' => 'Are you sure to delete this faq?',
            'template' => '{up} {down} {update} {delete}',
            'buttons' => array(
                'up' => array(
                    'url' => 'Yii::app()->createUrl("/faq/faqs/move", array("id" => $data->id, "dir" => "up"))',
                    'visible' => '$data->order != 1',
                    'label' => Yii::t('faq.backend', 'Up'),
                    'options' => array('data-op' => 'ajax'),
                    'icon' => 'chevron-up'
                ),
                'down' => array(
                    'url' => 'Yii::app()->createUrl("/faq/faqs/move", array("id" => $data->id, "dir" => "down"))',
                    'visible' => '$data->order != ' . $maxOrder,
                    'label' => Yii::t('faqs.backend', 'Down'),
                    'options' => array('data-op' => 'ajax'),
                    'icon' => 'chevron-down'
                ),
            )
        ),
    ),
));