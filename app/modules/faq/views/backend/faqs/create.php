<?php
$this->pageTitle = 'Create a FAQ';
$this->breadcrumbs=array(
	'Manage FAQs' => array('admin'),
	'Create a FAQ',
);

$this->renderPartial('_form', array('model' => $model)); 
?>