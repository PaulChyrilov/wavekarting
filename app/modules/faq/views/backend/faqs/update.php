<?php
$this->pageTitle = 'Update a FAQ';
$this->breadcrumbs=array(
	'Manage FAQs' => array('admin'),
	'Update a FAQ',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->getI18n('question')) . '"' ?> faq</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>