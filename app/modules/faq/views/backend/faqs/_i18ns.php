<?php
$tabs = array();
$module = Yii::app()->getModule('languages');
$defaultLang = $module->getDefault();

$i18ns = $model->i18ns;
foreach ($module->listing() as $lang) {
    $i18n = isset($i18ns[$lang['id']]) ? $i18ns[$lang['id']] : new FaqI18n;
    
    $tabs[] = array(
        'label' => $i18n->hasErrors() ? ('<span style="color:red;">' . $lang['name'] . '</span>') : $lang['name'],
        'active' => $lang['id'] == $defaultLang, 
        'content' => $this->renderPartial('_i18n_item', array('model' => $i18n, 'lang' => $lang, 'form' => $form), true)
    );
}
$this->widget('bootstrap.widgets.TbTabs', array(
    'type' => 'tabs', // 'tabs' or 'pills'
    'tabs' => $tabs,
    'encodeLabel' => false,
    'htmlOptions' => array('id' => 'tab-faq-' . $model->id)
));
?>
