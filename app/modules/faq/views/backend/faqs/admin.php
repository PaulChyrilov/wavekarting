<?php
$this->pageTitle= 'Manage FAQ';

$this->breadcrumbs=array(
	'FAQ',
);
?>

<h1>Manage locations</h1>

<?= TbHtml::link(Yii::t('faq.backend', 'Create a new faq'), array('create'), array('class' => 'btn')); ?>
<br />

<?php
$this->renderPartial('_list', compact('model')); 
?>