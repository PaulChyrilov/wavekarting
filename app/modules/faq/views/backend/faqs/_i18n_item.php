<?php 
$prefix = 'Faq[translateattrs][' . $lang['id'] . ']'; 
$redactor = HApplication::getEditor();
?>

<?= CHtml::hiddenField($prefix . '[lang_id]', $lang['id'])?>
<?= $form->textAreaRow($model, 'question', array('class' => 'span8', 'id' => "question-{$lang['id']}", 'name' => $prefix . '[question]')); ?>        
<?= $form->$redactor['method']($model, 'answer', array(
    'id' => "answer-{$lang['id']}", 
    'name' => $prefix . '[answer]',
    'options' => $redactor['options']
)); ?>
