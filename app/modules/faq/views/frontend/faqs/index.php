<?php $low = round(count($models) / 2); ?>
<section class="body-page">
    <header>
        <h2>FAQ</h2>
    </header>

    <section class="content-block backgrounded">
        <div class="about-us flashable">
            <div class="col2">
                <div class="offset30 line">
                    <?php for ($i = 0; $i < $low; ++$i) { ?>
                    <div class="faq-entry">
                        <div class="question"><?= CHtml::encode($models[$i]->getI18n('question'))?></div>
                        <div class="answer"><?= $models[$i]->getI18n('answer') ?></div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col2">
                <div class="offset30">
                    <?php for ($i = $low; $i < count($models); ++$i) { ?>
                    <div class="faq-entry">
                        <div class="question"><?= CHtml::encode($models[$i]->getI18n('question'))?></div>
                        <div class="answer"><?= $models[$i]->getI18n('answer') ?></div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </section>

</section>

<?php Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/css/pages/about.css') ?>
<script>
    $(function() {
        $('.question').on('click', function() {
            $(this).toggleClass('show').next().fadeToggle();
        })
    });
</script>