<?php

class FaqModule extends EWebModule 
{
    public $defaultController = 'faqs';
    
    public function init()
    {
        parent::init();

        $this->setImport(array(
            'faq.models.*',
        ));
    }
}
