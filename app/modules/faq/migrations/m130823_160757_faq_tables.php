<?php

class m130823_160757_faq_tables extends CDbMigration
{

    public function safeUp()
    {
        $sql = <<< EOD
DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` int(11) NOT NULL DEFAULT '1', 
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_idx` (`order`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
            
DROP TABLE IF EXISTS `faq_i18ns`;
CREATE TABLE IF NOT EXISTS `faq_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id_idx` (`parent_id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`) 
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
EOD;
        $this->execute($sql);
    }

    public function down()
    {
        
    }
}