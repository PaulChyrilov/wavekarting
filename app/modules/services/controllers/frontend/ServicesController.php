<?php

class ServicesController extends FrontendController
{
    public $modelName = 'Service';
    
    public function actionIndex()
    {
        $this->redirect(Yii::app()->homeUrl);
    }
    
    public function actionCategory($code)
    {
        $category = Yii::app()->getModule('categories')->getByCode($code, true);
        
        $dataProvider = new CActiveDataProvider(Service::model()->ordered()->withLang()->byCategory($code));
        $this->render('index', array('dataProvider' => $dataProvider, 'category' => $category));
    }

}