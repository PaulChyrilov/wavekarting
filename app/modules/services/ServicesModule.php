<?php

class ServicesModule extends EWebModule 
{
    public $defaultController = 'services';
    
    public function init()
    {
        parent::init();
        Yii::app()->setComponents(array(
            'image' => array(
                'presets' => array(
                    'service_preview' => array(
                        'adaptiveThumb' => array('width' => 170, 'height' => 180)
                    ),
                )
            )
        ));

        $this->setImport(array(
            'services.models.*',
        ));
    }
}
