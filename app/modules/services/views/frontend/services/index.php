<section class="body-page">
    <header>
        <h2><?= Yii::t('services', 'Services') ?></h2>
        <ul>
        <?php foreach (Yii::app()->getModule('categories')->getListCategory('service_category', false) as $cat) : ?>
            <li<?php if ($cat->code == $category->code) {?> class="active"<?php } ?>><?= CHtml::link(CHtml::encode($cat->getI18n('name')), array('/services/services/category', 'code' => $cat->code))?></li>
        <?php endforeach; ?>         
        </ul>
    </header>

    <section class="content-block">
        <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'id' => 'service-list',
                'itemView' => '_view',
                'template' => "{items}\n{pager}",
                'itemsCssClass' => 'services'
            ));
        ?>
    </section>

</section>

<?php 
$this->setPageTitle(Yii::t('services', 'Services'));
Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/css/pages/services.css') 
?>