<div class="col2">
    <article>
        <figure>
            <?= CHtml::image($data->getImageUrl('image', 'service_preview'), $data->getI18n('title'))?>
        </figure>
        <h2><?= $data->getI18n('title'); ?></h2>
        <p>
            <?= $data->getI18n('short_content'); ?>
        </p>
        <?= CHtml::link(Yii::t('common', 'read more &raquo;'), $data->getUrl(), array('class' => 'button pull-right offset-bottom-right-20'))?>
        <div class="clear"></div>
    </article>
</div>