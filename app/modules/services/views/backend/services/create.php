<?php
$this->pageTitle = 'Create service';
$this->breadcrumbs=array(
	'Manage services' => array('admin'),
	'Create service',
);

$this->renderPartial('_form', array('model' => $model)); 
?>