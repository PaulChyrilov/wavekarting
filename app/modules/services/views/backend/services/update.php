<?php
$this->pageTitle = 'Update service';
$this->breadcrumbs=array(
	'Manage services' => array('admin'),
	'Update service',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->getI18n('title')) . '"' ?> service</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>