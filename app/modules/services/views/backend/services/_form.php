<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
?>
<fieldset>    
    <?php $this->renderPartial('_i18ns', array('model' => $model, 'form' => $form)) ?>       
    <?= $form->textFieldRow($model, 'slug', array('class' => 'span8')); ?>        
    <?= $form->dropDownListRow(
        $model, 
        'category_id', 
        Yii::app()->getModule('categories')->getListData('service_category'), 
        array('encode' => false, 'prompt' => 'Select category')
    ); ?>
    <?= $form->fileFieldRow($model, 'image', array('hint' => 'image size: 170x180 <br/>' . ($model->image ? CHtml::image($model->getImageUrl('image', 'image_preview'), '') : ''))); ?>
    
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(t('pages.backend', $model->getIsNewRecord() ? 'Create' : 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
))?>


<?php $this->endWidget(); ?>
