<?php
$this->pageTitle= 'Manage services';

$this->breadcrumbs=array(
	'Services',
);
?>

<h1>Manage services</h1>

<?= TbHtml::link(Yii::t('services.backend', 'Create a new service'), array('create'), array('class' => 'btn')); ?>
<br />

<?php
$this->renderPartial('_list', compact('model')); 
?>