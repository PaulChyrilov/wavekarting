<?php
$dataProvider = $model->search();
$maxOrder = $model->getMaxOrder();
$this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => $model->getGridId(false),
    'dataProvider' => $dataProvider,
    'ajaxUrl' => array('/services/services/admin'),
    'filter'       => $model,
    'template'     => "{items}\n{pager}",
    'columns'      => array(
        array('header' => 'Image', 'value' => 'CHtml::image($data->getImageUrl("image", "image_preview"))', 'type' => 'raw', 'htmlOptions' => array('style' => 'width:110px;')),
		array(
            'header'   => 'Title',
            'type'     => 'raw',
            'name'     => 'searchTitle',
            'value'    => 'CHtml::encode($data->getI18n("title"))',
            'sortable' => false,
        ),
        array(
            'header' => 'Url',
            'value' => '"/services/{$data->slug}"',
            'sortable' => false,
        ),
        array(
            'class'              => 'bootstrap.widgets.TbButtonColumn',
            'deleteConfirmation' => 'Are you sure to delete this service?',
            'template' => '{up} {down} {update} {delete}',
            'buttons' => array(
                'up' => array(
                    'url' => 'Yii::app()->createUrl("/services/services/move", array("id" => $data->id, "dir" => "up"))',
                    'visible' => '$data->order != 1',
                    'label' => Yii::t('languages.backend', 'Up'),
                    'options' => array('data-op' => 'ajax'),
                    'icon' => 'chevron-up'
                ),
                'down' => array(
                    'url' => 'Yii::app()->createUrl("/services/services/move", array("id" => $data->id, "dir" => "down"))',
                    'visible' => '$data->order != ' . $maxOrder,
                    'label' => Yii::t('languages.backend', 'Down'),
                    'options' => array('data-op' => 'ajax'),
                    'icon' => 'chevron-down'
                ),
            )
        ),
    ),
));