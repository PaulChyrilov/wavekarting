<?php

class Service extends I18nActiveRecord
{
    public $created_at;
    public $updated_at;
    public $slug;
    public $order;
    public $category_id;
    public $searchTitle;
    
    
    public $i18nModel = 'ServiceI18n';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'services';
    }

    public function rules()
    {
        return array(
            array('category_id', 'required'),
            array('slug', 'unique'),
            array('category_id', 'safe'),
            array(
                'image', 'ext.components.image_processor.MImageValidator',
                'types' => array('jpg', 'png', 'jpeg'), 'maxSize' => 5 * 1024 * 1024, 
                'minWidth' => 100, 'minHeight' => 100, 'allowEmpty' => !$this->getIsNewRecord()
            ),
            array('searchTitle', 'safe', 'on' => 'search'),
            array('translateattrs, slug', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'               => 'ID',
            'slug'             => 'Slug',
            'image'            => 'Image',
            'category_id'      => 'Category',
            'created_at'       => 'Create date',
            'updated_at'       => 'Update date',            
        );
    }
    
    public function relations()
    {
        return array(
            'i18ns' => array(self::HAS_MANY, 'ServiceI18n', 'parent_id', 'index' => 'lang_id')
        );
    }
    
    public function scopes()
    {
        $a = $this->getTableAlias(false, false);
        return array(
            'ordered' => array('order' => "$a.`order` ASC"),
        );
    }
    
    public function byCategory($code)
    {
        $criteria = $this->getDbCriteria();
        $criteria->addCondition('`category_id` = :category');
        $criteria->params[':category'] = $code;
        return $this;
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        
        $criteria->select = 't.*';
        $criteria->join = 'INNER JOIN service_i18ns ON service_i18ns.parent_id = t.id';
        $criteria->group = 't.id';
        $criteria->compare('service_i18ns.title', $this->searchTitle, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->scopes = array('ordered');
        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'           => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'setUpdateOnCreate' => true
            ),
            'slugable' => array(
                'class' => 'ext.behaviors.SlugBehavior',
                'scenarios' => array('insert'),
                'sourceAttribute' => function($owner) {
                    return $owner->getI18n('title', false, Yii::app()->getModule('languages')->getDefault());
                }
            ),
            'mImage' => array(
                'class'          => 'ext.components.image_processor.MImageBehavior',
                'imageProcessor' => 'image' // image processor component name 
            )
        );
    }
    
    public function setTranslateAttrs($i18ns)
    {
        $this->setNestedRelation('i18ns', $i18ns);
        $this->clearDirtyAttributes($this->i18ns);
    }
    
    public function getUrl()
    {
        return Yii::app()->createUrl('/services/services/show', array(
            'slug' => $this->slug,
        ));
    }
    
    public function getCategory()
    {
        return Yii::app()->getModule('categories')->getByCode($this->category_id);
    }
    
    public static function getBySlug($slug)
    {
        return self::model()->findByAttributes(array('slug' => $slug));
    }
    
    public static function getServiceIds()
    {
        static $ids;
        if ($ids === null) {
            if (($ids = Yii::app()->getCache()->get('service_ids')) === false) {
                $ids = Yii::app()->getDb()->createCommand('SELECT t.id FROM services AS t')->queryColumn();
                Yii::app()->getCache()->set('service_ids', $ids);
            }
        }
        return $ids;
    }
    
    public function getNext($lang = null)
    {
        $ids = self::getServiceIds();
        if (($i = array_search($this->id, $ids)) !== false && isset($ids[$i + 1])) {
            return self::model()->resetScope()->withLang($lang)->findByPk($ids[$i + 1]);
        }
        return null;
    }
    
    public function getPrev($lang = null)
    {
        $ids = self::getServiceIds();
        if (($i = array_search($this->id, $ids)) !== false && isset($ids[$i - 1])) {
            return self::model()->resetScope()->withLang($lang)->findByPk($ids[$i - 1]);
        }
        return null;
    }
    
    protected function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->order = $this->getMaxOrder() + 1;
        }
        return parent::beforeSave();
    }
    
    protected function afterSave()
    {
        parent::afterSave();
        Yii::app()->getCache()->delete('service_ids');
    }
}