<?php
$this->pageTitle = 'Update news';
$this->breadcrumbs=array(
	'Manage News' => array('admin'),
	'Update news',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->title) . '"' ?> news</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>