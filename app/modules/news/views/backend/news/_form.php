<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
    ));
    $model->published_at = $model->published_at ? $model->published_at : date('Y-m-d');
    $redactor = HApplication::getEditor();
?>
<fieldset>
	<?= $form->errorSummary($model); ?>
    
    <?= $form->textFieldRow($model, 'title', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'slug', array('class' => 'span8')); ?>        
    <?= $form->$redactor['method']($model, 'content', array(
        'options' => array_merge($redactor['options'], array('air' => false))
    )); ?>
    <?= $form->$redactor['method']($model, 'short_content', array( 
        'options' => array_merge($redactor['options'], array('minHeight' => 50))
    )); ?>
    <?= $form->datepickerRow($model, 'published_at', array(
        'prepend' => '<i class="icon-calendar"></i>',
        'options' => array('format' => 'yyyy-mm-dd')
    )); ?>
    <?= $form->dropDownListRow($model, 'status', $model::statuses()); ?>
    <?= $form->textFieldRow($model, 'meta_title', array('class' => 'span8')); ?>        
    <?= $form->textAreaRow($model, 'meta_keywords', array('class' => 'span8', 'rows' => 5)); ?>
    <?= $form->textAreaRow($model, 'meta_description', array('class' => 'span8', 'rows' => 5)); ?> 
    
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(t('news.backend', $model->getIsNewRecord() ? 'Create' : 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
))?>


<?php $this->endWidget(); ?>
