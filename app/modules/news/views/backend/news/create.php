<?php
$this->pageTitle = 'Create a news';
$this->breadcrumbs=array(
	'Manage news' => array('admin'),
	'Create a news',
);

$this->renderPartial('_form', array('model' => $model)); 
?>