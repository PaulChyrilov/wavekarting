<?php
$this->breadcrumbs = array(
    'News',
);
?>

<h1 class="page_title">News</h1>
<?php

$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'id' => 'news-list',
    'itemView' => '_view',
));
?>