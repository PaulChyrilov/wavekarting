<div class="news-item">
    <p class="date"><?= $data->published_at; ?></p>
    <p class="title"><?= CHtml::encode($data->title); ?></p>
    <div>
        <?= $data->short_content; ?>
    </div>
    <p class="read_more"><?= CHtml::link('Read more &raquo;', $data->getUrl());?></p>
</div>