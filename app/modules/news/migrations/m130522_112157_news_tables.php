<?php

class m130522_112157_news_tables extends CDbMigration
{

    public function up()
    {
        $sql = <<< EOD
DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL, 
  `content` text NOT NULL,
  `short_content` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published_at` date NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published_at_idx` (`published_at`),
  KEY `slug_idx` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;    
EOD;
        $this->execute($sql);
    }

    public function down()
    {
        
    }
}