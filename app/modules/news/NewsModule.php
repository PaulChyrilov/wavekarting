<?php

class NewsModule extends EWebModule 
{
    public $defaultController = 'news';
    
	public function init()
	{
        parent::init();
        
		$this->setImport(array(
			'news.models.*',
		));
	}
    
}
