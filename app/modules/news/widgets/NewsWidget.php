<?php

class NewsWidget extends CWidget 
{

	public $usePagination = 1;

    public $limit = 10;

	public function run() 
    {
		$criteria = new CDbCriteria;
		$criteria->addCondition('`status` = ' . News::PUBLISHED);
		$criteria->order = 'published_at DESC';
        $criteria->limit = $this->limit;
		
		$result = News::model()->getAllWithPagination($criteria);
		
		$this->render('news', array(
			'news' => $result['items'],
			'pages' => $result['pages'],
		));
	}
}