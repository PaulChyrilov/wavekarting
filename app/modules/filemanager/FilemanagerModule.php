<?php

class FilemanagerModule extends EWebModule 
{
    public $defaultController = 'files';
    
    public $filePath = 'webroot.files.site';
    
	public function init()
	{
        parent::init();
        
        $dir = Yii::getPathOfAlias($this->filePath);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
            chmod($dir, 0777);
        }
        
		$this->setImport(array(
			'filemanager.models.*',
		));
	}
    
    public function getFiles($indusry, $types)
    {
        return FileItem::getFiles($indusry, $types);
    }
    
    public function download($ids)
    {
        return FileItem::download($ids);
    }
    
}
