<?php
$this->pageTitle = 'Create a file';
$this->breadcrumbs=array(
	'Manage files' => array('admin'),
	'Create a file',
);

$this->renderPartial('_form', array('model' => $model)); 
?>