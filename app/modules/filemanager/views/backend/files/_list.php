<?php
$dataProvider = $model->search();
$maxOrder = $model->getMaxOrder();
$this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => $model->getGridId(false),
    'dataProvider' => $dataProvider,
    'ajaxUrl' => array('/filemanager/files/admin'),
    'filter'       => $model,
    'template'     => "{items}\n{pager}",
    'columns'      => array(
        array(
            'header' => 'File', 
            'value' => 'CHtml::link("Download", $data->getFileUrl(), array("target" => "_blank"))', 
            'type' => 'raw', 
            'htmlOptions' => array('style' => 'width:110px;')
        ),
		array(
            'header'   => 'Title',
            'type'     => 'raw',
            'name'     => 'searchTitle',
            'value'    => 'CHtml::encode($data->getI18n("title"))',
            'sortable' => false,
        ),
        array(
            'class'              => 'bootstrap.widgets.TbButtonColumn',
            'deleteConfirmation' => 'Are you sure to delete this file?',
            'template' => '{up} {down} {update} {delete}',
            'buttons' => array(
                'up' => array(
                    'url' => 'Yii::app()->createUrl("/filemanager/files/move", array("id" => $data->id, "dir" => "up"))',
                    'visible' => '$data->order != 1',
                    'label' => Yii::t('filemanager.backend', 'Up'),
                    'options' => array('data-op' => 'ajax'),
                    'icon' => 'chevron-up'
                ),
                'down' => array(
                    'url' => 'Yii::app()->createUrl("/filemanager/files/move", array("id" => $data->id, "dir" => "down"))',
                    'visible' => '$data->order != ' . $maxOrder,
                    'label' => Yii::t('filemanager.backend', 'Down'),
                    'options' => array('data-op' => 'ajax'),
                    'icon' => 'chevron-down'
                ),
            )
        ),
    ),
));