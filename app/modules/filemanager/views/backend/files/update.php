<?php
$this->pageTitle = 'Update a file';
$this->breadcrumbs=array(
	'Manage files' => array('admin'),
	'Update a file',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->getI18n('title')) . '"' ?> file</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>