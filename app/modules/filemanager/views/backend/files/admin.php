<?php
$this->pageTitle= 'Manage files';

$this->breadcrumbs=array(
	'Files',
);
?>

<h1>Manage files</h1>

<?= TbHtml::link(Yii::t('filemanager.backend', 'Create a new file'), array('create'), array('class' => 'btn')); ?>
<br />

<?php
$this->renderPartial('_list', compact('model')); 
?>