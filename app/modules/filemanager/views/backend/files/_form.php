<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
?>
<fieldset>    
    <?php 
        $this->renderPartial('_i18ns', array('model' => $model, 'form' => $form));
        $module = Yii::app()->getModule('categories');
    ?>
    <?= $form->select2Row($model, 'industries', array(
        'data' => $module->getListData('file_industry'),
        'multiple' => 'multiple',
        'encode' => false,
        'options' => array(            
            'width' => '618px',
            'placeholder' => 'Select industries ...',
        ))
    );?>
    <?= $form->select2Row($model, 'types', array(
        'data' => $module->getListData('file_type'),
        'multiple' => 'multiple',
        'encode' => false,
        'options' => array(            
            'width' => '618px',
            'placeholder' => 'Select types ...',
        ))
    );?>
    <?= $form->fileFieldRow($model, 'file', array('hint' => 'pdf, doc, docx ' . ($model->file ? CHtml::link("Download", $model->getFileUrl(), array("target" => "_blank")) : ''))); ?>
    
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(Yii::t('filemanager.backend', $model->getIsNewRecord() ? 'Create' : 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
))?>


<?php $this->endWidget(); ?>
