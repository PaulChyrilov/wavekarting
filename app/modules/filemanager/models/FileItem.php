<?php

class FileItem extends I18nActiveRecord
{
    public $created_at;
    public $updated_at;
    public $order;
    public $id;
    
    protected $_industries;
    protected $_types;
    
    public $file;
    public $searchTitle;
    
    
    public $i18nModel = 'FileItemI18n';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'files';
    }

    public function rules()
    {
        return array(
            array('industries, types', 'required'),
            array('file', 'file', 
                'types' => array('pdf', 'doc', 'docx'), 
                'maxSize' => 10 * 1024 * 1024, 'allowEmpty' => !$this->getIsNewRecord()
            ),
            array('searchTitle, category_id', 'safe', 'on' => 'search'),
            array('translateattrs', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'               => 'ID',
            'file'             => 'File',
            'category_id'      => 'Industry',
            'created_at'       => 'Create date',
            'updated_at'       => 'Update date',            
        );
    }
    
    public function getIndustries()
    {
        if ($this->_industries === null) {
            $this->_populateOptions();
        }
        return $this->_industries;
    }
    
    public function setIndustries($industries)
    {
        $this->_industries = $industries;
    }
    
    public function getTypes()
    {
        if ($this->_types === null) {
            $this->_populateOptions();
        }
        return $this->_types;
    }
    
    public function setTypes($types)
    {
        $this->_types = $types;
    }

    public function relations()
    {
        return array(
            'i18ns' => array(self::HAS_MANY, 'FileItemI18n', 'parent_id', 'index' => 'lang_id'),
            'options' => array(self::HAS_MANY, 'FileItemOption', 'file_id'),
        );
    }
    
    public function scopes()
    {
        $a = $this->getTableAlias(false, false);
        return array(
            'ordered' => array('order' => "$a.`order` ASC"),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        
        $criteria->select = 't.*';
        $criteria->join = 'INNER JOIN file_i18ns ON file_i18ns.parent_id = t.id';
        $criteria->group = 't.id';
        $criteria->compare('file_i18ns.title', $this->searchTitle, true);
        $criteria->scopes = array('ordered');
        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'           => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'setUpdateOnCreate' => true
            ),
        );
    }
    
    public function setTranslateAttrs($i18ns)
    {
        $this->setNestedRelation('i18ns', $i18ns);
        $this->clearDirtyAttributes($this->i18ns);
    }
    
    /**
     * @param CUploadedFile $file
     */
    public function upload($file)
    {
        if ($file === null) {
            return false;
        }
        $path = Yii::getPathOfAlias(Yii::app()->getModule('filemanager')->filePath);
        if ($this->file) {
            unlink($path . '/' . $this->file);
        }
        $this->file = strtr($file->getName(), array(' ' => '_'));
        return $file->saveAs($path . '/' . $this->file);
    }
    
    public function getFilePath()
    {
        return Yii::getPathOfAlias(Yii::app()->getModule('filemanager')->filePath) . '/' . $this->file;
    }
    
    public function getFileUrl()
    {
        return strtr(Yii::app()->getModule('filemanager')->filePath, array('webroot' => '', '.' => '/')) . '/' . $this->file;
    }
    
    public static function download($ids)
    {
        $models = self::model()->withLang()->findAllByPk($ids);
        if (count($models) === 0) {
            return false;
        }
        $filename = date('YmdHis') . uniqid() . '.zip';
        $path = Yii::getPathOfAlias('application.runtime') . '/' . $filename;
        Yii::import('ext.components.zip.MZip');
        $zip = new MZip();
        $zip->create($path);
        foreach ($models as $file) {
            $zip->addFile($file->getFilePath(), $file->file);
        }
        $zip->close();
        $content = file_get_contents($path);
        unlink($path);
        
        return array(
            'filename' => $filename,
            'content' => $content,
            'files' => $models
        );
    }
    
    public static function getFiles($indusry, $types)
    {
        return self::model()->withLang(null, false, array('together' => false))->findAll(
            array(
                'select' => 't.id',
                'join' => '
                INNER JOIN file_options AS fo 
                    ON fo.file_id = t.id AND (fo.option_id = "' . $indusry . '" AND fo.type = "industry") 
                INNER JOIN file_options AS fo2
                    ON fo2.file_id = t.id AND (
                        fo2.option_id IN ("' . implode('","', $types) . '") AND fo2.type = "type"
                    )',
                'group' => 't.id'
            )
        );
    }


    protected function _populateOptions()
    {
        $this->_industries = array();
        $this->_types = array();
        foreach ($this->options as $opt) {
            if ($opt->type == 'industry') {
                $this->_industries[] = $opt->option_id;
            } else {
                $this->_types[] = $opt->option_id;
            }
        }
    }

    protected function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->order = $this->getMaxOrder() + 1;
        }
        return parent::beforeSave();
    }
    
    protected function afterSave()
    {
        parent::afterSave();
        FileItemOption::model()->deleteAllByAttributes(array('file_id' => $this->id));
        $command = Yii::app()->getDb()->createCommand();
        $table = FileItemOption::model()->tableName();
        foreach ($this->_industries as $i) {
            $command->insert($table, array(
                'file_id' => $this->id,
                'option_id' => $i,
                'type' => 'industry'
            ));
        }
        foreach ($this->_types as $t) {
            $command->insert($table, array(
                'file_id' => $this->id,
                'option_id' => $t,
                'type' => 'type'
            ));
        }
    }


    protected function afterDelete()
    {
        parent::afterDelete();
        FileItemOption::model()->deleteAllByAttributes(array('file_id' => $this->id));
        unlink(Yii::getPathOfAlias(Yii::app()->getModule('filemanager')->filePath) . '/' . $this->file);
    }
}