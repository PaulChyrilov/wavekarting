<?php

class FileItemI18n extends EActiveRecord
{
    public $title;
    public $description;
    
    public $dirtyAttributes = array('title', 'description');

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'file_i18ns';
    }

    public function rules()
    {
        return array(
            array('title, lang_id', 'required'),
            array('title', 'length', 'max' => 255),
            array('description', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'title'   => 'Title',
            'description' => 'Description',
        );
    }
}