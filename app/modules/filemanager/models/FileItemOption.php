<?php

class FileItemOption extends CActiveRecord
{
    public $file_id;
    public $option_id;
    public $type;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'file_options';
    }
}