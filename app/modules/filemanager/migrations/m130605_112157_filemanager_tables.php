<?php

class m130605_112157_filemanager_tables extends CDbMigration
{

    public function safeUp()
    {
        $sql = <<< EOD
DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) NOT NULL, 
  `order` int(11) NOT NULL DEFAULT '1', 
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_idx` (`order`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `file_options`;
CREATE TABLE IF NOT EXISTS `file_options` (
  `file_id` int(11) NOT NULL,
  `option_id` varchar(255) NOT NULL, 
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`file_id`, `option_id`),
  KEY `type_idx` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `file_i18ns`;
CREATE TABLE IF NOT EXISTS `file_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id_idx` (`parent_id`),
  UNIQUE KEY `lang_id_parent_id_unique_idx` (`lang_id`,`parent_id`) 
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;   
EOD;
        $this->execute($sql);
    }

    public function down()
    {
        
    }
}