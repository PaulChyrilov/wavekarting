<?php

class FilesController extends BackendController
{
	public $modelName = 'FileItem';
	public $redirectTo = array('admin');
    public $defaultAction = 'admin';
    
    public function actionCreate()
    {
        $model = new $this->modelName;

        if (isset($_POST[$this->modelName])) {
            $model->setAttributes($_POST[$this->modelName]);
            if ($model->validate()) {
                $model->upload(CUploadedFile::getInstance($model, 'file'));
                $model->save(false);
                Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('common', 'Successfully created.'));
                $this->redirect($this->redirectTo);
            }
        }
        $this->render('create', array('model' => $model));
    }
    
    public function actionUpdate($id)
    {
        $model = $this->loadModel($this->modelName, $id);
        if (isset($_POST[$this->modelName])) {
            $model->setAttributes($_POST[$this->modelName]);
            if ($model->validate()) {
                $model->upload(CUploadedFile::getInstance($model, 'file'));
                $model->save(false);
                Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('common', 'Successfully updated.'));
                $this->redirect($this->redirectTo);
            }
        }

        $this->render('update', array('model' => $model));
    }


    public function actionMove($id, $dir)
    {
        $model = parent::actionMove($id, $dir);
        $this->renderJson(array(
            'html' => $this->renderPartial('_list', array('model' => new FileItem('search')), true),
            'success' => true,
            'target' => '#' . $model->getGridId(false)
        ));
    }
    
}