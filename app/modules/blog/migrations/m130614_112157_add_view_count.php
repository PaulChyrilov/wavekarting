<?php

class m130614_112157_add_view_count extends CDbMigration
{

    public function safeUp()
    {
        $sql = <<< EOD
ALTER TABLE `posts`
ADD COLUMN `view_count`  int NOT NULL DEFAULT 0 AFTER `published_at`;
EOD;
        $this->execute($sql);
    }

    public function down()
    {
        
    }
}