<?php

class TagsController extends BackendController
{
	public $modelName = 'PostTag';
    public $defaultAction = 'admin';
    
    public function actionSuggest()
    {
        if (isset($_GET['q']) && ($q = trim($_GET['q'])) !== '') {
            echo CJSON::encode(PostTag::suggest($q));
        }
    }
    
}