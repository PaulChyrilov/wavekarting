<?php

class PostsController extends BackendController
{
    public $modelName = 'Post';
    public $redirectTo = array('admin');
    public $defaultAction = 'admin';
}