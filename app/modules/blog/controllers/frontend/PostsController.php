<?php

class PostsController extends FrontendController
{
    public $modelName = 'Post';
    
    public $layout = '//layouts/blog';
    
    public function actionShow($slug)
    {
        $model = $this->loadModelBySlug($this->modelName, $slug);
        HApplication::attachMetaParams($model);
        $model->updateVisitCount();
        $this->render('view', array('model' => $model));
    }

    public function actionArchive($date)
    {
        $dataProvider = new CActiveDataProvider(Post::model()->published()->withLang()->archive($date));
        $this->render('archive', compact('dataProvider', 'date'));
    }
    
    public function actionCategory($code)
    {
        $category = Yii::app()->getModule('categories')->getByCode($code, true);
        $dataProvider = new CActiveDataProvider(Post::model()->published()->withLang()->byCategory($code));
        $this->render('category', compact('dataProvider', 'category'));
    }
    
    public function actionIndex($code = null, $q = null)
    {
        $category = null;
        $finder = Post::model()->published()->resent()->withLang();
        if ($code !== null) {
            $category = Yii::app()->getModule('categories')->getByCode($code, true);
            $finder->byCategory($code);
        } elseif (($q = trim($q)) !== '') {
            $finder->searching($q);
        }
        
        $dataProvider = new CActiveDataProvider($finder);
        $this->render('index', array(
            'dataProvider' => $dataProvider, 'category' => $category, 'q' => $q
        ));
    }
}