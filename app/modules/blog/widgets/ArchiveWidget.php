<?php

class ArchiveWidget extends CWidget
{
    public function run()
    {
        $archive = Yii::app()->getModule('blog')->getArchive();
        $this->render('archive', compact('archive'));
    }
}