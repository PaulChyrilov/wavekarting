<?php

class BlogCategoryWidget extends CWidget
{
    public $code = 'blog_category';
    public $activeCategory = null;
    
    public function run()
    {
        $categories = Yii::app()->getModule('categories')->getListCategory($this->code, false);
        $this->render('categories', compact('categories'));
    }
}