<h3><?= Yii::t('site', 'Categories')?></h3>
<ul>
    <li<?php if ($this->activeCategory === null) { ?> class="active"<?php } ?>><?= CHtml::link(Yii::t('blog', 'All posts'), array('/blog/posts/index'))?></li>
    <?php foreach ($categories as $cat) : ?>
        <li<?php if ($cat->code == $this->activeCategory) { ?> class="active"<?php } ?>><?= CHtml::link(CHtml::encode($cat->getI18n('name')), array('/blog/posts/index', 'code' => $cat->code))?></li>
    <?php endforeach; ?>
</ul>