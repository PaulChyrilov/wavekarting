<h3><?= Yii::t('site', 'Latest posts')?></h3>
<ul>
    <?php foreach ($posts as $post) { ?>
    <li><?= CHtml::link($post->getI18n('title'), $post->getUrl())?></li>
    <?php } ?>
</ul>