<h3><?= Yii::t('mainpage', 'News')?></h3>
<?php foreach ($posts as $post) { ?>
    <h4><?= CHtml::encode($post->getI18n('title')) ?></h4>
    <div>
    <?= $post->getI18n('short_content') ?>
    </div>
    <?= CHtml::link(Yii::t('common', 'read more &raquo;'), $post->getUrl(), array('class' => 'button'))?>
<?php } ?>