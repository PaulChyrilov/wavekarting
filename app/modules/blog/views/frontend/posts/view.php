<?php $this->renderPartial('_sidebar', array('category' => false)) ?>
<nav class="navigate">
    <?php if (($prev = $model->getPrevPost()) !== null) { ?>
    <a class="prev" href="<?= $prev->getUrl() ?>">&laquo; <?= CHtml::encode($prev->getI18n('title')) ?></a>
    <?php } ?>
    <?php if (($next = $model->getNextPost()) !== null) { ?>
    <a class="next" href="<?= $next->getUrl() ?>"><?= CHtml::encode($next->getI18n('title')) ?> &raquo;</a>
    <?php } ?>
    <div class="clear"></div>
</nav>

<article>
    <h2><?= CHtml::encode($model->getI18n('title')); ?></h2>
    <?php if ($model->image) { ?>
    <figure>
        <?= CHtml::image($model->getImageUrl('image', 'post_preview')) ?>
    </figure>
    <?php } ?>
    <div class="post">
        <div class="description">
            <div class="date"><?= date('d.m.Y', strtotime($model->published_at)); ?></div>
            <?php $cat = $model->getCategory(); ?>
            <div class="category"><?= Yii::t('blog', 'Posted in ') ?><?= CHtml::link($cat->getI18n('name'), array('/blog/posts/index', 'code' => $cat->code)) ?>
                <div class="bubble">
                    <div class="content"><?= $model->view_count ?></div>
                    <div class="arrow"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="text"><?= $model->getI18n('content'); ?></div>
    </div>
    <div class="clear"></div>
</article>

<div class="social-blog">
    <?= block('post_social_buttons') ?>
</div>