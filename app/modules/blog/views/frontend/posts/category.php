<?php
$this->breadcrumbs = array(
    'Blog' => array('index'),
    'Category ' . $category->getI18n('name')
);
?>
<h1><?= Yii::t('blog', 'Posts from category: ')?><?= $category->getI18n('name') ?></h1>
<?php $this->renderPartial('_list', compact('dataProvider')); ?>
