<?php $this->beginClip('blog_sidebar')?>
<nav class="navigate">
    <?php $this->widget('application.modules.blog.widgets.BlogCategoryWidget', array(
        'activeCategory' => isset($category) ? ($category ? $category->code : false) : null
    ));?>
</nav>
<div class="search">
    <h3><?= Yii::t('site', 'Search in blog')?></h3>
    <div class="group-search">
        <?= CHtml::form(array('/blog/posts/index'), 'get') ?>
        <?= CHtml::textField('q', isset($q) ? $q : '', array())?>
        <?= CHtml::submitButton('', array('name' => '')) ?>
        <?= CHtml::endForm() ?>
    </div>
</div>
<nav class="posts">
    <?php $this->widget('application.modules.blog.widgets.LatestPostWidget', array(
        'limit' => 5
    ));?>
</nav>
<?php $this->endClip()?>