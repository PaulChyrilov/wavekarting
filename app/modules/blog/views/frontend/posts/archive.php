<?php
$this->breadcrumbs = array(
    'Blog' => array('index'),
    'Archive ' . $date
);
?>
<h1>Posts archived <?= $date ?></h1>
<?php

$this->renderPartial('_list', compact('dataProvider'));
$this->renderPartial('_sidebar');
?>
