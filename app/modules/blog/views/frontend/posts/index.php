<?php
$this->setPageTitle(Yii::t('blog', 'Blog'));

$this->renderPartial('_list', compact('dataProvider'));
$this->renderPartial('_sidebar', array('category' => $category, 'q' => $q));
?>