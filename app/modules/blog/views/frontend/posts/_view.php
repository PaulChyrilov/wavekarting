<article>
    <h2><?= CHtml::link(CHtml::encode($data->getI18n('title')), $data->getUrl()); ?></h2>
    <?php if ($data->image) { ?>
    <figure>
        <?= CHtml::image($data->getImageUrl('image', 'post_preview')) ?>
    </figure>
    <?php } ?>
    <div class="post">
        <div class="description">
            <div class="date"><?= date('d.m.Y', strtotime($data->published_at)); ?></div>
            <?php $cat = $data->getCategory(); ?>
            <div class="category"><?= Yii::t('blog', 'Posted in ') ?><?= CHtml::link($cat->getI18n('name'), array('/blog/posts/index', 'code' => $cat->code)) ?>
                <div class="bubble">
                    <div class="content"><?= $data->view_count ?></div>
                    <div class="arrow"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="text"><?= $data->getI18n('short_content'); ?></div>
    </div>
    <div class="clear"></div>
    <?= CHtml::link(Yii::t('common', 'read more &raquo;'), $data->getUrl(), array('class' => 'button post-read-more'))?>
</article>