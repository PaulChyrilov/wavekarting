<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
    $model->published_at = $model->published_at ? $model->published_at : date('Y-m-d');
?>
<fieldset>
    <?php $this->renderPartial('_i18ns', array('model' => $model, 'form' => $form)) ?>
    <?= $form->textFieldRow($model, 'slug', array('class' => 'span8')); ?>        
    <?= $form->select2Row($model, 'stringTags', array(
        'asDropDownList' => false, 
        'options' => array(            
            'width' => '618px',
            'tags' => $model->getAllTags(),
            'placeholder' => '',
            'tokenSeparators' => array(','),
            'minimumInputLength' => 2,
//            'ajax' => array(
//                'quietMillis' => 200,
//                'cache' => true,
//                'data' => 'js:function (term, page) {
//                    return {q: term};
//                }',
//                'url' => $this->createUrl('/blog/tags/suggest'),
//                'dataType' => 'json',
//                'results' => 'js:function (data, page) {
//                    return {results: data};
//                }'
//            )
        ))
    );?>
    <?= $form->dropDownListRow(
        $model, 
        'category_id', 
        Yii::app()->getModule('categories')->getListData('blog_category'), 
        array('encode' => false, 'prompt' => 'Select category ...')
    ); ?>
    <?= $form->datepickerRow($model, 'published_at', array(
        'prepend' => '<i class="icon-calendar"></i>',
        'options' => array('format' => 'yyyy-mm-dd')
    )); ?>
    <?= $form->fileFieldRow($model, 'image', array('hint' => 'image size: 330x218 <br/>' . ($model->image ? CHtml::image($model->getImageUrl('image', 'image_preview'), '') : ''))); ?>
    <?= $form->dropDownListRow($model, 'status', $model::statuses()); ?>
    
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(t('blog.backend', $model->getIsNewRecord() ? 'Create' : 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
))?>

<?php $this->endWidget(); ?>
