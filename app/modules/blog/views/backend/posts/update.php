<?php
$this->pageTitle = 'Update post';
$this->breadcrumbs=array(
    'Manage posts' => array('admin'),
    'Update post',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->getI18n('title')) . '"' ?> post</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>