<?php
$this->pageTitle= 'Manage posts';

$this->breadcrumbs=array(
	'Posts',
);
?>

<h1>Manage posts</h1>

<?= TbHtml::link(Yii::t('posts.backend', 'Create a new post'), array('create'), array('class' => 'btn')); ?>
<br />

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => $model->getGridId(),
    'dataProvider' => $model->search(),
    'filter' => $model,
    'template' => "{items}\n{pager}",
    'columns' => array(
        array(
            'header'   => 'Title',
            'type'     => 'raw',
            'name'     => 'searchTitle',
            'value'    => 'CHtml::encode($data->getI18n("title"))',
            'sortable' => false,
        ),
        array(
            'name' => 'slug',
            'value' => '$data->slug',
            'sortable' => false,
        ),
        array(
            'name' => 'status',
            'value' => 'Post::statuses($data->status)',
            'filter' => Post::statuses()
        ),
        array(
            'name' => 'published_at',
            'filter' => false,
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} {delete}',
            'deleteConfirmation' => 'Are you sure to delete this post?',
        ),
    ),
)); 
?>