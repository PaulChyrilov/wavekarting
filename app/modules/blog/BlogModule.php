<?php

class BlogModule extends EWebModule
{

    public $defaultController = 'posts';

    public function init()
    {
        parent::init();
        Yii::app()->setComponents(array(
            'image' => array(
                'presets' => array(
                    'post_preview' => array(
                        'adaptiveThumb' => array('width' => 330, 'height' => 218)
                    ),
                )
            )
        ));

        $this->setImport(array(
            'blog.models.*',
        ));
    }
    
    public function getArchive()
    {
        return Post::getArchive();
    }
    
    public function getArchiveDate($date)
    {
        $tokens = explode('-', $date);
        if (count($tokens) !== 2) {
            return '';
        }
        return Yii::app()->getLocale()->getMonthName((int) $tokens[1], 'wide', true) . ' ' . $tokens[0];
    }

}
