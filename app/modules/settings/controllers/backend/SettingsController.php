<?php

class SettingsController extends BackendController
{
	public $modelName = 'Member';
	public $redirectTo = array('admin');
    public $defaultAction = 'admin';
    
    public function actionAdmin()
    {
        $model = new Setting;
        if (isset($_POST['Setting'])) {
            $model->setAttributes($_POST['Setting']); 
            if ($model->perform()) {
                $this->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('common', 'Successfully created.'));
                $this->refresh();
            }
        }
        $this->render('admin', compact('model'));
    }
}