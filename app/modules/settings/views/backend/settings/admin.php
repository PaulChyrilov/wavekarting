<?php
$this->pageTitle = 'Manage settings';
$this->breadcrumbs = array(
	'Manage settings'
);
?>

<h1>Manage settings</h1>
<br />
<?php
$this->renderPartial('_form', array('model' => $model));
?>