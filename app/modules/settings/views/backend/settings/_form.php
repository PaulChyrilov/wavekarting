<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'setting-form',
        'enableClientValidation' => true,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
?>
<fieldset>
    <?= $form->textFieldRow($model, 'project_name', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'mailchimp.apikey', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'mailchimp.list', array('class' => 'span8')); ?>
    <?= $form->textFieldRow($model, 'application_form.email', array('class' => 'span8')); ?>
    <?= $form->textFieldRow($model, 'download_file.email', array('class' => 'span8')); ?>
    <?= $form->textFieldRow($model, 'twitter.channel', array('class' => 'span8')); ?>
    <?= $form->textFieldRow($model, 'twitter.oauth_access_token', array('class' => 'span8')); ?>
    <?= $form->textFieldRow($model, 'twitter.oauth_access_token_secret', array('class' => 'span8')); ?>
    <?= $form->textFieldRow($model, 'twitter.consumer_key', array('class' => 'span8')); ?>
    <?= $form->textFieldRow($model, 'twitter.consumer_secret', array('class' => 'span8')); ?>
    <?= $form->fileFieldRow($model, 'logo', array('hint' => 'size: 123 x 50 ' . ($model->logo ? CHtml::image($model->logo, '') : '') )); ?>
    <?= $form->dropDownListRow($model, 'editor', Setting::$editors); ?>
    
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(t('settings.backend', 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
))?>
<?php $this->endWidget(); ?>