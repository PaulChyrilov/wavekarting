<?php

class Setting extends CFormModel
{
    private $_file;
    private $_params;
    
    public static $editors = array(
        'impravieditor' => 'Impravi Editor',
        'ckeditor' => 'CKEditor',
    );

    public function __construct($scenario = '')
    {
        $this->_file = Yii::getPathOfAlias(Yii::app()->getModule('settings')->settingPath) . '.php';
        $this->_params = require $this->_file;
        parent::__construct($scenario);
    }

    public function getAttributes($names = null)
    {
        return $this->_params;
    }

    public function __get($name)
    {
        if (isset($this->_params[$name])) {
            return $this->_params[$name];
        }

        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        if (isset($this->_params[$name])) {
            $this->_params[$name] = $value;
        } else {
            parent::__set($name, $value);
        }
    }

    public function rules()
    {
        $keys = array_keys($this->_params);
        unset($keys[array_search('logo', $keys)]);
        return array(
            array(implode(',', $keys), 'required'),
            array('application_form.email', 'email'),
            array(
                'logo', 'ext.components.image_processor.MImageValidator', 'allowEmpty' => trim($this->_params['logo']) !== '',
                'types' => array('jpg', 'png', 'jpeg'), 'maxSize' => 5 * 1024 * 1024, 
                'minWidth' => 20, 'minHeight' => 20
            ),
        );
    }
    
    public function behaviors()
    {
        return array(
            'mImage' => array(
                'class'          => 'ext.components.image_processor.MImageBehavior',
                'imageProcessor' => 'image' // image processor component name 
            )
        );
    }

    public function attributeLabels()
    {
        return array(
            
        );
    }

    public function perform()
    {
        if ($this->validate()) {
            if (($logo = CUploadedFile::getInstance($this, 'logo')) !== null) {
                $webroot = Yii::getPathOfAlias('webroot');
                if ($this->_params['logo'] && file_exists($webroot . $this->_params['logo'])) {
                    @unlink($webroot . $this->_params['logo']);
                }
                $this->uploadImage($logo, 'logo');
                $this->_params['logo'] = $this->getImageUrl('logo', 'orig');
            }
            return file_put_contents(
                $this->_file, '<?php return ' . var_export($this->_params, true) . ';'
            );
        }
        return false;
    }

}