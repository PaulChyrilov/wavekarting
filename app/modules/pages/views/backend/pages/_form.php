<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
    ));
?>
<fieldset>    
    <?php $this->renderPartial('_i18ns', array('model' => $model, 'form' => $form)) ?>    
    <?= $form->textFieldRow($model, 'code', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'slug', array('class' => 'span8')); ?>        
    <?= $form->dropDownListRow(
        $model, 
        'category_id', 
        Yii::app()->getModule('categories')->getListData('page_category'), 
        array('encode' => false, 'prompt' => 'Select category')
    ); ?>
    <?= $form->dropDownListRow($model, 'template', $model::templates()); ?>
    <?= $form->dropDownListRow($model, 'status', $model::statuses()); ?>
    
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(t('pages.backend', $model->getIsNewRecord() ? 'Create' : 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
))?>


<?php $this->endWidget(); ?>
