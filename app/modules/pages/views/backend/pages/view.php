<?php
$this->pageTitle=param('siteName') . ' - Просмотр новости';
$this->breadcrumbs=array(
	'Администрирование' => array('/admin/backend/main/index'),
	'Управление новостями' => array('admin'),
	'Просмотр новости',
);
$this->menu = array(
	array('label' => 'Добавить новость', 'url' => array('create')),
	array('label' => 'Редактировать новость', 'url' => array('update', 'id' => $model->id)),
	array('label' => 'Удалить новость', 'url' => '#',
		'url'=>'#',
		'linkOptions'=>array(
			'submit'=>array('delete','id'=>$model->id),
			'confirm'=> 'Вы действительно хотите удалить выбранную новость?'
		),
	),
	
);

$this->renderPartial('view', array('model' => $model));