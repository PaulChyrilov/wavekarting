<?php
$this->pageTitle = 'Create page';
$this->breadcrumbs=array(
	'Manage pages' => array('admin'),
	'Create page',
);

$this->renderPartial('_form', array('model' => $model)); 
?>