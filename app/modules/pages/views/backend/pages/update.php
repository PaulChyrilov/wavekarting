<?php
$this->pageTitle = 'Update page';
$this->breadcrumbs=array(
	'Manage pages' => array('admin'),
	'Update page',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->getI18n('title')) . '"' ?> page</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>