<section class="body-page">

    <header>
        <h2><?= $model->getI18n('title') ?></h2>
    </header>

    <section class="content-block backgrounded">
        <div class="common">
            <div class="content flashable">
                <?= $model->getI18n('content'); ?>
                <div class="clear"></div>
            </div>
            <?php if (in_array($model->category_id, array('services', 'solutions', 'technology'))) { ?>
            <nav class="service-navigate">
                <?php if (($prev = $model->getPrev()) !== null) { ?>
                <a class="prev" title="<?= CHtml::encode($prev->getI18n('title')) ?>" href="<?= $prev->getUrl() ?>">&nbsp;</a>
                <?php } ?>
                <?php if (($next = $model->getNext()) !== null) { ?>
                <a class="next" title="<?= CHtml::encode($next->getI18n('title')) ?>" href="<?= $next->getUrl() ?>">&nbsp;</a>
                <?php } ?>
                <div class="clear"></div>
            </nav>
            <?php } ?>
        </div>
    </section>

</section>

<?php Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/css/pages/common.css') ?>
