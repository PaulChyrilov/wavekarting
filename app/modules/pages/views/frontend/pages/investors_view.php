<section class="body-page">

    <header>
        <h2><?= CHtml::encode($model->getI18n('title'))?></h2>
    </header>

    <section class="content-block backgrounded">
        <div class="investors-content">
            <?= $model->getI18n('content') ?>
            <div class="clear"></div>
        </div>

        <hr/>

        <div class="list-proposal">
            <?= block('investor_proposition', array('{LOGIN_LINK}' => $this->createUrl('/files/login'))) ?>
        </div>

    </section>

</section>
<?php Yii::app()->getClientScript()
    ->registerCssFile(Yii::app()->baseUrl . '/css/pages/investors.css')
?>

