<section class="body-page">
    <header>
        <h2><?= $model->getI18n('title') ?></h2>
    </header>

    <section class="content-block backgrounded">

        <div class="community">

            <figure>
                <img src="/images/video1.jpg" />
                <?= CHtml::link('', array('#'), array()) ?>
            </figure>

            <section class="social-block">
                <?= $model->getI18n('content'); ?>
                <hr/>
                <div class="social-group">
                    <h3><?= Yii::t('mainpage', 'Follow us on:')?></h3>
                    <nav>
                        <a href="#" class="twitter"><</a>
                        <a href="#" class="facebook"></a>
                        <a href="#" class="google"></a>
                    </nav>
                </div>
            </section>

            <div class="clear"></div>

        </div>

    </section>

</section>

<?php Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/css/pages/community.css') ?>
