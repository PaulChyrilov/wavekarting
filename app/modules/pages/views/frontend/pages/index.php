<?php
$this->breadcrumbs = array(
    'Pages',
);
?>

<h1 class="page_title">Pages</h1>
<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'id' => 'page-list',
    'itemView' => '_view',
));
?>