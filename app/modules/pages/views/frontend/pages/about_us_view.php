<section class="body-page">
    <header>
        <h2><?= CHtml::encode($model->getI18n('title'))?></h2>
        <?php $this->widget('zii.widgets.CMenu', array(
            'items' => Yii::app()->getModule('menu')->getByCode('about_us')
        )) ?>
    </header>

    <section class="content-block backgrounded">

        <div class="about-us flashable">
            <?= $model->getI18n('content') ?>
<!--            <div class="col2">
                <div class="offset30 line">
                    <h2>Subtitle</h2>
                    <p>Why isn't there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>
                    <p>Why isn't there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>
                    <p>Why isn't there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>
                    <p>Why isn't there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>
                </div>
            </div>

            <div class="col2">
                <div class="offset30">
                    <h2>Subtitle</h2>
                    <p>Why isn't there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>
                    <p>Why isn't there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>
                    <p>Why isn't there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>
                    <p>Why isn't there a fancy file element upload button for twitter bootstrap? it would be sweet if the blue primary button was implemented for the upload button. Is it even possible to finesse the upload button using CSS (seems like a native browser element that cant be manipulated)</p>
                </div>
            </div>-->
        </div>

    </section>

    <!--<section class="group-block">

        <header>
            <p><?= Yii::t('aboutpage', 'our team') ?></p>
            <hr/>
        </header>

        <div class="wrapper">
            <?php // $this->widget('application.modules.team.widgets.MemberWidget') ?>
        </div>

    </section>-->

</section>

<?php Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/css/pages/about.css') ?>
