<?php

class Page extends I18nActiveRecord
{
    const DRAFT = '0';
    const PUBLISHED = '1';
    const ARCHIVED = '2';

    public $created_at;
    public $updated_at;
    public $slug;
    public $code;
    public $template;
    public $category_id;
    public $searchTitle;
    
    
    public $i18nModel = 'PageI18n';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'pages';
    }

    public function rules()
    {
        return array(
            array('code, template', 'required'),
            array('code', 'unique'),
            array('status, category_id', 'safe'),
            array('code, status, searchTitle', 'safe', 'on' => 'search'),
            array('translateattrs, slug', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'               => 'ID',
            'code'             => 'Code',
            'template'         => 'Template',
            'slug'             => 'Slug',
            'category_id'      => 'Category',
            'created_at'       => 'Create date',
            'updated_at'       => 'Update date',
            'status'           => 'Status',            
        );
    }
    
    public function relations()
    {
        return array(
            'i18ns' => array(self::HAS_MANY, 'PageI18n', 'parent_id', 'index' => 'lang_id')
        );
    }
    
    public function scopes()
    {
        $a = $this->getTableAlias(false, false);
        return array(
            'published' => array('condition' => "$a.`status` = " . self::PUBLISHED),
            'ordered' => array('order' => "$a.`order` ASC"),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        
        $criteria->select = 't.*';
        $criteria->join = 'INNER JOIN page_i18ns ON page_i18ns.parent_id = t.id';
        $criteria->group = 't.id';
        $criteria->compare('page_i18ns.title', $this->searchTitle, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('category_id', $this->category_id, true);

        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'sort'       => array(
                'defaultOrder' => 'created_at DESC',
            ),
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'           => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'setUpdateOnCreate' => true
            ),
            'slugable' => array(
                'class' => 'ext.behaviors.SlugBehavior',
                'scenarios' => array('insert'),
                'sourceAttribute' => function($owner) {
                    return $owner->getI18n('title', false, Yii::app()->getModule('languages')->getDefault());
                }
            ),
        );
    }
    
    public function setTranslateAttrs($i18ns)
    {
        $this->setNestedRelation('i18ns', $i18ns);
        $this->clearDirtyAttributes($this->i18ns);
    }
    
    public function getUrl()
    {
        return Yii::app()->createUrl('/pages/pages/show', array(
            'slug' => $this->slug,
        ));
    }
    
    public function getCategory()
    {
        return Yii::app()->getModule('categories')->getByCode($this->category_id);
    }
    
    public static function sitemap($class = __CLASS__, $lang = null)
    {
        $map = array();
        $condition = array('select' => 'id, slug, updated_at, category_id');
        foreach (CActiveRecord::model($class)->published()->withLang($lang)->findAll($condition) as $model) {
            $map[] = array(
                'id' => $model->id,
                'url' => $model->getUrl(),
                'title' => $model->getI18n('title'),
                'category' => $model->category_id,
                'updated_at' => $model->updated_at
            );
        }
        return $map;
    }
    
    public static function statuses($status = null)
    {
        static $statuses = array(
            self::DRAFT => 'Draft',
            self::PUBLISHED => 'Published',
            self::ARCHIVED => 'Archived',
        );
        return $status === null ? $statuses : $statuses[$status];
    }
    
    public static function templates()
    {
        return array(
            'view' => 'General',
            'investors_view' => 'Investors',
            'about_us_view' => 'About Us',
            'contact_us_view' => 'Contact Us',
            'community_view' => 'Community'
        );
    }
    
    
    public static function getByCode($code)
    {
        return self::model()->findByAttributes(array('code' => $code));
    }
    
    public static function getBySlug($slug)
    {
        return self::model()->findByAttributes(array('slug' => $slug));
    }
    
    public static function suggest($keyword, $lang = null, $limit = 10)
    {
        $pageIds = Yii::app()->getDb()->createCommand()
            ->select('parent_id')
            ->from('page_i18ns')
            ->where(
                'lang_id = :lang_id AND title LIKE :keyword', 
                array(
                    ':lang_id' => $lang, 
                    ':keyword' => '%' . strtr($keyword, array('%'    => '\%', '_'    => '\_', '\\'   => '\\\\')) . '%',
                )
            )
            ->limit($limit)
            ->queryColumn();
        
        if (count($pageIds) === 0) {
            return array();
        }
        
        $pages = self::model()->resetScope()->published()->withLang($lang)->findAllByPk(
            $pageIds,
            array(
                'select' => 'id, slug',
                'order' => 'slug ASC'
            )
        );
        $names = array();
        Yii::app()->getUrlManager()->showScriptName = false;
        foreach ($pages as $page) {
            $names[] = array(
                'id' => $page->id, 
                'label' => $page->getI18n('title', false, $lang), 
                'value' => $page->getI18n('title', false, $lang), 
                'url' => $page->getUrl()
            );
        }
        Yii::app()->getUrlManager()->showScriptName = true;
        return $names;
    }
    
    public static function getIds($category)
    {
        static $ids;
        if (empty($category)) {
            $category = 'empty';
        }
        if (!isset($ids[$category])) {
            $key = "page-$category-ids";
            if (($ids[$category] = Yii::app()->getCache()->get($key)) === false) {
                $ids[$category] = Yii::app()->getDb()->createCommand(
                    'SELECT t.id FROM `pages` AS t WHERE category_id = :category_id'
                )->queryColumn(array(':category_id' => $category));
                Yii::app()->getCache()->set($key, $ids[$category]);
            }
        }
        return $ids[$category];
    }
    
    public function getNext($lang = null)
    {
        $ids = self::getIds($this->category_id);
        if (($i = array_search($this->id, $ids)) !== false && isset($ids[$i + 1])) {
            return self::model()->resetScope()->withLang($lang)->findByPk($ids[$i + 1]);
        }
        return null;
    }
    
    public function getPrev($lang = null)
    {
        $ids = self::getIds($this->category_id);
        if (($i = array_search($this->id, $ids)) !== false && isset($ids[$i - 1])) {
            return self::model()->resetScope()->withLang($lang)->findByPk($ids[$i - 1]);
        }
        return null;
    }
    
    protected function afterSave()
    {
        parent::afterSave();
        $category = $this->category_id;
        if (empty($category)) {
            $category = 'empty';
        }
        $cache = Yii::app()->getCache();
        $cache->delete("page-$category-ids");
        $cache->delete('page-' . $this->getOldValue('category_id') . '-ids');
    }
}