<?php

class PagesModule extends EWebModule 
{
    public $defaultController = 'pages';
    
    public function init()
    {
        parent::init(); 

        $this->setImport(array(
            'pages.models.*',
        ));
    }
}
