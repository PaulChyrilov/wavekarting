<?php

class m130529_142157_add_page_template extends CDbMigration
{

    public function up()
    {
        $sql = <<< EOD
ALTER TABLE `pages`
ADD COLUMN `template`  varchar(255) NOT NULL DEFAULT 'view' AFTER `code`;
EOD;
        $this->execute($sql);
    }

    public function down()
    {
        
    }
}