<?php

class PagesController extends BackendController
{
    public $modelName = 'Page';
    public $redirectTo = array('admin');
    public $defaultAction = 'admin';
    
    public function actionSuggest()
    {
        if (isset($_GET['term']) && ($q = trim($_GET['term'])) !== '') {
            echo CJSON::encode(Page::suggest($q, isset($_GET['lang']) ? $_GET['lang'] : null));
        }
    }
    
}