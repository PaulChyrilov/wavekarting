<?php
/* @var $this CategoriesController */
/* @var $model Category */

$this->breadcrumbs=array(
    'Categories' => array('admin'),
    'Create',
);
?>

<h1>Create category list</h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>