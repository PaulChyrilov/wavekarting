<?php
/* @var $this CategoriesController */
/* @var $model Category */
/* @var $form TbActiveForm */
?>
<br />
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'     => $model->getFormId(),
    'type' => TbHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>
<fieldset>
	<?= $form->errorSummary($model); ?>
    
    <?= $form->textFieldRow($model, 'code', array('maxlength' => 64)); ?>
    
    <?= $form->textFieldRow($model, 'name', array('maxlength' => 128)); ?>

</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(Yii::t('categories.backend', $model->getIsNewRecord() ? 'Create' : 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
)); ?>
<?php $this->endWidget(); ?>