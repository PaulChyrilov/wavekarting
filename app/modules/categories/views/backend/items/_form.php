<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $item->getFormId(),
        'action' => array('/categories/items/edit', 'categoryId' => $item->category_id, 'id' => $item->id),
        'type' => 'horizontal',
        'enableClientValidation' => false,
        'enableAjaxValidation' => false
    ));
?>
<fieldset>
    <?php $this->renderPartial('_i18ns', array('model' => $item, 'form' => $form)) ?>    
    <?= $form->dropDownListRow($item, 'parent_id', $parents, array('encode' => false)); ?>
    <?= $form->textFieldRow($item, 'code'); ?>
    <?= $form->textFieldRow($item, 'url'); ?>
</fieldset>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => Yii::t('categories.backend', $item->getIsNewRecord() ? 'Create' : 'Save'))); ?>
</div>
<?php $this->endWidget(); ?>
<?php Yii::app()->getClientScript()->registerScript('suggest-url', '    
    $("#' . $item->getFormId() . '").submit(function() {
        var form = $(this); 
        form.data("dataType", "json");
        CMS.submitForm(form, function(err, rsp) {
            if (err) {
                console.log(err);
                return;
            } else if (rsp.target && rsp.html) {
                $(rsp.target).html($(rsp.target, $("<div>" + rsp.html + "</div>")).html());
            }
            if (rsp.success) {
                CMS.popup.el.modal("hide");
            }
        });
        return false;
    }); 

');

