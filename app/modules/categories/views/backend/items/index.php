<?= TbHtml::link(
    Yii::t('categories.backend', 'Create a new category'), 
    array('/categories/items/edit', 'categoryId' => $category->id), 
    array('class' => 'btn btn-success', 'data-op' => 'modal', 'data-title' => 'Edit category')
); ?>
<br />
<?php $this->renderPartial('_list', compact('category', 'provider')) ?>