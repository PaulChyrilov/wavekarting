<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'type'=>'striped bordered',
	'dataProvider' => $provider,
	'template' => "{items}",
    'id' => $category->getGridId(),
	'columns' => array(
        array('header' => 'Name', 'value' => 'str_repeat("&nbsp;&nbsp;&nbsp;", $data->level - 1) . $data->getI18n("name")', 'type' => 'raw'),
        'code',
        'url',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{up} {down} {update} {delete}',
            'htmlOptions' => array('style' => 'width:80px;'),
            'buttons' => array(
                'update' => array(
                    'visible' => '$data->level != 1',
                    'url' => 'Yii::app()->createUrl("/categories/items/edit", array("id" => $data["id"], "categoryId" => ' . $category->id . '))',
                    'options' => array('data-op' => 'modal', 'data-title' => 'Edit category', 'data-skip' => 1)
                ),                
                'delete' => array(
                    'visible' => '$data->level != 1',
                    'url' => 'Yii::app()->createUrl("/categories/items/delete", array("id" => $data->id))',
                ),
                'up' => array(
                    'url' => 'Yii::app()->createUrl("/categories/items/move", array("id" => $data->id, "dir" => "up"))',
                    'visible' => '$data->level != 1',
                    'label' => Yii::t('menu.backend', 'Up'),
                    'options' => array('data-op' => 'ajax'),
                    'icon' => 'chevron-up'
                ),
                'down' => array(
                    'url' => 'Yii::app()->createUrl("/categories/items/move", array("id" => $data->id, "dir" => "down"))',
                    'visible' => '$data->level != 1',
                    'label' => Yii::t('menu.backend', 'Down'),
                    'options' => array('data-op' => 'ajax'),
                    'icon' => 'chevron-down'
                ),
            )
        )
    ),
));