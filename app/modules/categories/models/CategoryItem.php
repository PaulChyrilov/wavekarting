<?php

class CategoryItem extends I18nActiveRecord 
{
    public $parent_id;
    public $i18nModel = 'CategoryItemI18n';

    public static function model($className = __CLASS__) 
    {
        return parent::model($className);
    }

    public function tableName() 
    {
        return 'category_items';
    }

    public function rules() 
    {
        return array(
            array('parent_id', 'required'),
            array('url, code', 'length', 'max' => 255),
            array('translateattrs, code', 'safe'),
            array('id, url', 'safe', 'on' => 'search'),
        );
    }
    
    public function behaviors()
    {
        return array(
            'nestedSetBehavior' => array(
                'class' => 'ext.behaviors.NestedSetBehavior',
                'hasManyRoots' => true,
                'rootAttribute' => 'root',
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'levelAttribute' => 'level',
            ),
            'slugable' => array( 
                'class'     => 'ext.behaviors.SlugBehavior',
                'scenarios' => array('insert'),
                'sourceAttribute' => function($owner) {
                    return $owner->getI18n('name', false, Yii::app()->getModule('languages')->getDefault());
                },
                'slugAttribute' => 'code'
            ),
            'CTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created_at',
                'updateAttribute'   => null
            )
        );
    }
    
    public function relations()
    {
        return array(
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'i18ns' => array(self::HAS_MANY, 'CategoryItemI18n', 'parent_id', 'index' => 'lang_id')
        );
    }

    public function attributeLabels() 
    {
        return array(
            'id' => Yii::t('categories.backend', 'ID'),
            'url' => Yii::t('categories.backend', 'Url'),
            'code' => Yii::t('categories.backend', 'Code'),
            'parent_id' => Yii::t('categories.backend', 'Parent'),
        );
    }
    
    public function setTranslateAttrs($i18ns)
    {
        $this->setNestedRelation('i18ns', $i18ns);
        $this->clearDirtyAttributes($this->i18ns);
    }

    public function search() 
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('code', $this->code, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public static function getByCode($code, $lang = null)
    {
        static $categories = array();
        $key = $code . '.' . $lang;
        if (!isset($categories[$key])) {
            $categories[$key] = self::model()->withLang($lang)->findByAttributes(array('code' => $code));
        }
        return $categories[$key];
    }
    
    public function populateParent()
    {
        $this->parent_id = $this->parent()->find()->id;
    }
}