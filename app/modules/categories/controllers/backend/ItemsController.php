<?php

class ItemsController extends BackendController 
{    
    public function actionIndex() 
    {
        $id = Yii::app()->getRequest()->getParam('id');
        $category = $this->loadModel('Category', $id);
        $provider = $this->_getProvider($category->getTree());
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            $this->renderPartial('index', compact('category', 'provider'));
            Yii::app()->end();
        }
        $this->render('index', compact('category', 'provider'));
    }
    
    public function actionEdit($categoryId, $id = null)
    {
        $category = $this->loadModel('Category', $categoryId);
        if ($id == null) {
            $item = new CategoryItem;
        } else {
            $item = $this->loadModel('CategoryItem', $id);
            $item->populateParent();
        }
        $item->category_id = $category->id;
        $ajax = false;
        if (isset($_POST['CategoryItem'])) {
            $item->setAttributes($_POST['CategoryItem']);
            if ($item->validate()) {
                $response = array('success' => false, 'target' => '#' . $category->getGridId());
                $node = $this->loadModel('CategoryItem', $item->parent_id);
                if ($item->getIsNewRecord()) {
                    if ($item->appendTo($node)) {
                        Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('common', 'Inserted successfully.'));
                        $response['success'] = true;
                    }
                } else {
                    if ($node->equals($item->parent()->find())) {
                        $item->saveNode(false);
                    } else {
                        $item->moveAsLast($node);
                    }
                    Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('common', 'Updated successfully.'));
                    $response['success'] = true;
                }
                if ($response['success']) {
                    $response['html'] = $this->renderPartial('_list', array(
                        'category' => $category, 'provider' => $this->_getProvider($category->getTree())
                    ), true);
                    
                    $this->renderJson($response);
                }
                
            } else {
                $ajax = true;
            }
        }
        $params = array(
            'item' => $item,
            'parents' => $category->listData() 
        );
        if ($ajax) {
            $this->renderJson(array(
                'success' => false,
                'target' => '#' . $item->getFormId(),
                'html' => $this->renderPartial('_form', $params, true)
            ));
        } else {
            $this->render('_form', $params);            
        }
    }
    
    public function actionMove($id, $dir)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            $current = $this->loadModel('CategoryItem', $id);
            if ($dir == $current::MOVE_UP) {           
                $prev = $current->prev()->find();
                if ($prev) {
                    $current->moveBefore($prev);
                }
            } elseif ($dir == $current::MOVE_DOWN) {
                $next = $current->next()->find();
                if ($next) {
                    $current->moveAfter($next);
                }
            }
            $category = $current->category;        
            $this->renderJson(array(
                'html' => $this->renderPartial('_list', array(
                    'category' => $category, 'provider' => $this->_getProvider($category->getTree())
                ), true),
                'success' => true,
                'target' => '#' . $category->getGridId()
            ));
        } else {
            throw new CHttpException(400, Yii::t('backend', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    
    public function actionDelete($id)
    {
        $this->loadModel('CategoryItem', $id)->deleteNode();
        echo true;        
    }
    
    protected function _getProvider($data)
    {
        return new CArrayDataProvider($data, array(
            'pagination' => array('pageSize' => 1000)
        ));
    }
}