<?php

class CategoriesController extends BackendController
{
    public $modelName = 'Category';
    public $redirectTo = array('admin');
    public $defaultAction = 'admin';

}
