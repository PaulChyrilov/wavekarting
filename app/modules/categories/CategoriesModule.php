<?php

class CategoriesModule extends EWebModule
{
    public $defaultController = 'categories';
    
	public function init()
	{
        parent::init();
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
        
		// import the module-level models and components
		$this->setImport(array(
			'categories.models.*',
		));
	}
    
    public function getListCategory($code, $forMenu = true)
    {
        $category = Category::getByCode($code);
        if ($category === null) {
            throw new CException('The list of categories "' . $code . '" was not found.');
        }
        return $category->getAll(null, $forMenu);
    }
    
    public function getListData($code)
    {
        $category = Category::getByCode($code);
        if ($category === null) {
            throw new CException('The list of categories "' . $code . '" was not found.');
        }
        $list = $category->listData(null, 'code');
        array_shift($list);
        return $list;
    }
    
    /**
     * @param string $code
     * @return CategoryItem;
     */
    public function getByCode($code, $exceptionOnNull = false)
    {
        $category = CategoryItem::getByCode($code);
        if ($exceptionOnNull && $category === null) {
            throw new CHttpException(404, 'Unable to find the requested object.');
        }
        return $category;
    }
    
    
}
