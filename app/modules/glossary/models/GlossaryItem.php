<?php

class GlossaryItem extends I18nActiveRecord
{
    public $created_at;
    public $updated_at;
    public $searchTitle;
    public $searchLetter;
    
    
    public $i18nModel = 'GlossaryItemI18n';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'glossary_items';
    }

    public function rules()
    {
        return array(
            array('searchTitle, searchLetter', 'safe', 'on' => 'search'),
            array('translateattrs, slug, skype', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'               => 'ID',
            'created_at'       => 'Create date',
            'updated_at'       => 'Update date',            
        );
    }
    
    public function relations()
    {
        return array(
            'i18ns' => array(self::HAS_MANY, 'GlossaryItemI18n', 'parent_id', 'index' => 'lang_id')
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        
        $criteria->select = 't.*';
        $criteria->join = 'INNER JOIN glossary_item_i18ns ON glossary_item_i18ns.parent_id = t.id';
        $criteria->group = 't.id';
        $criteria->compare('glossary_item_i18ns.title', $this->searchTitle, true);
        $criteria->compare('glossary_item_i18ns.letter', $this->searchLetter, true);
        
        $sort = new CSort;
        $sort->defaultOrder = 'glossary_item_i18ns.letter ASC';
        
        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => $sort
        ));
    }
    

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'           => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'setUpdateOnCreate' => true
            ),
        );
    }
    
    public function byLetter($letter)
    {
        $criteria = $this->getDbCriteria();
        $criteria->join .= ' INNER JOIN glossary_item_i18ns  
            ON glossary_item_i18ns.parent_id = t.id 
                AND glossary_item_i18ns.lang_id = :lang_id
                AND glossary_item_i18ns.letter = :letter';
        $criteria->params[':lang_id'] = Yii::app()->getLanguage();
        $criteria->params[':letter'] = $letter;
        return $this;
    }


    public function setTranslateAttrs($i18ns)
    {
        $this->setNestedRelation('i18ns', $i18ns);
        $this->clearDirtyAttributes($this->i18ns);
    }
    
    public static function getLetters($lang = null)
    {
        $lang = $lang === null ? Yii::app()->getLanguage() : $lang;
        return Yii::app()->getDb()->createCommand('
            SELECT DISTINCT(`letter`) 
            FROM glossary_item_i18ns
            WHERE `lang_id` = :lang_id
            ORDER BY `letter` ASC
        ')->queryColumn(array(':lang_id' => $lang));
    }
}