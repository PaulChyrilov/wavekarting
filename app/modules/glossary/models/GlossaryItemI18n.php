<?php

class GlossaryItemI18n extends EActiveRecord
{
    public $title;
    public $description;
    public $letter;
    
    public $dirtyAttributes = array('title', 'description', 'letter');

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'glossary_item_i18ns';
    }

    public function rules()
    {
        return array(
            array('title, description, letter, lang_id', 'required'),
            array('title', 'length', 'max' => 255),
            array('letter', 'length', 'max' => 1),
        );
    }

    public function attributeLabels()
    {
        return array(
            'title'   => 'Title',
            'letter'  => 'Letter',
            'description' => 'Description',
        );
    }
    
    protected function beforeSave()
    {
        $this->letter = mb_strtoupper($this->letter, Yii::app()->charset);        
        return parent::beforeSave();
    }
}