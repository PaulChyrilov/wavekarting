<?php

class GlossaryModule extends EWebModule 
{
    public $defaultController = 'items';
    
    public function init()
    {
        parent::init();

        $this->setImport(array(
            'glossary.models.*',
        ));
    }
    
    public function getLetters($lang = null)
    {
        return GlossaryItem::getLetters($lang);
    }
}
