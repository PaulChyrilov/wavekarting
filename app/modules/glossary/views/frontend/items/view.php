<section class="body-page">

    <header>
        <h2><?= $model->getI18n('title') ?></h2>
    </header>

    <section class="content-block backgrounded">

        <div class="common">

            <div class="content">
                <?= $model->getI18n('content'); ?>
                <div class="clear"></div>
            </div>

        </div>

    </section>

</section>

<?php Yii::app()->getClientScript()->registerCssFile( Yii::app()->baseUrl . '/css/pages/common.css') ?>
