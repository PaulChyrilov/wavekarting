<?php 
$this->setPageTitle(Yii::t('glossary', 'Glossary' ) . ' / ' . $letter);
Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl.'/css/pages/glossary.css') 
?>
<section class="body-page">
    <header>
        <h2><?= Yii::t('glossary', 'Glossary') ?></h2>
        <ul>
            <?php foreach ($letters as $l) { ?>
            <li<?php if ($letter == $l) { ?> class="active"<?php } ?>><?= CHtml::link(CHtml::encode($l), array('/glossary/items/index', 'letter' => $l))?></a></li>
            <?php } ?>
        </ul>
    </header>

    <section class="content-block backgrounded">
        <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'id' => 'glossary-list',
                'itemView' => '_view',
                'template' => "{items}\n{pager}",
                'itemsCssClass' => 'list-description',
                'pagerCssClass' => 'pagination',
                'pager' => array(
                    'class' => 'CLinkPager',
                    'header' => Yii::t('common', 'Pages:'),
                    'cssFile' => false,
                )
            ));
        ?>
    </section>


</section>