<?php
$this->pageTitle = 'Create glossary item';
$this->breadcrumbs=array(
	'Manage glossary' => array('admin'),
	'Create glossary item',
);

$this->renderPartial('_form', array('model' => $model)); 
?>