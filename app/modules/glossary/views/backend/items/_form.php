<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
?>
<fieldset>
    <?php $this->renderPartial('_i18ns', array('model' => $model, 'form' => $form)) ?>
    
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton(Yii::t('glossary.backend', $model->getIsNewRecord() ? 'Create' : 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
))?>


<?php $this->endWidget(); ?>
