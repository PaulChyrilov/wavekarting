<?php
$dataProvider = $model->search();
$this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => $model->getGridId(false),
    'dataProvider' => $dataProvider,
    'ajaxUrl' => array('/glossary/items/admin'),
    'filter'       => $model,
    'template'     => "{items}\n{pager}",
    'columns'      => array(
        array(
            'header'   => 'Letter',
            'type'     => 'raw',
            'name'     => 'searchLetter',
            'value'    => 'CHtml::encode($data->getI18n("letter"))',
            'sortable' => false,
        ),
        array(
            'header'   => 'Title',
            'type'     => 'raw',
            'name'     => 'searchTitle',
            'value'    => 'CHtml::encode($data->getI18n("title"))',
        ),
        array(
            'class'              => 'bootstrap.widgets.TbButtonColumn',
            'deleteConfirmation' => 'Are you sure to delete this glossary item?',
            'template' => '{update} {delete}',
        ),
    ),
));