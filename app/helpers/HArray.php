<?php

class HArray
{

    public static function string2array($str, $delim = '/\s*,\s*/')
    {
        return preg_split($delim, trim($str), -1, PREG_SPLIT_NO_EMPTY);
    }

    public static function array2string($arr, $delim = ', ')
    {
        return implode($delim, $arr);
    }

    /**
     * Collect array from input array
     *
     * @param array $enumerable
     * @param string $key
     * @param null|string $value
     * @return array
     */
    public static function collect(array $enumerable, $key, $value = null)
    {
        $result = array();
        if ($value === null) { // only keys
            foreach ($enumerable as $val) {
                $result[] = is_array($val) ? $val[$key] : $val->$key;
            }
        } else { // key => value
            foreach ($enumerable as $val) {
                if (is_array($val)) {
                    $result[$val[$key]] = $val[$value];
                } else {
                    $result[$val->$key] = $val->$value;
                }
            }
        }
        return $result;
    }
    
    /**
     * @param $list
     *
     * @return mixed
     */
    public static function getFirstKey($list)
    {
        $arr = array_keys($list);
        return array_shift($arr);
    }

}