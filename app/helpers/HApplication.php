<?php

class HApplication
{
    /**
     * @param CActiveRecord $model
     */
    public static function attachMetaParams($model)
    {
        $controller = Yii::app()->getController();
        if ($model instanceof I18nActiveRecord) {
            if ($title = $model->getI18n('meta_title')) {
                $controller->setPageTitle($title);
            } else {
                $controller->setPageTitle($model->getI18n('title'));
            }
            $controller->metaKeywords = $model->getI18n('meta_keywords');
            $controller->metaDescription = $model->getI18n('meta_description');
        } else {
            if ($model->hasAttribute('meta_title')) {
                $controller->setPageTitle($model->getAttribute('meta_title'));
            }
            if ($model->hasAttribute('meta_keywords')) {
                $controller->metaKeywords = $model->getAttribute('meta_keywords');
            }
            if ($model->hasAttribute('meta_description')) {
                $controller->metaDescription = $model->getAttribute('meta_description');
            }            
        }
    }
    
    public static function createMenuUrl($url)
    {
        if (stripos($url, 'http') === 0 || strpos($url, '/') !== 0) {
            return $url;
        }
        $default = Yii::app()->getModule('languages')->getDefault();
        $lang = Yii::app()->getLanguage();
        if ($default != $lang) {
            $url = '/' . $lang . $url;
        }
        return rtrim($url, '/');
    }
    
    public static function detectMenuActive($url, $condition = null)
    {
        return stripos(Yii::app()->getRequest()->getRequestUri(), $url) === 0;
    }
    
    public static function search($q) 
    {
        $app = Yii::app();
        if (mb_strlen($q, $app->charset) < 3) {
            return array();
        }
        $app->getModule('blog');
        $app->getModule('pages');
        $sql = '
            SELECT 
                posts.id, "Post" AS model_type, posts.published_at AS `published_at`
            FROM posts
            INNER JOIN post_i18ns
                ON post_i18ns.parent_id = posts.id AND post_i18ns.lang_id = :lang 
            WHERE posts.`status` = ' . Post::STATUS_PUBLISHED . ' 
                AND post_i18ns.title LIKE :q OR post_i18ns.content LIKE :q
            
            UNION
            
            SELECT 
                pages.id, "Page" AS model_type, pages.updated_at AS `published_at`
            FROM pages
            INNER JOIN page_i18ns
                ON page_i18ns.parent_id = pages.id AND page_i18ns.lang_id = :lang
            WHERE pages.`status` = ' . Page::PUBLISHED . ' 
                AND page_i18ns.title LIKE :q OR page_i18ns.content LIKE :q
            ORDER BY published_at DESC    
            LIMIT 30';
        $tmp = $app->getDb()->createCommand($sql)->queryAll(true, array(
            ':lang' => $app->getLanguage(), 
            ':q' => '%' . strtr($q, array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\')) . '%'
        ));
        $result = array();
        foreach ($tmp as $e) {
            $result[$e['model_type']][] = $e['id'];
        }
        foreach ($result as $key => $ids) {
            $result[$key] = CActiveRecord::model($key)->withLang()->findAllByPk($ids, array('index' => 'id'));
        }
        $data = array();
        foreach ($tmp as $e) {
            if (isset($result[$e['model_type']][$e['id']])) {
                $data[] = $result[$e['model_type']][$e['id']];
            }
        }
        return $data;
    }
    
    public static function getEditor($params = array())
    {
        if (Yii::app()->params['editor'] === 'ckeditor') {
            return array(
                'method' => 'ckEditorRow',
                'options' => array() 
            );
        } else {
            return array(
                'method' => 'redactorRow',
                'options' => array_merge(array(
                    'minHeight' => 100,
                    'imageUpload' => Yii::app()->createUrl('/images/images/redactorupload'),
                    'imageGetJson' => Yii::app()->createUrl('/images/images/redactorindex'),
                ), $params) 
            );
        }
    }
    
}