<?php

class HString
{    
    /**
     * Split string by delim  
     * @param string $str
     * @param string $delim
     * 
     * @return array 
     */
    public static function split($str, $delim = '/\s*,\s*/')
    {
        return preg_split($delim, trim($str), -1, PREG_SPLIT_NO_EMPTY);
    }
    
    /**
     * Limits a phrase to a given number of characters.
     *
     * @param   string   $str phrase to limit characters of
     * @param   integer  $limit number of characters to limit to
     * @param   boolean  $preserveWords enable or disable the preservation of words while limiting
     * @param   string   $encoding
     * @return  string
     */
    public static function limitChars($str, $limit = 100, $endChar = '...', $preserveWords = false, $encoding = 'UTF-8')
    {
        $limit = (int) $limit;

        if (($str = trim($str)) === '' || self::length($str, $encoding) <= $limit) {
            return $str;
        }

        if ($limit <= 0) {
            return '';            
        }
        $endChar = $endChar === null ? '' : $endChar;
        if ($preserveWords === false) {
            if (function_exists('mb_substr')) {
                return rtrim(mb_substr($str, 0, $limit, $encoding)) . $endChar;
            }
            return rtrim(substr($str, 0, $limit)) . $endChar;
        }

        preg_match('/^.{' . ($limit - 1) . '}\S*/us', $str, $matches);

        if (count($matches) > 0) {
            return rtrim($matches[0]);
        }
        return $str;       

    }

    /**
     * Limits a phrase to a given number of words.
     *
     * @param   string   phrase to limit words of
     * @param   integer  number of words to limit to
     * @param   string   end character or entity
     * @return  string
     */
    public static function limitWords($str, $limit = 100, $endChar = null, $encoding = 'UTF-8')
    {
        if (($str = trim($str)) === '') {
            return $str;            
        }
        $limit = (int) $limit;
        $endChar = ($endChar === null) ? '&#8230;' : $endChar;
        
        if ($limit <= 0) {
            return $endChar;            
        }

        preg_match('/^\s*+(?:\S++\s*+){1,' . $limit . '}/u', $str, $matches);

        // Only attach the end character if the matched string is shorter
        // than the starting string.
        if (count($matches) > 0) {
            return rtrim($matches[0]) . 
                (self::length($matches[0], $encoding) === self::length($str, $encoding) ? '' : $endChar);
        }
        return $str;
    }
    
    /**
     * Get string length
     * @param string $string
     * @param string $encoding default = UTF-8
     * @return integer
     */
    public static function length($string, $encoding = 'UTF-8')
    {
        if (function_exists('mb_strlen')) {
            return mb_strlen($string, $encoding);
        }
        return strlen($string);
    }
    
    /**
     * Filter to trim and strip_tags in form params 
     * @param string $value to clean
     * @param boolean $stripTags
     * @param boolean $middleSpaces
     * @return string
     */
    public static function sanitizeInput($value, $stripTags = true, $middleSpaces = true)
    {
        if ($stripTags) {
            $value = strip_tags($value);
        }
        $value = preg_replace('/\n{2,}/u', "\n\n", trim($value));
        $value = strtr($value, array("\r" => ''));
        return $middleSpaces ? preg_replace('/[ \t]+/u', ' ', $value) : $value;
    }
}