<?php
/**
 *
 * common.php configuration file
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

require_once dirname(__FILE__) . '/../helpers/common.php';

return array(
    'basePath' => realPath(__DIR__ . '/..'),
    'preload' => array('log'),
    'aliases' => array(
        'vendor' => 'application.lib.vendor'
    ),
    'import' => array(
        'application.controllers.*',
        'application.helpers.*',
        'application.models.*',
        'application.models.forms.*',
        'ext.components.*',
        'ext.behaviors.*',
        'ext.widget.*',
        'ext.components.yii-mail.*',
    ),
    'components' => array(
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
        'mail' => array(
            'class' => 'ext.components.yii-mail.YiiMail',
            'transportType' => 'php',
            //'transportOptions' => '-fnoreply@test.com',
            'viewPath' => 'application.views.mails',
            'logging' => false,
            'dryRun' => false
        ),
        'cache' => array(
            'class' => 'CFileCache'
        ),
    ),
    'params' => array_merge(
        array(
        // php configuration
            'php.defaultCharset' => 'utf-8',
            'php.timezone'       => 'UTC',
        ), 
        require (dirname(__FILE__) . '/params.php')
    )
);