<?php
/**
 *
 * main.php configuration file
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
defined('APP_CONFIG_NAME') or define('APP_CONFIG_NAME', 'main');

use Yiinitializr\Helpers\ArrayX;

// web application configuration
return ArrayX::merge(array(
    'name' => '{APPLICATION NAME}',
    
    // path aliases
    'aliases' => array(),
    
    'behaviors' => array(
        'runEnd' => array('class' => 'ext.behaviors.WebApplicationEndBehavior'),
    ),
    // controllers mappings
    'controllerMap' => array(),

    // application modules
    'modules' => array(
        'admins',
        'mail',
        'menu',
//        'news',
        'pages',
        'blog',
        'languages',
        'images',
        'gallery',
        'categories',
        'contentblock',
        'team',
        'services',
        'solutions',
        'careers',
        'locations',
        'glossary',
        'settings',
        'filemanager',
        'faq'
    ),
    // application components
    'components' => array(
        'bootstrap' => array(
            'class' => 'vendor.clevertech.yiibooster.src.components.Bootstrap',
        ),
        'user' => array(
            'allowAutoLogin' => true,
            'class' => 'ext.components.WebUser'
        ),
        'log' => array(
            'routes' => array(
                array(
                    'class' => 'vendor.malyshev.yii-debug-toolbar.yii-debug-toolbar.YiiDebugToolbarRoute',
                    // Access is restricted by default to the localhost
                    //'ipFilters'=>array('127.0.0.1','192.168.1.*', 88.23.23.0/24),
                ),
            )
        ),
        'image' => array(
            'class'        => 'ext.components.image_processor.MImageProcessor',
            'imagePath'    => 'webroot.files.images', //save images to this path    
            'imageUrl'     => '/files/images/',
            'fileMode'     => 0777,
            'imageHandler' => array(
                'class'  => 'ext.components.image_processor.image_handler.MImageHandler',
                'driver' => 'MDriverImageMagic', // MDriverGD
            ),
            'forceProcess'       => true,
            'afterUploadProcess' => array(
                'condition' => array('maxWidth' => 800, 'maxHeight' => 600),
                'actions'   => array(
                    'resize' => array('width' => 800, 'height' => 600),
                )
            ),
            'presets' => array(
                'image_preview' => array(
                    'thumb' => array('width'  => 100, 'height' => 100)
                ),
//                'preset1' => array(
//                    'thumb' => array('width' => 100, 'height'  => 100)
//                ),
//                'preset2' => array(
//                    'quality' => 100,
//                    'resize'  => array('width'  => 800, 'height' => 600),
//                    'flip'    => array('mode' => 1)
//                ),
            ),
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(                
                '/' => 'site/index',
                'sitemap.xml' => 'site/sitemap',
                '/pages/<slug:[-\w]+>' => 'pages/pages/show',
                
                '/blog/category/<code:[-\w]+>' => 'blog/posts/index',
                '/blog' => 'blog/posts/index',
                '/blog/<slug:[-\w]+>' => 'blog/posts/show',
                
                '/solutions' => 'solutions/solutions/index',
                '/solutions/<slug:[-\w]+>' => 'solutions/solutions/show',
                
                '/locations' => 'locations/locations/index',
                '/glossary' => 'glossary/items/index',
                
                '/careers' => 'careers/careers/index',
                '/careers/<slug:[-\w]+>' => 'careers/careers/show',
                                
                'services' => array('services/services/category', 'defaultParams' => array('code' => 'production')),                
                'services/category/<code:[-\w]+>' => 'services/services/category',
                'services/<slug:[-\w]+>' => 'services/services/show',
                
                '<_m:\w+>/<_c:\w+>/<id:\d+>' => '<_m><_c>/view',
                '<_m:\w+>/<_c:\w+>/<_a:\w+>/<id:\d+>' => '<_m>/<_c>/<_a>',
                '<_m:\w+>/<_c:\w+>/<_a:\w+>' => '<_m>/<_c>/<_a>',
                
                '<_c:\w+>/<id:\d+>' => '<_c>/view',
                '<_c:\w+>/<_a:\w+>/<id:\d+>' => '<_c>/<_a>',
                '<_c:\w+>/<_a:\w+>' => '<_c>/<_a>',
            ),
            
        ),
    ),
    // application parameters
    'params' => array(),
), require(dirname(__FILE__) . '/common.php'));