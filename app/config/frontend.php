<?php

return array(
    'components' => array(
        'urlManager' => array(
            'class' => 'ext.components.I18nUrlManager',
        ),
        'clientScript' => array(
            'packages' => array(
                'articmodal' => array(
                     'baseUrl' => '/js/lib/arcticmodal/',
                     'js' => array('jquery.arcticmodal.js'),
                     'css' => array('jquery.arcticmodal.css'),
                     'depends' => array('jquery'),
                 ),
            )
        )
    ),    
);