<?php

return array(
    'homeUrl' => array('/settings/settings/admin'),
    'defaultController' => 'settings',
    'name' => 'Admin System',
    'preload' => array('bootstrap'),
    'components' => array(
        'urlManager' => array(
            'showScriptName' => true,
            'rules' => array(
                '/' => 'settings/settings/admin',
            )
        ),
        'user' => array(
            'stateKeyPrefix' => 'backend',
        )
    ),
);