(function($, window) {
    var cms;
    window.CMS = cms = {
        store: {},
        popup: {},
        modal: function(title, url, skip) {
            this.store.popup = this.store.popup || {};            
            var loader = $('<div id="main-loader">loading ...</div>'),
                self   = this,
                popup  = this.popup,
                load   = function(content) {
                    loader.fadeOut(function() {
                        popup.content.html(content).show();
                    });
                };
                         
            popup.content.html(loader);
            popup.title.html(title);
            if (!popup.el.is(':visible')) {
                popup.el.modal('show');
            }
            if (!skip && self.store.popup[url]) {
                load(self.store.popup[url]);
            } else {
                $.get(url, null, function(rsp) { 
                    self.store.popup[url] = rsp;
                    load(rsp);
                });
            }
            return false;
        },
        submitForm: function(form, cb) {
            form = $(form);
            $.ajax({
                url: form.attr('action'),
                data: form.serialize(),
                dataType: form.data('dataType') || 'json',
                type: form.attr('method'),
                success: function(data) {
                    cb(null, data);
                },
                error: function(xhr, textStatus, errorThrown) {
                    cb(errorThrown, null);
                }
            });
        },
        init: function() {
            var self = this;
            self.$body = $('body');
            self.popup = {
                el: $('#popup-modal'),
                title: $('#popup-title'),
                content: $('#popup-content')
            };
            
            self.$body.on('click.api.open-modal', '[data-op="modal"]', function(e) {
                e.preventDefault();
                var $this = $(this);
                if ($this.attr('disabled') || $this.data('disabled')) {
                    return;
                }
                self.modal($this.data('title'), $this.attr('href'), $this.data('skip'));
            }).on('click.api.ajax-operation', '[data-op="ajax"]', function(e) {
                e.preventDefault();
                var $el = $(this),
                    confirmText = $el.data('confirm');
                if (!confirmText || confirm(confirmText)) {
                    $.ajax({
                        url: $el.attr('href'),
                        dataType: 'json',
                        type: $el.data('method') || 'post',
                        success: function(data) {
                            if (data.success) {
                                if (data.target && data.html !== undefined) {
                                    $(data.target).each(function() {
                                        $(this).replaceWith(data.html);
                                    });
                                }
                            }
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            console.log(errorThrown);
                        }
                    });                    
                }
            });
            $('.flashable').each(function() {
                var $el = $(this),
                    $parent = $el.parent();
                $parent.css('minHeight', $parent.height());
                $el.hide().delay(200).fadeIn(500)
            });
        }
    }
    
    $(function() {
        cms.init();
        $('#footer-section').slideDown(750);

        $('body').on('click', '.proposition', function(e) {
            e.preventDefault();
            $.arcticmodal({
                type: 'ajax',
                url: $(this).attr('href'),
                ajax: {
                    type: 'GET',
                    success: function(data, el, response) {
                        data.body.html(response);
                    }
                }
            });
        }).on('change', '#file-industry', function() {
            var $el = $(this);
            if ($el.data('sending')) {
                return false;
            }
            $el.data('sending', 1);
            $.post($el.data("action"), { industry: $el.val() }, function(rsp) {
                $.arcticmodal("close");
                $.arcticmodal({content: rsp});
            })
        }).on('submit', '#file-type-form', function() {
            var form = $(this);
            if (form.data('sending')) {
                return false;
            }
            if (form.find('input:checked').length === 0) {
                $('#file-type-form-em').show();
            } else {
                $('#file-type-form-em').hide();
                form.data('sending', 1);
                $.post(form.attr("action"), form.serialize(), function(rsp) {
                    $.arcticmodal("close");
                    $.arcticmodal({content: rsp});
                });
            }
            return false;
        }).on('submit', '#file-download-form', function() {
            if ($(this).find('input:checked').length === 0) {
                $('#file-download-form-em').show();
                return false;
            }
            setTimeout(function() {
                $.get('/files/thankyou', null, function(rsp) {
                    $.arcticmodal("close");
                    $.arcticmodal({content: rsp})
                });
            }, 1000);
            return true;
        });
        
        var $content = $('.content');
        
        $content.find('img').each(function() {
            var $el = $(this);
            if ($el.css('float') === 'left' && $el.css('margin-right') === '10px') {
                $el.css('margin-right', '30px');
            }
        });
        $content.find('p:last').css('margin-bottom', '0');
    });
    
})(jQuery, window);